const possibleErrors = {
  // использование отрицательного нуля ( -0 )
  "no-compare-neg-zero": "error",

  // присвоение в условных операторах
  // if (user.jobTitle = "manager") {}
  "no-cond-assign": "error",

  // использование console
  "no-console": ["error", {allow: ["error"]}],

  // использование констант в условиях if(true)
  "no-constant-condition": "error",

  // использование ключевого слова debugger;
  "no-debugger": "warn",

  // дубликаты аргументов
  "no-dupe-args": "error",

  // дубликаты ключей
  "no-dupe-keys": "error",

  // дубликаты значения в case
  "no-duplicate-case": "error",

  // пустые блоки
  "no-empty": "warn",

  // пустые классы символов в регулярках ([] - /abc[]/)
  "no-empty-character-class": "error",

  // обяъвление переменных в catch
  "no-ex-assign": "off",

  // лишнее приведение в тип boolean (if(!!a) {})
  "no-extra-boolean-cast": "off",

  // лишние скобки
  "no-extra-parens": "off",

  // лишние точки с запятой
  "no-extra-semi": "error",

  // присвоение к именованной функции без присвоения (function a(){}; a=b;)
  "no-func-assign": "error",

  // декларировании Ф и var внутри условных операторов
  "no-inner-declarations": "off",

  // неккоректные регулярные выражения в конструкторе
  "no-invalid-regexp": "error",

  // использование пробельных символов в коде (не в строках)
  "no-irregular-whitespace": ["error", {"skipComments": true}],

  // использование нескольких пробелов в регулярке(/   /)
  "no-regex-spaces": "error",

  "no-sparse-arrays": "off",

  // template выражения в строках ("Hi ${name}")
  "no-template-curly-in-string": "error",

  // отрицательный левый операнд в left (!key in object)
  "no-unsafe-negation": "error",

  "no-unsafe-finally": "off",

  // вызов глобальных объектов как функций( Math(), а не math() )
  "no-obj-calls": "error",

  // непроставленные точки с запятой
  "no-unexpected-multiline": "error",

  // неиспользуемый код (после return, throw)
  "no-unreachable": "error",

  // сравнение с NaN (1 != NaN)
  "use-isnan": "error",

  // неккоректный jsdoc http://usejsdoc.org
  "valid-jsdoc": "off",

  // сравнение с неизвестным типом (typeof foo === 'abracadabra')
  "valid-typeof": "error"
};
const bestPractices = {
  // использование сеттера/геттера не в паре setWithoutGet/getWithoutSet
  "accessor-pairs": "warn",

  // методы массива должны возвращать значения
  "array-callback-return": "warn",

  // объявление в скоупе (на уровень ниже испльзуемого)
  "block-scoped-var": "warn",

  // методы без this (если в методе не используется контекст, то стоит объявлять как static)
  "class-methods-use-this": "warn",

  // цикломатическая сложность
  "complexity": ["warn", {"maximum": 14}],

  // return с undefined, без значения
  "consistent-return": "off",

  // все выражения в скоупе if(a) { scope }
  "curly": "error",

  // без default значения для case (и без комментария // no default)
  "default-case": "off",

  // точка(обращение к свойству) не на новой строке
  "dot-location": "off", //["warn", "property"]

  // вызов не через точку там, где можно с ней
  "dot-notation": "warn",

  // не строгое сравнение
  "eqeqeq": "warn",

  // for-in без hasOwnProperty
  "guard-for-in": "warn",

  // использование alert
  "no-alert": "warn",

  // использование arguments.caller and arguments.callee (затрудняет оптимизацию интерперетатору)
  "no-caller": "error",

  // объявления let/const/class в case без скоупа
  "no-case-declarations": "error",

  // при исользовании регулярки экранировать знак =, в /\=foo/, иначе может быть интерпретировано как деление /=
  "no-div-regex": "warn",

  // return в else, когда можно обойтись
  "no-else-return": "warn",

  // пустые Ф/генераторы/стрелочные и без комментариев внутри
  // нужно хотя бы комментарий вставить в тело
  "no-empty-function": "off",

  // пустое правило деструктуризации
  "no-empty-pattern": "warn",

  // сравнение с null
  "no-eq-null": "error",

  // использование eval
  "no-eval": "error",

  // расширение нативных объектов
  "no-extend-native": "warn",

  // ненужный bind
  "no-extra-bind": "warn",

  // ненужный label в циклах, switch
  "no-extra-label": "error",

  // проваливания без комментария // fall through
  "no-fallthrough": "warn",

  // неполная запись чисел с плавающей запятой (.5, 2., -.7)
  "no-floating-decimal": "warn",

  // магическое приведение типов
  "no-implicit-coercion": ["error", {
    "boolean": true,
    "number": true,
    "string": true,
    "allow": [/*"!!", "~", "*", "+"*/]
  }],

  // объявление переменных/Ф в рут скоупе
  "no-implicit-globals": "off",

  // использование eval-методов
  "no-implied-eval": "error",

  // невалидный this
  "no-invalid-this": "off",

  // использование __iterator__
  "no-iterator": "warn",

  // использование label
  "no-labels": "error",

  // ненужные скоупы
  "no-lone-blocks": "error",

  // создание Ф в циклах
  "no-loop-func": "warn",

  // магические числа
  "no-magic-numbers": "off",

  // множественные пробелы
  "no-multi-spaces": "warn",

  // многострочные строки через \
  "no-multi-str": "warn",

  // присвоение нативным объектам (String = 123)
  "no-global-assign": "error",

  // создание экземляра без присвоения
  "no-new": "error",

  // создание Ф через Function
  "no-new-func": "warn",

  // создание примитивов через new (new Number(1))
  "no-new-wrappers": "error",

  // восьмеричные числа через 0
  "no-octal": "warn",

  // экранирование восьмеричных чисел не через unicode или hexadecimal
  "no-octal-escape": "warn",

  // присвоение значения параметру
  "no-param-reassign": "off",

  // использование __proto__, вместо getPrototypeOf
  "no-proto": "off",

  // определение переменных с одинаковым названием
  "no-redeclare": "error",

  // присвоение в return
  "no-return-assign": ["warn", "always"],

  // использование ссылок javascript:*
  "no-script-url": "warn",

  // присвоение в самого себя (a = a)
  "no-self-assign": "error",

  //сравнение с самим собой
  "no-self-compare": "error",

  // использование оператора запятая
  "no-sequences": "off",

  // использование throw не с объектом ошибки (throw "lalala")
  "no-throw-literal": "off",

  // немодифицированное значение для циклов (while(node) {do(node);})
  "no-unmodified-loop-condition": "error",

  // неиспользуемые выражения
  "no-unused-expressions": ["warn", {
    "allowTernary": true, //тернарные выражения
    "allowShortCircuit": true //выражения вида a && b(), a || (b = c())
  }],

  // неиспользуемые labels
  "no-unused-labels": "error",

  // использование call и apply там, где можно сделать обычный вызов
  "no-useless-call": "off",

  // конкатинация строк там, где она не нужна
  "no-useless-concat": "error",

  // использование void
  "no-void": "off",

  // использование определенных слов в комментариях
  "no-warning-comments": ["warn", {
    "terms": ["todo"],
    "location": "anywhere"
  }],

  // использование with
  "no-with": "warn",

  // использование второго параметра в parseInt
  "radix": ["warn", "always"],

  // объявление var наверху
  "vars-on-top": "off",

  // вызов самовызывающийся Ф без оборачивания
  "wrap-iife": ["warn", "any"],

  // использование yoda сравнений (0 == a, 1 === b)
  "yoda": "off"
};
const variables = {
  // инициализация переменной при объявлении
  "init-declarations": "off",

  // имя аргумента в catch совпадает с объявленной переменной,
  // var err = "x"; try {throw "problem";} catch (err) {}
  "no-catch-shadow": "error",

  // удаление переменной
  "no-delete-var": "error",

  // имя лейбла совпадает с перменной
  "no-label-var": "error",

  // запрет определенных глобальных переменных
  "no-restricted-globals": "off", // ["error", "event"],

  // одинаковые имена переменных и Ф, в скоупах ниже
  "no-shadow": "off",

  // использование зарегестрированных имен (var undefined = true)
  "no-shadow-restricted-names": "error",

  // использование неопределенных переменных/Ф, не в typeof
  "no-undef": ["error", {"typeof": false}],

  // инициализация переменных undefined
  "no-undef-init": "error",

  // использование undefined в коде(a===undefined, а не typeof a==="undefined")
  "no-undefined": "error",

  // неиспользуемые переменные, аргументы, ошибка в catch
  "no-unused-vars": ["error", {
    "vars": "local",
    "args": "none", //all?
    "caughtErrors": "none" //all?
  }],

  // использование Ф/классов/переменных перед их объявлением
  "no-use-before-define": ["error", {
    "functions": false,
    "classes": true
  }]
};
const nodeAndCommon = {
  // возвращать callback
  "callback-return": "off",

  // require только сверху
  "global-require": "off",

  // обязательно обработать параметр с ошибкой(err|error)
  "handle-callback-err": "error",

  // объявление различных переменных с require переменными (var a='a', b=require('d');)
  "no-mixed-requires": "off",

  // оператор new для require (new require('class'))
  "no-new-require": "error",

  // конкатенация путей с __dirname, __filename, не через модуль path
  "no-path-concat": "warn",

  // прямое использование process.env
  "no-process-env": "off",

  // использование process.exit
  "no-process-exit": "off",

  // нежелательные модули
  "no-restricted-modules": "off",

  // использование синхронных методов
  "no-sync": "off"
};
const stylistic = {
  // пробелы в начале и конце массива
  "array-bracket-spacing": ["error", "never", {
    "singleValue": false,
    "objectsInArrays": false,
    "arraysInArrays": false
  }],

  // пробелы в начале и конце блока
  "block-spacing": ["error", "always"],

  // скобки блока не на следующей строке
  "brace-style": ["error", "stroustrup"],

  // обязательный camelcase стиль
  "camelcase": "off",

  // запятая после последнего элементе массива/объекта
  "comma-dangle": "off",

  // пробелы перед/после запятой
  "comma-spacing": ["error", {"before": false, "after": true}],

  // запятая в конце
  "comma-style": ["error", "last"],

  // вычисляемое имя ключа отделено пробелами
  "computed-property-spacing": ["error", "never"],

  // одинаковые псевдонимы для this
  "consistent-this": "off",

  // перенос в конце файла
  "eol-last": "error",

  // обязательное именование Ф
  "func-names": "off",

  // объявление Ф
  "func-style": "off",

  // черный список слов
  "id-blacklist": "off",

  // длина идентификатора
  "id-length": ["error", {
    "min": 2,
    "max": Infinity,
    "exceptions": ["h", "x", "y"]
  }],

  // регулярка для идентификаторов
  "id-match": "off",

  // отступ
  "indent": "off",

  // кавычки в jsx
  "jsx-quotes": "error",

  // пробелы от двоеточия ключа
  "key-spacing": ["error", {
    "beforeColon": false,
    "afterColon": true
  }],

  // пробелы перед/после ключевых слов
  "keyword-spacing": "error",

  // перевод строки \n или \r\n
  "linebreak-style": "off",//["error", "unix"],

  // форматирование комментариев
  "lines-around-comment": "off",

  // максимальный уровень вложенности
  "max-depth": ["error", 5],

  // максимальная длина строки
  "max-len": ["error", 80],

  // максимальная вложенность коллбэков
  "max-nested-callbacks": ["error", 5],

  // максимальное число параметров
  "max-params": ["error", 5],

  // максимальное число выражений на линии
  "max-statements-per-line": ["error", {"max": 2}],

  // максимальное число выражений
  "max-statements": ["error", 25, {
    "ignoreTopLevelFunctions": true
  }],

  // консутркторы(то что записано в capitalize стиле) без new
  "new-cap": "off",

  // скобки после создание через new (new Class())
  "new-parens": "error",

  // каждая новая цепочка с новой строки
  "newline-per-chained-call": ["error", {
    "ignoreChainWithDepth": 3
  }],

  // использование Array как конструктора с больше чем одним аргументом
  "no-array-constructor": "error",

  // использование побитовых операторов
  "no-bitwise": "off",

  // использование continue
  "no-continue": "off",

  // комментарии на одной строке с кодом
  "no-inline-comments": "off",

  // одиночный if там, где можно сделать else if
  "no-lonely-if": "error",

  // использование табов и пробелов вместе
  "no-mixed-spaces-and-tabs": "error",

  // максимальное кол-во подряд идущих пустых срок
  "no-multiple-empty-lines": ["error", {
    "max": 10,
    "maxEOF": 1,
    "maxBOF": 1
  }],

  // if else с отрицаением там, где можно без отрицания (if(!a) {b;} else {a;} => if(a) {a;} else {b;})
  "no-negated-condition": "error",

  // множественные тернарные операторы
  "no-nested-ternary": "error",

  // создание объекта через new Object
  "no-new-object": "error",

  // использование ++ --
  "no-plusplus": "off",

  // пробел перед скобками вызова Ф (ф ())
  "func-call-spacing": ["error", "never"],

  // тернарные операторы
  "no-ternary": "off",

  // конечные пробелы
  "no-trailing-spaces": "error",

  // использование нижнего подчеркикания
  "no-underscore-dangle": "off",

  // использование тернарного оператора там, где он не нужен
  "no-unneeded-ternary": "error",

  // пробел перед вызововм метода/свойства (foo .bar)
  "no-whitespace-before-property": "error",

  // пробелы перед/после скобок объекта
  "object-curly-spacing": ["error", "never"],

  // один var, let, const на одно определение
  "one-var": ["error", "never"],

  // одно объявление на одну строку, не считая инициализации переменных
  "one-var-declaration-per-line": ["error", "initializations"],

  // использование коротких операторов, где можно
  "operator-assignment": "error",

  // перевод строк перед/после операторов
  "operator-linebreak": "off",

  // переводы строк внутри блоков
  "padded-blocks": "off",

  // использование кавычек в свойствах
  "quote-props": ["error", "as-needed"],

  // кавычки
  "quotes": ["error", "single"],

  // обязательный jsdoc
  "require-jsdoc": ["error", {
    "require": {
      "FunctionDeclaration": true,
      "ClassDeclaration": true,
      "MethodDefinition": true
    }
  }],

  // точка с запятой
  "semi": ["error", "always"],

  // пробел перед/после точки с запятой
  "semi-spacing": "off",

  // сортировка import'ов
  "sort-imports": "off",

  // сортировка var
  "sort-vars": "off",

  // пробел перед блоком
  "space-before-blocks": "error",

  // пробел перед скобками Ф
  "space-before-function-paren": ["error", {
    asyncArrow: "always",
    named: "never",
    anonymous: "never"
  }],

  // пробел в скобках
  "space-in-parens": "off",

  // пробелы после/перед инфиксными операторами
  "space-infix-ops": "off",

  // пробелы после/перед унарными операторами
  "space-unary-ops": "error",

  // пробел перед текстом комментария
  "spaced-comment": "off",

  // BOM в файле с unicode кодировкой
  "unicode-bom": "error",

  // оборачивание регулярок в скобки
  "wrap-regex": "off",

  "padding-line-between-statements": [
    "error",
    {
      blankLine: "always",
      prev: ["const", "let", "var"],
      next: "*"
    },
    {
      blankLine: "any",
      prev: ["const", "let", "var"],
      next: ["const", "let", "var"]
    },
    {
      blankLine: "always",
      prev: "*",
      next: "return"
    }
  ]
};
const es6 = {
  // СФ в теле
  "arrow-body-style": ["error", "as-needed"],

  // скобки в аргументах СФ
  "arrow-parens": ["error", "as-needed"],

  // пробелы до/после стрелки
  "arrow-spacing": ["error", {
    "before": true,
    "after": true
  }],

  // вызов родительского конструктора, когда нет родителя
  "constructor-super": "error",

  // пробел до/после звездочки генератора
  "generator-star-spacing": ["error", {
    "before": false,
    "after": true
  }],

  // присвоение к названию класса (class A{}; A=1; let A=class A{})
  "no-class-assign": "error",

  // использование СФ там, где используются операторы сравнения
  "no-confusing-arrow": "off",

  // присваивание константе
  "no-const-assign": "error",

  // дубликаты свойств/методов класса
  "no-dupe-class-members": "error",

  // использование new с Symbol
  "no-new-symbol": "error",

  // нежелательные модули
  "no-restricted-imports": "off",

  // использование this перед super
  "no-this-before-super": "error",

  // не используемое вычисление ключа
  "no-useless-computed-key": "error",

  // не используемый конструктор
  "no-useless-constructor": "error",

  "no-useless-rename": "off",

  // использование let и const вместо var
  "no-var": "error",

  // сокращения es6
  "object-shorthand": "error",

  // использование СФ в коллбэках
  "prefer-arrow-callback": "error",

  // использование const там, где значение не изменется
  "prefer-const": ["error", {
    destructuring: "all"
  }],

  // использование числовых литералов, вместо parseInt. 0b110101, 0o767, 0x9f1
  "prefer-numeric-literals": "error",

  // использование rest в параметрах (...args)
  "prefer-rest-params": "error",

  // использование spread, вместо apply
  "prefer-spread": "error",

  // использование шаблонов, вместо конкатенации вручную
  "prefer-template": "error",

  // обязательный yield в генераторах
  "require-yield": "error",

  // пробелы в spread
  "rest-spread-spacing": ["error", "never"],

  "sort-imports": "off",

  // описание для Symbol
  "symbol-description": "error",

  // пробелы в значениях в шаблоне
  "template-curly-spacing": "off",

  // пробел после звездочки в yield
  "yield-star-spacing": ["error", "after"]
};

const rules = Object.assign(possibleErrors, bestPractices,
  variables, nodeAndCommon, stylistic, es6);

module.exports = {
  "extends": [],

  "plugins": [],

  "parserOptions": {
    "ecmaVersion": 2017,
    "sourceType": "module",
    "ecmaFeatures": {
      "impliedStrict": true,
      "modules": true,
      "jsx": true,
      "experimentalObjectRestSpread": true
    }
  },

  "env": {
    "es6": true,
    "browser": true,
    "node": true,
    "mocha": true,
    "shared-node-browser": true,
    "worker": true,
    "mongo": true,
    "serviceworker": true
  },

  "globals": {
    "console": true,
    "assert": true
  },

  "rules": rules
};
