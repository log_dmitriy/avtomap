FROM node:10.12-alpine

LABEL maintainer="rdi <rochnyak_d_i@mail.ru>" \
      description="проект Openspeedcam"

ENV NODE_ENV=production

WORKDIR /app

COPY package*.json ./

RUN apk add --no-cache python build-base paxctl graphicsmagick \
  && paxctl -cm `which node` \
  && npm i -g pm2 webpack@2.5.1 karma@1.7.0 \
  && NODE_ENV=development npm i

EXPOSE 80 8080 8085 3000

# VOLUME ["/app/logs"]

CMD ["node", "bin/www"]
