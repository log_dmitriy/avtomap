# расположение моки
MOCHA = ./node_modules/.bin/mocha

PARAMS = --reporter spec \
		--colors \
		--sort \
		--timeout 3000 \

PATHS = `find test -name '*.js' ! -ipath '*/__*' | sort -fd`

test:
	@NODE_ENV=test $(MOCHA) \
		$(PARAMS) \
		$(PATHS)

trace-test:
	@NODE_ENV=test $(MOCHA) \
		--full-trace \
		$(PARAMS) \
		$(PATHS)

# запускает отдельный тест
# например: make test/lib/aaa/user
# запустит тест только для файла находящемуся
# по указанному пути (./test/lib/aaa/user.js)
# также можно указать путь к директории
# тогда будут выполнены тесты по всем файлам
# первого уровня вложенности
test/%:
	@NODE_ENV=test $(MOCHA) \
		$(PARAMS) \
		$@

.PHONY: test
.PHONY: trace-test
.PHONY: test/%