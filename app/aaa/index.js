const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const PassportAuthenticationError = require(
  'passport/lib/errors/authenticationerror');

const roles = require('./roles');
const roleLogics = require('./logics');

module.exports = function(models) {
  const User = models.get('user');

  passport.serializeUser((user, done) => done(null, user._id));
  passport.deserializeUser(async (id, done) => {
    let row;

    try {
      row = await User.storage.getById(id);
    }
    catch (error) {
      done(error);

      return;
    }

    done(null, new User(row));
  });

  passport.use(new LocalStrategy(
    // настройки
    {usernameField: 'login', passwordField: 'password'},
    // функция верификации
    (login, password, done) => {
      User.storage.get({login})
        .then(row => {
          const user = new User(row);

          if (!row[User.storage.idKey] || !user.checkPassword(password)) {
            return done(null, false, {
              message: 'Неверный логин или пароль'
            });
          }

          return done(null, user, {
            message: 'Вы успешно вошли'
          });
        })
        .catch(done);
    }
  ));

  const authenticate = passport.authenticate('local', {
    // писать результат info.message в req.session.messages[]
    successMessage: true,
    failureMessage: true,

    // выкидывать на middleware с ошибкой, а не выдавать ответ с кодом 401
    failWithError: true,

    // отключить трансформацию инфы при успешной аутентификации,
    //  которая попадает в req.authInfo
    authInfo: false,

    badRequestMessage: 'Логин и пароль обязательны для заполнения'
  });

  const authorize = async (req, resource) => {
    if (req.roles.includes('banned')) {
      return false;
    }

    for (const role of req.roles) {
      if (role === 'admin') {
        return true;
      }

      if (!Reflect.has(roles, role)) {
        continue;
      }

      if (roles[role].includes(resource)) {
        return true;
      }

      if (roles[role].includes(`${resource} +logic`)) {
        const result = await roleLogics[resource](req.user, models);

        if (!result) {
          continue;
        }
      }
    }

    return false;
  };

  return {
    authenticate(req, res) {
      return new Promise((resolve, reject) => {
        authenticate(req, res, error => {
          if (error) {
            return reject(error);
          }

          resolve();
        });
      });
    },
    AuthenticateError: PassportAuthenticationError,

    authorize
  };
};
