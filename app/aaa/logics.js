module.exports = {
  'point.delete': async (user, models) => {
    const Point = models.get('point');
    const count = await Point.storage.count({
      'author._id': user._id});

    return count >= 100;
  }
};
