const defResources = [
  'addresses.countries', 'addresses.regions',
  'comments.add', 'comments.change', 'comments.delete', 'comments.complain',
  'export',
  'point.add', 'point.change',
  'profile.detail', 'profile.change', 'profile.points.created',
  'profile.points.changed', 'profile.points.comments',
  'notifications.user', 'notifications.point', 'notifications.comment',
  'notifications.point.change', 'notifications.comment.change'
];

const user = defResources.concat(['point.delete +logic']);
const moderator = defResources.concat(['point.delete']);

const banned = [];

module.exports = {
  user,
  moderator,
  banned
};
