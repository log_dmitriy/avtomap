/** модуль формирует данные полученные от пользователя  */

const fsp = require('fs-promise');
const bodyParser = require('body-parser');
const formidable = require('formidable');
const typeis = require('type-is');
const os = require('os');

module.exports = function({
  whiteMethods = ['POST', 'PUT', 'DELETE'],
  maxFields = 1000,
  maxFieldsSize = 1 * 1024 * 1024,
  maxFileSize = 4 * 1024 * 1024,
  // директория для загрузки
  uploadDir = os.tmpdir(),
  // расширения исходных файлов
  keepExtensions = true,
  // множественная загрузка файлов
  multiples = true
} = {}) {
  const jsonParser = bodyParser.json({
    limit: maxFieldsSize
  });

  const urlEncodedParser = bodyParser.urlencoded({
    extended: false,
    parameterLimit: maxFields,
    limit: maxFieldsSize
  });

  return function(req, res, next) {
    const method = req.method.toUpperCase();

    if (whiteMethods.includes(method) && typeis(req, 'multipart')) {
      const fields = {};
      const files = {};

      const form = new formidable.IncomingForm({
        keepExtensions,
        multiples,
        uploadDir,
        maxFileSize,
        maxFieldsSize,
        maxFields
      })
      .on('field', (name, value) => {
        if (name in fields) {
          if (!Array.isArray(fields[name])) {
            fields[name] = [fields[name]];
          }

          fields[name].push(value);
        }
        else {
          fields[name] = value;
        }
      })
      .on('file', function(name, file) {
        if (this.multiples) {
          if (!files[name]) {
            files[name] = [];
          }

          files[name].push(file);
        }
        else {
          files[name] = file;
        }
      })
      .on('error', next)
      .on('end', () => {
        req.body = fields;
        req.files = files;

        next();
      });

      form.parse(req);

      res.on('finish', () => {
        function remove(file) {
          if (!Array.isArray(file)) {
            fsp.remove(file.path).catch(console.error)

            return;
          }

          file.forEach(ff => remove(ff));
        };

        for (const name in files) {
          const file = files[name];

          remove(file);
        }
      });
    }
    else if (typeis(req, 'application/json')) {
      jsonParser(req, res, next);
    }
    else {
      urlEncodedParser(req, res, next);
    }
  };
};
