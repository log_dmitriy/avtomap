/*
 модуль для работы с кэшем
 */

const redis = require('redis');
const {create: createManager} = require('./managers/cache');

module.exports = async function(config, modelManager) {
  const managerPath = config.managerPath;
  const db = redis.createClient(config);
  const facade = {
    /**
     * добаляет значение по ключу
     *
     * @param {String}  key    ключ
     * @param {*}       value  значение
     *
     * @return {Promise}
     */
    setValue(key, value) {
      return new Promise((resolve, reject) => {
        db.set(key, value, (error, result) => {
          if (error) {
            return reject(error);
          }

          resolve(result);
        });
      });
    },

    /**
     * возвращает значение по ключу
     *
     * @param {String}  key    ключ
     *
     * @return {Promise}
     */
    getValue(key) {
      return new Promise((resolve, reject) => {
        db.get(key, (error, result) => {
          if (error) {
            return reject(error);
          }

          resolve(result);
        });
      });
    },

    /**
     * добаляет значения в множество
     *
     * @param {String}  key    название множества
     * @param {Array}   values значения множества
     *
     * @return {Promise}
     */
    addInSet(key, ...values) {
      return new Promise((resolve, reject) => {
        db.sadd(key, ...values, (error, result) => {
          if (error) {
            return reject(error);
          }

          resolve(result);
        });
      });
    },

    /**
     * возвращает значения множества
     *
     * @param {String}  key название множества
     *
     * @return {Promise}
     */
    getSet(key) {
      return new Promise((resolve, reject) => {
        db.smembers(key, (error, result) => {
          if (error) {
            return reject(error);
          }

          resolve(result);
        });
      });
    },

    /**
     * закрыть кеш
     */
    quit() {
      return db.quit();
    }
  };

  const manager = await createManager(managerPath, facade, modelManager);

  await manager.runAll();

  return facade;
};
