/*
  модуль для работы с БД
*/

const mongo = require('mongodb').MongoClient;

module.exports = async function({host, port, database, user, password}) {
  const config = {
    poolSize: 10,
    ssl: false
  };

  const passport = Boolean(user) ? `${user}:${password}@` : '';
  const url = `mongodb://${passport}${host}:${port}/${database}`;
  const db = await mongo.connect(url, config);

  return db;
};
