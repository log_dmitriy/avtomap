/** расширенная ошибка */
class ExtendedError extends Error {
  /**
   * конструктор
   *
   * @param {String}  message   сообщение
   * @param {Boolean} stopTrace флаг остановки трассировки
   *
   * @return {undefined}
   */
  constructor(message, stopTrace) {
    super(message);

    this.name = this.constructor.name;
    this.message = message;

    if (stopTrace === void 0) {
      stopTrace = this.constructor;
    }

    Error.captureStackTrace(this, stopTrace);
  }
}

module.exports = ExtendedError;
