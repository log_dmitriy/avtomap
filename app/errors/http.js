const CODES = require('http').STATUS_CODES;
const ExtendedError = require('./extendederror');

/** ошибка http */
class HttpError extends ExtendedError {
  /**
   * конструктор
   *
   * @param {Number} status   статус ответа
   * @param {String} message  сообщение ответа
   * @param {Object} body     тело сообщения
   * @param {Object} headers  заголовки сообщения
   *
   * @return {undefined}
   */
  constructor(status, message = CODES[status], body = {}, headers = {}) {
    if (message instanceof Error) {
      const error = message;

      message = error.message;
      body = ('data' in error) ? error.data : body;

      super(message, error);
    }
    else {
      super(message);
    }

    this.status = status;
    this.message = message;
    this.body = body;
    this.headers = headers;
  }
}

module.exports = HttpError;
