/* модуль обработки ошибок */

const HttpError = require('./http');

/* middleware для обработки 404 ошибки */
const notfound = function(req, res, next) {
  next(new HttpError(404));
};

/* middleware для обработки всех ошибок */
const allErrors = function(error, req, res, next) {
  const isHTTP = error instanceof HttpError;
  const status = error.status || 500;
  let result = haveField(error.body) ?
    error.body : {message: error.message};

  if (isHTTP) {
    res.set(error.headers);
  }

  if (req.app.get('env') === 'development') {
    result.__error = {
      name: error.name,
      stack: error.stack
    };
  }
  else if (!isHTTP && status >= 500) {
    result = {message: 'Server error!'};
  }

  res.status(status);
  res.send(result);
};

const responsePatch = function(app) {
  app.response.HttpError = HttpError;
  app.response.throw = function(...args) {
    const lastArg = args[args.length - 1];

    if (typeof lastArg === 'function') {
      const errorArgs = args.slice(0, -1);

      lastArg(new HttpError(...errorArgs));
    }
    else {
      throw new HttpError(...args);
    }
  };
};

module.exports = {notfound, allErrors, responsePatch};

/**
 * проверяет наличие хотя бы одного поля в объекте
 *
 * @param  {Object} obj объект
 *
 * @return {Boolean}
 */
function haveField(obj) {
  if (obj === void 0) {
    return false;
  }

  for (const field in obj) {
    if (obj.hasOwnProperty(field)) {
      return true;
    }
  }

  return false;
}
