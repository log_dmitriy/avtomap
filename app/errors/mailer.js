const ExtendedError = require('./extendederror');

/** ошибка отправителя сообщений */
class MailerError extends ExtendedError {}

module.exports = MailerError;
