const ExtendedError = require('./extendederror');

/** ошибка модели */
class ModelError extends ExtendedError {}

module.exports = ModelError;
