const path = require('path');
const AExportGenerator = require('./index');

const mapPath = path.join(__dirname, '..', 'map', 'igo.json');
const map = require(mapPath);

/**
 * генератор экспорта в формате igo
 */
class IgoGenerator extends AExportGenerator {
  /**
   * возвращает заголовок в файле выгрузки
   */
  get header() {
    const header = super.header;

    return `IDX,X,Y,TYPE,SPEED,DirType,Direction, ${header}`;
  }

  /**
   * @param {object} fields поля точки
   */
  transformPoint(fields) {
    const idx = fields.idx || 0;
    const type = map.type[fields.type];
    const {x, y, dirType} = fields;

    const curDirection = fields.direction || 0;
    const direction = curDirection >= 180 ? curDirection - 180 : curDirection + 180;
    const speed = idx === 0 ? 5 : fields.speed;

    const strMainFields = [
      idx, x, y, type,
      speed, dirType, direction
    ].join(',');

    return strMainFields;
  }
}

module.exports = IgoGenerator;
