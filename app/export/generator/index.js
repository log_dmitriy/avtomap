const stream = require('stream');
const moment = require('moment');

/**
 * абстрактный класс-генератор экспортных данных
 */
class AExportGenerator extends stream.Transform {
  /**
   * конструктор
   *
   * @parma {Object}  i18n
   */
  constructor(i18n) {
    super({
      // автоматически завершать открытую для чтения часть,
      //  когда завершается открытая для записи и наоборот
      allowHalfOpen: false,

      readableObjectMode: false,
      writableObjectMode: true
    });

    this.idx = 0;
    this.i18n = i18n;

    this.push(`${this.header}\n`);
  }

  /**
   * возвращает заголовок в файле выгрузки
   */
  get header() {
    const date = moment().format('DD-MM-YYYY HH:mm');

    return `// OpenSpeedcam ${date}`;
  }

  /**
   * @param {Object} fields поля точки
   */
  transformPoint(point) {
    throw new Error('Метод transformPoint не определен.');
  }

  /**
   * обрабатывает записываемые байты, вычисляет вывод,
   *   затем передает вывод на открытую для чтения часть,
   *   используя метод push()
   *
   * @param  {Buffer|String} chunk    порция, которая преобразуется
   * @param  {String}        encoding 'buffer' - если буфер
   * @param  {Function}      callback вызывает после обработки chunk
   */
  _transform(chunk, encoding, callback) {
    try {
      this.__pushData(chunk);

      callback();
    }
    catch (error) {
      callback(error);
    }
  }

  /**
   * Записывает данные точки
   *
   * @param {Object} fields данные точки
   */
  __pushData(fields) {
    const hasChildrens = Reflect.has(fields, 'childrens');

    if (hasChildrens) {
      fields.__pointIndex = 1;
    }

    const data = this.transformPoint(fields);

    this.push(`${data}\n`);

    if (!hasChildrens) {
      return;
    }

    fields.childrens.forEach((child, index) => {
      child._id = fields._id;
      child.type = fields.type;
      child.speed = fields.speed;
      child.__pointIndex = index + 2;

      this.__pushData(child);
    });
  }
}

module.exports = AExportGenerator;
