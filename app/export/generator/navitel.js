const path = require('path');
const AExportGenerator = require('./index');

const mapPath = path.join(__dirname, '..', 'map', 'navitel.json');
const map = require(mapPath);

/**
 * генератор экспорта в формате navitel
 */
class NavitelGenerator extends AExportGenerator {
  /**
   * возвращает заголовок в файле выгрузки
   */
  get header() {
    const header = super.header;

    return `IDX,X,Y,TYPE,SPEED,DirType,Direction ${header}`;
  }

  /**
   * @param {object} fields поля точки
   */
  transformPoint(fields) {
    const idx = fields.idx || 0;
    const type = map.type[fields.type];
    const {x, y, speed, dirType, direction} = fields;

    const strMainFields = [
      idx, x, y, type,
      speed, dirType, direction
    ].join(',');
    // const strCommentsFields = String(fields._id || '');

    return strMainFields;
  }
}

module.exports = NavitelGenerator;
