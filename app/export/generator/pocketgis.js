/* eslint no-case-declarations: off */

const path = require('path');
const AExportGenerator = require('./index');

const mapPath = path.join(__dirname, '..', 'map', 'pocketgis.json');
const map = require(mapPath);

const Point = require('../../../models/point');
const fieldsInfo = {};
const defaultBlackFieldsComments = ['x', 'y', 'speed', 'dirType', 'direction', 'distance', 'angle', 'childrens'];

/**
 * генератор экспорта в формате pocketgis
 */
class PocketGisGenerator extends AExportGenerator {
  /**
   * конструктор
   *
   * @parma {Object}  i18n
   * @parma {Object}  withId
   */
  constructor(i18n, { withId = true, extendedComment = false } = {}) {
    super(i18n);

    this.withId = withId;
    this.blackFieldsComments = extendedComment ?
      defaultBlackFieldsComments :
      defaultBlackFieldsComments.concat(['rating'])
  }

  /**
   * возвращает заголовок в файле выгрузки
   */
  get header() {
    const header = super.header;

    return `IDX,X,Y,TYPE,SPEED,DirType,Direction,Distance,Angle ${header}`;
  }

  createComments(fields) {
    const comments = [];

    Object.keys(fields).forEach(field => {
      if (
        this.blackFieldsComments.includes(field) ||
        field.startsWith('__') ||
        (!this.withId && field === '_id')
      ) {
        return;
      }

      if (!Reflect.has(fieldsInfo, field)) {
        fieldsInfo[field] = Point.normalizeField({
          name: field,
          force: true
        });
      }

      const info = fieldsInfo[field];
      const value = fields[field];

      switch (info.type) {
        case 'boolean':
          if (value === true) {
            const text = this.i18n.get(info.label);

            comments.push(text);
          }
          break;
        case 'array':
          let resultArr = Array.isArray(value) ? value : [value];

          resultArr = resultArr.map(val => {
            const finded = info.list.find(item => item.value === val);

            return this.i18n.get(finded.text);
          });

          let resultValue = resultArr.join(' ');

          if (field === 'type' && Reflect.has(fields, '__pointIndex')) {
            resultValue += ` ${fields.__pointIndex}`;
          }

          comments.push(resultValue);
          break;
        case 'objectID':
        case 'string':
          comments.push(value);
          break;
      }
    });

    return comments
  }

  /**
   * @param {Object} fields поля точки
   */
  transformPoint(fields) {
    const idx = fields.idx || 0;
    const type = map.type[fields.type];
    const {x, y, speed, dirType, direction, distance, angle} = fields;
    const comments = this.createComments(fields)

    const strMainFields = [
      idx, x, y, type,
      speed, dirType, direction,
      distance, angle
    ].join(',');
    const strCommentsFields = comments.join(' | ');

    return `${strMainFields} // ${strCommentsFields}`;
  }
}

module.exports = PocketGisGenerator;
