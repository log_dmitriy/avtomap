const pocketgis = require('./generator/pocketgis');
const navitel = require('./generator/navitel');
const igo = require('./generator/igo');

module.exports = {pocketgis, navitel, igo};
