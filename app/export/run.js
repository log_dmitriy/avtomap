const generators = require('./index');

const cachedLists = {};

/**
 * Создает фильтр для точек
 *
 * @param Point
 * @param rating
 * @param confirmed
 * @param country
 * @param region
 * @param types
 * @param selectOnlyConfirmed
 *
 * @returns {Object}
 */
function createFilter(Point, {
  rating, confirmed, country, region, types, selectOnlyConfirmed
}) {
  let flagError = false;
  const errors = {};
  const addError = (name, message) => {
    flagError = true;
    errors[name] = message;
  };

  if (!cachedLists.ratings) {
    cachedLists.ratings =
      Point.normalizeField({name: 'rating', force: true}).list;
    cachedLists.types =
      Point.normalizeField({name: 'type', force: true}).list;
  }

  const filter = {};

  if (cachedLists.ratings.find(obj => obj.value === rating)) {
    filter.rating = {$gte: rating};
  }
  else {
    addError('rating', 'Неизвестное значение');
  }

  if (Array.isArray(types)) {
    for (const type of types) {
      if (!cachedLists.types.find(obj => obj.value === type)) {
        addError('types', 'Неизвестное значение');
        break;
      }
    }

    if (
      selectOnlyConfirmed &&
      confirmed &&
      types.includes('mobile_ambush')
    ) {
      const date = new Date(confirmed);
      const invalid = isNaN(date.valueOf());

      if (invalid) {
        addError('confirmed', 'Неизвестное значение');
      }
      else {
        const curDate = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          0, 0, 0
        );

        filter.$or = [
          {
            type: 'mobile_ambush',
            confirmed: {$gte: curDate}
          },
          {
            type: {
              $in: types.filter(value => value !== 'mobile_ambush')
            }
          }
        ];
      }
    }
    else {
      filter.type = {$in: types};
    }
  }
  else {
    addError('types', 'Неизвестное значение');
  }

  if (country !== '') {
    filter.country = country;
  }

  /*if (region !== '') {
   filter.region = region;
   }*/

  if (flagError === true) {
    throw errors;
  }

  return filter;
}

module.exports = async function(data, i18n, Point) {
  const format = (data.format || '').toLowerCase();

  if (!Reflect.has(generators, format)) {
    throw {format: 'Не указан формат'};
  }

  const {selectMiddlePoints, extended, withIdPoint} = data;
  const filter = createFilter(Point, data);

  const generator = new generators[format](i18n, {
    withId: withIdPoint,
    extendedComment: extended
  });
  const selectedFields = extended ? {
    updated: 0,
    location: 0,
    author: 0,
    created: 0,
    photos: 0,
  } : {
    updated: 0,
    location: 0,
    region: 0,
    country: 0,
    author: 0,
    created: 0,
    confirmed: 0,
    photos: 0,
    rating: 0
  }
  const rows = await Point.storage.getAll(filter, { fields: selectedFields });

  function fill() {
    rows.map(row => {
      const hasChildrens = Reflect.has(row, 'childrens');

      if (hasChildrens && !selectMiddlePoints && row.childrens.length > 1) {
        row.childrens = row.childrens.splice(-1);
      }

      generator.write(row);
    });
  }

  return {generator, fill};
};
