const NodeGeocoder = require('node-geocoder');
const GeoNamesProvider = require('./provider/geonames');

// https://github.com/nchaulet/node-geocoder

/** класс для работы с набором геокодеров */
class GeocodersStack {
  /**
   * конструктор
   *
   * @param {Array}  geocoders       набор геокодеров
   * @param {String} defaultGeocoder дефолтный геокодер
   */
  constructor(geocoders, defaultGeocoder = 'geonames') {
    this.geocoders = geocoders;
    this.currentGeocoder = defaultGeocoder;
  }

  /**
   * прямое геокодирование
   *
   * @param {Object} query параметры запроса
   */
  geocode(query) {
    const geocoder = this.geocoders[this.currentGeocoder];

    return geocoder.geocode(query);
  }

  /**
   * обратное геокодирование
   *
   * @param {Object} latLng координаты
   */
  async reverse(latLng) {
    const geocoder = this.geocoders[this.currentGeocoder];

    const response = await geocoder.reverse(latLng);

    return this.formatReverseResponse(response[0]);
  }

  /**
   * форматирует результат ответа на обратное гекодирование
   *
   * @param response
   *
   * @return {Object}
   */
  formatReverseResponse(response) {
    let country;
    let region;

    switch (this.currentGeocoder) {
      case 'openstreetmap':
        country = response.countryCode;
        region = response.state;
        break;
      case 'geonames':
        country = response.countryCode;
        region = response.adminName1;
        break;
      case 'google':
      default:
        country = response.country;
        region = response.administrativeLevels &&
          response.administrativeLevels.level1long;
    }

    region = region || '';

    if (region) {
      // очистить от ударений
      region = region.replace(/́/g, '');
    }

    return {country, region, response};
  }
}

module.exports = function(settings, defaultGeocoder) {
  const providers = Object.keys(settings);
  const geocoders = Object.create(null);

  providers.forEach(provider => {
    const options = JSON.parse(
      JSON.stringify(settings[provider]));

    options.provider = provider;
    options.httpAdapter = 'https';

    const geocoder = (provider === 'geonames') ?
      new  GeoNamesProvider(options) :
      NodeGeocoder(options);

    geocoders[provider] = geocoder;
  });

  return new GeocodersStack(geocoders, defaultGeocoder);
};
