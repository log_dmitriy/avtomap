const http = require('http');

/*{
  "geonames":[
    {
      "adminCode1": "11",
      "lng": "34.39341",
      "distance": "1.15255",
      "geonameId": 697079,
      "toponymName": "Plodorodnoye",
      "countryId": "690791",
      "fcl": "P",
      "population": 300,
      "countryCode": "UA",
      "name": "Plodorodnoye",
      "fclName":"city, village,...",
      "countryName": "Ukraine",
      "fcodeName":"populated place",
      "adminName1":"Republic of Crimea",
      "lat":"45.53885","fcode":"PPL"
    }
  ]
}*/

module.exports = class GeoNamesProvider {
  /**
   *
   * @param lat
   * @param lon
   * @param language
   * @returns {Promise}
   */
  reverse({lat, lon, language}) {
    return new Promise((resolve, reject) => {
      let buf = '';

      const req = http.request({
        method: 'GET',
        protocol: 'http:',
        hostname: 'api.geonames.org',
        port: 80,
        path: '/findNearbyJSON?username=openspeedcam.net&' +
          `lat=${lat}&lng=${lon}&lang=${language}`
      }, res => {
        res.on('data', chunk => {
          buf += chunk.toString();
        });
        res.on('end', () => {
          try {
            const res = JSON.parse(buf);

            resolve(res.geonames);
          }
          catch (error) {
            reject(error);
          }
        });
      });

      req.on('error', error => reject(error));

      req.end();
    });
  }
};
