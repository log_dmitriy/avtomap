/* сторонние модули */
const express = require('express');
// const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const passport = require('passport');

let cachedApp = null;

module.exports = async function(config) {
  if (!cachedApp) {
    /* маршрут к API приложения */
    const API_PATH = config.get('api.entry');
    /* путь к общей точке входа приложения */
    const INDEX_FILE = config.get('paths.index');

    /* части приложения / свои модули */
    const logger = require('./logger')(config.get('logger'));
    const staticFiles = require('./static')(config.get('paths.public'));
    const {create: createRoutesManager} = require('./managers/router');
    const {create: createMwsManager} = require('./managers/middlewares');
    const {create: createModelManager} = require('./managers/models');
    const {create: createMailer} = require('./managers/mailer');
    const {create: createParamManager} = require('./managers/parameters');
    const {create: createI18nManager} = require('./managers/i18n');
    const errors = require('./errors');
    const bodyParser = require('./bodyparser')(config.get('bodyparser'));
    const cookie = cookieParser();
    const session = require('./session')(config.get('session'));
    const cacheConnector = require('./cache');
    const dbConnector = require('./database');
    const aaaCreator = require('./aaa');
    // const icon = favicon(config.get('paths.icon'));

    // установка соединения с базой данных
    const database = await dbConnector(config.get('storage'));

    /* загрузка и регистрация middleware'ов в менеджере */
    const mwsManager = await createMwsManager(
      config.get('middlewares.path'));
    /* загрузка и регистрация обработчиков параметров в менеджере */
    const paramManager = await createParamManager(
      config.get('parameters'));
    /* загрузка и регистрация роутов в менеджере */
    const routesManager = await createRoutesManager(
      config.get('router.path'), mwsManager, paramManager, API_PATH);
    /* загрузка и регистрация моделей в менеджере */
    const modelManager = await createModelManager(
      config.get('models'), database);
    /* загрузка и регистрация локалей */
    const i18nManager = await createI18nManager(config.get('i18n'));
    /* инициализация объекта Mailer и регистрация шаблонов */
    const mailer = await createMailer(config.get('mailer'), i18nManager);
    const aaa = aaaCreator(modelManager);

    /* инициализация геокодера */
    const geocoder = require('./geocoder')(
      config.get('geocoder.list'),
      config.get('geocoder.def')
    );

    /* инициализация и заполнение кеша */
    const cache = await cacheConnector(config.get('cache'), modelManager);

    /* создание приложения */
    const app = express();

    app.request.siteurl = config.get('url.public');

    /* добавит помощников для обработки ошибок в response */
    errors.responsePatch(app);

    /* выключает заголовок x-powered-by */
    app.disable('x-powered-by');

    /* настройки приложения */
    app.set('env', config.get('env'));
    app.set('config', config);
    app.set('db', database);
    app.set('cache', cache);
    app.set('router', routesManager);
    app.set('models', modelManager);
    app.set('i18n', i18nManager);
    app.set('mailer', mailer);
    app.set('aaa', aaa);
    app.set('geocoder', geocoder);

    // app.use(icon);
    app.use(logger.access);
    app.use(logger.error);

    app.use('*', (req, res, next) => {
      if (
        req.subdomains &&
        Array.isArray(req.subdomains) &&
        req.subdomains.includes('www')
      ) {
        res.redirect(301, req.siteurl + req.originalUrl);
      }
      else {
        next();
      }
    });

    app.use(paramManager.getMiddleware());
    app.use(bodyParser);
    app.use(staticFiles);
    app.get('*', (req, res, next) => {
      const isAPI = req.originalUrl.startsWith(API_PATH);

      if (isAPI) {
        return next();
      }

      return res.sendFile(INDEX_FILE, error => {
        if (error) {
          return next(error);
        }
      });
    });
    app.use(cookie);
    app.use(session);
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(i18nManager.getMiddleware());

    if (mwsManager.has('global')) {
      app.use(mwsManager.get('global'));
    }

    /* регистрация всех маршрутов в приложении */
    routesManager.assignIn(app);

    app.use(errors.notfound);
    app.use(errors.allErrors);

    /**
     * выключение сервера и всех его зависимостей
     *
     * @return {Promise}
     */
    async function shutdown() {
      cache.quit();
      database.close();
    }

    cachedApp = {app, shutdown};
  }

  return cachedApp;
};
