const logger = require('morgan');
const rfs = require('rotating-file-stream');
const fs = require('fs');

const types = ['console', 'file'];

/**
 * @param {String} streamType   тип стримов, которые будут
 *                              использоваться в логгерах
 * @param {String} logDirectory путь к директории с файлами логов
 *
 * @return {Object} Объект с moddleware'ами логгеров
 */
module.exports = function({streamType = 'console', logDirectory}) {
  if (!types.includes(streamType)) {
    const msg = `Тип стрима для логгов ${streamType} не известен`;

    throw new TypeError(msg);
  }

  let accessLogStream = process.stdout;
  let errorLogStream = process.stderr;

  if (streamType === 'file') {
    const options = {
      // время ротации
      interval: '1d',
      // размер одного файла
      size: '10M',
      // тип сжатия
      compress: 'gzip',
      path: logDirectory,
      maxFiles: 100
    };

    // создать директорию с логами, если её нет
    if (!fs.existsSync(logDirectory)) {
      fs.mkdirSync(logDirectory);
    }

    accessLogStream = rfs('access.log', Object.assign({}, options));
    errorLogStream = rfs('error.log', Object.assign({}, options));
  }

  const access = logger('combined', {
    // вести запись по запросу, а не ответу
    immediate: false,
    // стрим для записи логов
    stream: accessLogStream,
    // функция для пропуска логгирования
    skip(req, res) {
      return res.statusCode >= 400;
    }
  });

  const error = logger('combined', {
    immediate: false,
    stream: errorLogStream,
    skip(req, res) {
      return res.statusCode < 400;
    }
  });

  return {access, error};
};
