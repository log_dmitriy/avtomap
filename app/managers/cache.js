/* модуль для работы с заполнителями кеша */

const Manager = require('./interface');

/** менеджер кешей */
class CacheManager extends Manager {
  /**
   * конструктор
   *
   * @param {String}  path    путь
   * @param {Object}  db      хранилище кеша
   * @param {Object}  models  менеджер моделей
   */
  constructor(path, db, models) {
    super(path);

    this.db = db;
    this.models = models;
  }

  /**
   * регистрирует кэш-заполнитель
   *
   * @param  {Function} cacheFn заполнитель кеша
   */
  async register(cacheFn) {
    const caches = this.get() || [];

    caches.push(cacheFn);

    await super.register(caches);
  }

  /**
   * запускает все заполнители кеша
   *
   * @return {Promise}
   */
  async runAll() {
    const caches = this.get() || [];

    for (const cacheFn of caches) {
      await cacheFn(this.db, this.models);
    }
  }
}


/* facade */
exports.create = async function(...args) {
  const manager = new CacheManager(...args);

  await manager.load();

  return manager;
};
exports.CacheManager = CacheManager;
