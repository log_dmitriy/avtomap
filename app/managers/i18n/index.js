/* модуль для работы с интернационализацией */

const SimpleIntl = require('./model');
const Manager = require('../interface');

/** менеджер интернационализации */
class I18nManager extends Manager {
  /**
   * конструктор
   *
   * @param {String}  dirPath         директория с файлами
   * @param {String}  cookieName      ключ кук
   * @param {String}  queryParameter  ключ параметра запроса
   * @param {String}  defaultLocale   локаль по-умолчанию
   *
   * @return {undefined}
   */
  constructor({
    path: dirPath,
    cookieName = 'lang',
    queryParameter = 'lang',
    defaultLocale = 'ru'
  } = {}) {
    super(dirPath);

    this.cookieName = cookieName;
    this.queryParameter = queryParameter;
    this.defaultLocale = defaultLocale;
  }

  /**
   * регистрирует локаль
   *
   * @param {Object} data  данные локали
   * @param {String} lang  название локали
   *
   * @return {undefined}
   */
  async register(data, lang) {
    const intl = new SimpleIntl(lang, data);

    await super.register(intl, lang);
  }

  /**
   * создает middleware для смены локали текущего пользователя
   *
   * @return {Function}
   */
  getMiddleware() {
    const queryParameter = this.queryParameter;
    const defaultLocale = this.defaultLocale;
    const cookieName = this.cookieName;

    return function(req, res, next) {
      const i18nManager = req.app.get('i18n');
      const query = req.query[queryParameter];
      const cookie = req.cookies[cookieName];

      let lang = defaultLocale;

      if (query !== void 0 && i18nManager.has(query)) {
        lang = query;
      }
      else if (cookie !== void 0 && i18nManager.has(cookie)) {
        lang = cookie;
      }

      if (cookie !== lang) {
        const expires = new Date();

        expires.setFullYear(expires.getFullYear() + 1);
        res.cookie(cookieName, lang, {
          expires: expires.toUTCString()
        });
      }

      req.lang = lang;
      req.i18n = i18nManager.get(lang);

      next();
    };
  }
}


/* facade */
exports.create = async function(...args) {
  const manager = new I18nManager(...args);

  await manager.load();

  return manager;
};
exports.I18nManager = I18nManager;
