/**
 * простой класс для работы с локалью
 */
class SimpleIntl {
  /**
   * конструктор
   *
   * @param {String} lang  название локали
   * @param {Object} data  данные локали
   *
   * @return {undefined}
   */
  constructor(lang, data) {
    this.lang = lang;
    this.data = data;
  }

  /**
   * возвращает фразу локали по ключу
   *
   * @param {String} key ключ фразы
   *
   * @return {String} фраза
   */
  get(key, params) {
    let str = (key in this.data) ? this.data[key] : key;

    if (params) {
      Object.keys(params).forEach(param => {
        const value = params[param];

        str = str.replace(`{${param}}`, value);
      });
    }

    return str;
  }
}

module.exports = SimpleIntl;
