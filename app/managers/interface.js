const path = require('path');
const fsp = require('fs-promise');

const defaultName = 'incognito';

/** интерфейс менеджеров, описывает общую логику их работы */
class Manager {
  /**
   * @param  {String}  pathToResources путь к ресурсам
   * @param  {Boolean} modules         являются ли ресурсы модулями
   * @param  {Boolean} recursive       проходить ли рекурсивно
   *                                   по директориям
   */
  constructor(
    pathToResources,
    {modules = true, recursive = true} = {}
  ) {
    if (!pathToResources) {
      throw TypeError('Путь к ресурсам должен быть указан!');
    }

    this.__recursive = recursive;
    this.__modules = modules;
    this.resources = new Map();
    this.path = pathToResources;
    this.blackListLoad = ['index.js', 'interface.js'];
  }

  /**
   * загружает и регистрирует все ресурсы
   *
   * @param  {String} dir     директория в которой находятся ресурсы
   * @param  {String} prefix  префикс файлов
   *
   * @return {Promise}
   */
  async load(dir, prefix = '') {
    const dirPath = dir || this.path;
    const files = await fsp.readdir(dirPath);

    for (const filename of files) {
      if (this.blackListLoad.includes(filename)) {
        continue;
      }

      const fullPath = path.join(dirPath, filename);
      const stats = await fsp.lstat(fullPath);

      if (stats.isFile()) {
        await this.__loadFile(fullPath, prefix);
      }

      if (this.__recursive && stats.isDirectory()) {
        await this.load(fullPath, `${filename}/`);
      }
    }
  }

  /**
   * загружает ресурс из файла
   *
   * @private
   *
   * @param  {String} fullPath путь к файлу
   * @param  {String} prefix   префикс файла
   *
   * @return {Promise}
   */
  async __loadFile(fullPath, prefix = '') {
    let resource;

    if (this.__modules === true) {
      resource = require(fullPath);
    }
    else {
      resource = await fsp.readFile(fullPath, {
        encoding: 'utf8',
        flag: 'r'
      });
    }

    const normalizeName = path.basename(fullPath,
      path.extname(fullPath));

    await this.register(resource, prefix + normalizeName);
  }

  /**
   * регистрирует ресурс
   *
   * @param  {*}      resource  ресурс
   * @param  {String} name      название ресурса
   *
   * @return {undefined}
   */
  async register(resource, name = defaultName) {
    this.resources.set(name, resource);
  }

  /**
   * возвращает ресурс по его названию
   *
   * @param  {String} name название
   *
   * @return {*}      ресурс
   */
  get(name = defaultName) {
    return this.resources.get(name);
  }

  /**
   * проверяет есть ли ресурс по его названию
   *
   * @param  {String} name название
   *
   * @return {Boolean}
   */
  has(name) {
    return this.resources.has(name);
  }

  /**
   * выполняет указанную функцию один раз для каждого ресурма в хранилище
   *
   * @param  {Function} fn функция
   *
   * @return {undefined}
   */
  forEach(fn) {
    this.resources.forEach(fn);
  }
}

module.exports = Manager;
