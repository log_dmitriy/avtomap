/* линтеровский jsdoc не умеет в дестрктуризацию параметров */
/* eslint valid-jsdoc: off */

const striptags = require('striptags');
const nodemailer = require('nodemailer');
//const platform = require('os').platform();

const MailerError = require('../errors/mailer');
const Manager = require('./interface');
const TMPL_PARAM_RE = /\$\{([^}]+)\}/g;

/** менеджер отправки сообщений */
class Mailer extends Manager {
  /**
   * конструктор
   *
   * @param  {Object}   options         настройки
   * @param  {String}   options.from    адрес отправителя
   * @param  {Object}   options.service настройки сервиса
   *                                    https://nodemailer.com/smtp/
   * @param  {Boolean}  options.test    тестовый режим
   * @param  {String}   options.path    путь к шаблонам
   * @param  {Function} i18nManager     менеджер интернационализации
   *
   * @return {undefined}
   */
  constructor({from, service, test, path} = {}, i18nManager) {
    super(path, {modules: false});

    if (test !== true && service === void 0) {
      throw TypeError(
        'Необходимо указать options.test или options.service');
    }

    this.i18nManager = i18nManager;

    // gmail все равно подставит адрес пользователя
    //  от которого идет отправка
    this.from = from;

    this.transport = (test === true) ?
      nodemailer.createTransport(Mailer.__testTransport) :
      nodemailer.createTransport(service);

    Mailer.__setMiddlewares(this.transport);
  }

  /**
   * отправляет письмо
   *
   * @param  {Array|String} to      адрес получателя(ей) письма
   * @param  {String}       message html текст письма
   * @param  {Object}       options настройки письма
   *                                https://nodemailer.com/message/
   *
   * @return {Promise}
   */
  send(to, message, options = {}) {
    if (to === void 0) {
      throw TypeError('Аргумент to обязателен');
    }

    if (message === void 0) {
      throw TypeError('Аргумент message обязателен');
    }

    if (Array.isArray(to)) {
      to = to.join(', ');
    }

    const mailOptions = Object.assign({
      from: this.from,
      to,
      html: message
    }, options);

    return new Promise((resolve, reject) => {
      this.transport.sendMail(mailOptions, (error, info) => {
        if (error) {
          error = new MailerError(error.message);

          return reject(error);
        }

        resolve(info);
      });
    });
  }

  /**
   * отправляет письмо с заданным шаблоном
   *
   * @param  {String} to            адрес получателя
   * @param  {String} templateName  название шаблона
   * @param  {Object} data          данные письма
   * @param  {Object} options       параметры
   *
   * @return {Promise}
   */
  sendTemplate(to, templateName, lang, data = {}, options = {}) {
    const defLang = this.i18nManager.defaultLocale;
    let template;

    if (this.has(`${lang}/${templateName}`)) {
      template = this.get(`${lang}/${templateName}`);
    }
    else if (this.has(`${defLang}/${templateName}`)) {
      template = this.get(`${defLang}/${templateName}`);
    }
    else {
      template = this.get(templateName);
    }

    const params = template.match(TMPL_PARAM_RE) || [];

    let message = template;

    params.forEach(param => {
      const normalizeName = param.slice(2, -1);
      const value = data[normalizeName] || '';

      message = message.replace(param, value);
    });

    return this.send(to, message, options);
  }

  /**
   * устанавливает обработчики на отправитель
   *
   * @param  {Object} transporter отправитель
   *
   * @return {Object}             отправитель
   */
  static __setMiddlewares(transporter) {
    transporter.use('compile', (mail, callback) => {
      if (mail.data.text === void 0) {
        mail.data.text = striptags(mail.data.html);
      }

      callback();
    });

    return transporter;
  }

  /**
   * настройки для режима тестирования
   *
   * @return {Object}
   *
   * @private
   */
  static get __testTransport() {
    return {
      // streamTransport: true,
      // newline: platform === 'win32' ? 'windows' : 'unix'
      jsonTransport: true
    };
  }
}

/* facade */
exports.create = async function(...args) {
  const manager = new Mailer(...args);

  await manager.load();

  return manager;
};
exports.Mailer = Mailer;
