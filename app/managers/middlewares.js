/* модуль для работы с middlewares'ами */

const Manager = require('./interface');
const decorateFunctionsMixin = require('../mixins/decorateFunctions');

/** менеджер middleware'ов */
class MiddlewareManager extends Manager {
  /**
   * регистрирует middleware
   *
   * @param  {Object|Function} middleware описание middleware, либо он сам
   * @param  {String}          name       название
   *
   * @return {undefined}
   */
  async register(middleware, name) {
    let mwName;

    if (typeof middleware !== 'function') {
      mwName = middleware.name;
      middleware = middleware.middleware;
    }

    name = (mwName === void 0) ? name : mwName;
    name = (name === void 0) ? middleware.name : name;

    if (!name) {
      throw TypeError('Название middleware\'а обязательно!');
    }

    middleware = this.__decorateFunction(middleware);

    const middlewares = this.get(name) || [];

    middlewares.push(middleware);

    await super.register(middlewares, name);
  }

  /**
   * Возвращает middleware'ы по именам
   *
   * @param  {Array} names название moddleware'ов
   *
   * @return {Array}
   */
  getAll(names) {
    if (!Array.isArray(names)) {
      throw TypeError('names должно быть массивом');
    }

    const middlewares = [];

    names.forEach(name => {
      let middleware;

      if (typeof name === 'function') {
        middleware = name;
      }
      else {
        middleware = this.get(name);
      }

      if (!middleware) {
        return;
      }

      middlewares.push(middleware);
    });

    return middlewares;
  }
}
Object.assign(MiddlewareManager.prototype, decorateFunctionsMixin);


/* facade */
exports.create = async function(...args) {
  const manager = new MiddlewareManager(...args);

  await manager.load();

  return manager;
};
exports.MiddlewareManager = MiddlewareManager;
