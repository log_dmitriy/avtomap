const IModel = require('./imodel');
const {
  normalize: normalizeSchema,
  isNormalized: isNormalizedSchema
} = require('./schemaNormalizer');

const validateLanguage = require('../../../lang/validation/ru.json');

IModel.setDefaultValidateOptions({language: validateLanguage});

/**
 * фабрика по созданию моделей
 */
module.exports = function factory(Model, schema, storage, decorator) {
  if (!IModel.isPrototypeOf(Model)) {
    throw new TypeError('Model должна быть унаследована от IModel!');
  }

  if (!schema) {
    throw new TypeError('Схема для модели обязательна!');
  }

  if (!isNormalizedSchema(schema)) {
    schema = normalizeSchema(schema);
  }

  Model.prototype.schema = schema;

  if (storage !== void 0) {
    Model.storage = storage;
  }

  if (decorator !== void 0) {
    Model = decorator(Model);
  }

  return Model;
};
