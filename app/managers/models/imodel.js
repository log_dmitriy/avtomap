/* eslint no-template-curly-in-string: off */
/* eslint class-methods-use-this: off */
/* eslint max-statements: off */

const Joi = require('joi');
const ModelError = require('../../errors/model');

/**
 * интерфейс для моделей
 */
class IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields = {}) {
    if (new.target === IModel) {
      throw TypeError('Нельзя получить объект интерфейса IModel');
    }

    this.fields = Object.create(null);
    this.hiddenList = [];

    this.setFields(fields);
  }

  /**
   * устанавливает поля объекта
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  setFields(fields = {}) {
    if (this.schema) {
      const result = Joi.validate(fields, this.schema, {
        abortEarly: false
      });

      if (result.value) {
        fields = result.value;
      }
    }

    Object.assign(this.fields, fields);
  }

  /**
   * возвращает поля объекта со значениями по-умолчанию
   *
   * @return {Object}
   */
  getNormalFields() {
    if (!this.schema) {
      return;
    }

    const result = Joi.validate(this.fields, this.schema, {
      abortEarly: false,
      stripUnknown: true
    });
    const values = result.value || {};

    if (!result.error) {
      return values;
    }

    result.error.details.forEach(({type, path}) => {
      if (type !== 'any.unknown') {
        return;
      }

      delete values[path];
    });

    return result.value;
  }

  /**
   * нормализует поля объекта
   * удаляет лишние поля, подставляет значения по-умолчанию
   */
  normalize() {
    const normalFields = this.getNormalFields();

    this.fields = normalFields;
  }

  /**
   * подготовливает данные для превращения в JSON строку
   *
   * @param  {Boolean|Array} withHidden включить в данные спрятанные поля
   *
   * @return {Object}
   */
  toJSON(withHidden = false) {
    let hiddenList;

    if (Array.isArray(withHidden)) {
      hiddenList = this.hiddenList.filter(
        item => !withHidden.includes(item));
    }
    else {
      hiddenList = (withHidden === false) ?
        this.hiddenList : [];
    }

    const fields = Object.create(null);

    for (const field in this.fields) {
      if (hiddenList.includes(field)) {
        continue;
      }

      fields[field] = this.fields[field];
    }

    return fields;
  }

  /**
   * проверяет данные объекта на валидность
   *
   * @param {String}  [field] поле, которое нужно отвалидировать
   */
  async validate(field) {
    if (!this.schema) {
      throw new ModelError('Для валидации нужна схема');
    }

    let schema = this.schema;

    if (field) {
      schema = Joi.reach(schema, field);
    }

    return IModel.validateFields(
      this.fields, schema, this.validateOptions);
  }

  /**
   * проверяет данные объекта на валидность
   *
   * @param {Object}  fields поля, которые нужно отвалидировать
   * @param {Object}  schema схема
   * @param {validateOptions} validateOptions
   *
   * @return {Promise}
   */
  static async validateFields(fields, schema, validateOptions) {
    return new Promise((resolve, reject) => {
      Joi.validate(fields, schema, validateOptions,
        (error, value) => {
          if (error) {
            const normalErrors = normalizeErrors(error.details);

            reject(normalErrors);

            return;
          }

          resolve(value);
        });
    });
  }

  /**
   * установка настроек валидирования
   *
   * @param options
   */
  static setValidateOptions(options) {
    this.constructor.prototype.validateOptions = Object.assign(
      IModel.defaultValidateOptions, options);
  }

  /**
   * достает название поля
   *
   * @param {String} name ключ поля
   *
   * @return {String}
   */
  static getLabelField(name) {
    const schema = this.getRawSchema();

    return schema[name].label;
  }

  /**
   * достает сырую, необработанную схему
   *
   * @returns {Object}
   */
  static getRawSchema() {
    const schema = this.prototype.schema._meta[0].rawSchema;

    return schema;
  }

  /**
   * Достает указанные нормализованные поля из схемы.
   *
   * @param {Object} [values] - объект полей со значениями
   * например:
   *  {
     *    type: 'static_cam'
     *  }
   * @param {Array|Boolean} [withHiddenFields] добавить указанные
   *                                           скрытые поля
   * @param {Object}        [defaultValues]    значения по-умолчанию
   *
   * @return {Object}
   */
  static getNormalFields(
    values = {},
    withHiddenFields = [],
    defaultValues = {}
  ) {
    const fields = Object.create(null);
    const rawFields = this.getRawSchema();

    for (const fieldKey in rawFields) {
      const rawField = rawFields[fieldKey];

      const isHidden = rawField.hidden === true &&
        withHiddenFields !== true &&
        !withHiddenFields.includes(fieldKey);

      if (isHidden) {
        continue;
      }

      const haveDependency = rawField.dependency !== void 0;
      const haveValue = values[fieldKey] !== void 0;
      const value = haveValue ? values[fieldKey] : defaultValues[fieldKey];
      let dependFieldValue;

      if (haveDependency) {
        const dependOn = rawField.dependency.field;

        if (fields[dependOn] === void 0) {
          throw new Error(`Поле ${fieldKey} зависит от ${dependOn} ` +
            'которое еще не проинициализировано');
        }

        dependFieldValue = fields[dependOn].value === void 0
          ? fields[dependOn].default
          : fields[dependOn].value;
      }

      const field = this.normalizeField({
        field: rawField,
        name: fieldKey,
        value,
        dependFieldValue
      });

      if (field === void 0) {
        continue;
      }

      fields[fieldKey] = field;
    }

    return fields;
  }

  /**
   * @param field
   * @param name
   * @param value
   * @param dependFieldValue
   * @param force
   *
   * @return {Object}
   */
  static normalizeField({field, name, value, dependFieldValue, force} = {}) {
    if (field === void 0) {
      field = this.getFieldInfo(name);
    }

    const haveDependency = field.dependency !== void 0;
    const fieldData = Object.assign(
      JSON.parse(JSON.stringify(field)), {name});

    if (haveDependency) {
      const multiple = fieldData.dependency.multiple;

      if (multiple === true) {
        const dependData = this.__normalizeOfDepend(
          fieldData.dependency.values,
          dependFieldValue,
          {merge: force}
        );

        if (dependData === void 0) {
          return;
        }

        delete fieldData.dependency;

        Object.assign(fieldData, dependData);

        if (
          value === void 0 ||
          !includeValueInList(value, dependData.list)
        ) {
          value = dependData.default;
        }
      }
      else if (
        !force &&
        fieldData.dependency.values.indexOf(dependFieldValue) < 0
      ) {
        return;
      }
      else if (value === void 0) {
        value = fieldData.dependency.default;
      }
    }

    if (fieldData.type === 'array') {
      const values = fieldData.values;

      if (values.list !== void 0) {
        fieldData.list = this.__normalizeList(
          values.list, values.type);
      }
    }
    else if (
      fieldData.list !== void 0 &&
      !Array.isArray(fieldData.list)
    ) {
      fieldData.list = this.__normalizeList(
        fieldData.list, fieldData.type);
      fieldData.type = 'array';
    }

    if (name === 'childrens') {
      fieldData.type = 'other-point';
    }

    if (name === 'confirmed') {
      fieldData.onlyUpdate = true;
    }

    return Object.assign({value}, fieldData);
  }

  /**
   * возвращает информацию о поле
   *
   * @param fieldName
   *
   * @return {Object}
   */
  static getFieldInfo(fieldName) {
    const rawFields = this.getRawSchema();

    return rawFields[fieldName];
  }

  /**
   * @param  {Object} - объект полей зависимых от другого поля.
   * @param  {*} [dependFieldValue] - значение зависимого поля.
   * @param  {Object} [opts]
   * @return {*}
   */
  static __normalizeOfDepend(dependencyValues, dependFieldValue, opts = {}) {
    const mergeMode = opts.merge;
    let fieldData;

    for (let index = 0; index < dependencyValues.length; index++) {
      const data = dependencyValues[index];

      if (mergeMode) {
        fieldData = fieldData || {list: data.list || {}};

        Object.assign(fieldData.list, data.list);
      }
      else if (data.valuesDependencies.indexOf(dependFieldValue) >= 0) {
        return data;
      }
    }

    return fieldData;
  }

  /**
   * Нормализует список.
   *
   * @param  {Object}
   * @param  {String}
   * @return {Array}
   */
  static __normalizeList(rawList, type) {
    const list = [];

    for (const item in rawList) {
      const value = (['integer', 'number'].includes(type)) ?
        parseFloat(item) : item;

      list.push({
        value,
        text: rawList[item].label
      });
    }

    return list.sort((item1, item2) => {
      const sort1 = rawList[item1.value].sort;
      const sort2 = rawList[item2.value].sort;

      return sort1 - sort2;
    });
  }
}

/**
 * установка опций валидирования по-умолчанию для всех моделей
 *
 * @param {Object} options
 */
IModel.setDefaultValidateOptions = function(options) {
  Object.assign(IModel.prototype.validateOptions, options);
};

/**
 * опции для валидвирования по-умолчанию
 *
 * @type {Object}
 */
IModel.prototype.validateOptions = {
  // обрывать проверку при первой же ошибке
  abortEarly: false,
  // true - игнорировать неизвестные ключи
  allowUnknown: false,
  // false - удалять неизвестные ключи в объектах и массивах
  stripUnknown: false // {array: false, object: true}
};

/**
 * нормализует ошибки
 *
 * @param {Array} errors
 *
 * @return {Object}
 */
function normalizeErrors(errors) {
  if (!Array.isArray(errors)) {
    throw new TypeError('Параметр errors должен быть массивом!');
  }

  const normalErrors = {};

  errors.forEach(error => {
    normalErrors[error.path] = error.message;
  });

  return normalErrors;
}

/**
 * @param value
 * @param list
 *
 * @return {boolean}
 */
function includeValueInList(value, list) {
  return (list !== void 0) && (value in list);
}

module.exports = IModel;
