/* модуль для работы с моделями приложения */
const path = require('path');
const fsp = require('fs-promise');

const Manager = require('../interface');
const ModelStorage = require('./modelstorage');
const factory = require('./factory');
const proxifier = require('./proxifier');

/**
 * менеджер моделей
 */
class ModelManager extends Manager {
  /**
   * конструктор
   *
   * @param {Object}  config    конфигурация менеджера
   * @param {*}       database  подключение к БД
   *
   * @return {undefined}
   */
  constructor(config, database) {
    super(config.path, {recursive: false});

    this.schemasConfig = config.schemas;

    this.database = database;
  }

  /**
   * регистрирует модель
   *
   * @param  {Object} Model конструктор модели
   * @param  {String} name  название модели
   *
   * @return {undefined}
   */
  async register(Model, name) {
    name = (name === void 0) ? Model.name.toLowerCase() : name;

    const schema = await this.loadSchema(name);
    const modelStorage = new ModelStorage(Model, this.database);
    const model = factory(Model, schema, modelStorage, proxifier);

    await super.register(model, name);
  }

  /**
   * Загружает схему
   *
   * @param {String}  name  название схемы
   */
  async loadSchema(name) {
    const schemaPath = path.join(
      this.schemasConfig.path, `${name}.json`);
    let schema = {};

    try {
      await fsp.stat(schemaPath);
      schema = require(schemaPath);
    }
    catch (error) {
      if (error.code !== 'ENOENT') {
        throw error;
      }
    }

    return schema;
  }
}

/* facade */
exports.create = async function(...args) {
  const manager = new ModelManager(...args);

  await manager.load();

  return manager;
};
exports.ModelManager = ModelManager;
