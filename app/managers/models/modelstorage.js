const IModel = require('./imodel');
const ModelError = require('../../errors/model');
const ObjectID = require('mongodb').ObjectID;

/** класс для синхронизации моделей с хранилищем */
class ModelStorage {
  /**
   * конструктор
   *
   * @param {Object}  Model    модель сущности
   * @param {Object}  storage  хранилище
   *
   * @return {undefined}
   */
  constructor(Model, storage) {
    if (!IModel.isPrototypeOf(Model)) {
      throw new TypeError('Аргумент Model должен быть реализацией IModel');
    }

    this.Model = Model;
    this.modelName = Model.name.toLowerCase();
    this.storage = storage.collection(this.modelName);
  }

  /**
   * возвращает сущность по фильтру
   *
   * @param filter
   * @param options
   */
  async get(filter, options) {
    const entity = await this.storage.findOne(filter, options);

    return entity || {};
  }

  /**
   * возвращает множество значений указанного поля
   *
   * @param {String}  fieldName название поля
   * @param {Object}  filter
   *
   * @return {Array}
   */
  async distinct(fieldName, filter) {
    const values = await this.storage.distinct(fieldName, filter);

    return values || [];
  }

  /**
   * возвращает кол-во элементов по фильтру
   *
   * @param filter
   *
   * @returns {Number}
   */
  async count(filter) {
    const count = await this.storage.count(filter);

    return count;
  }

  /**
   * возвращает сущность по идентификатору
   *
   * @param id
   * @param options
   */
  async getById(id, options) {
    id = this.normalizeID(id);

    return this.get({
      [this.idKey]: id
    }, options);
  }

  /**
   * возвращает набор сущностей по фильтру
   *
   * @param filter
   * @param sort
   * @param skip
   * @param limit
   * @param fields
   */
  async getAll(filter, {sort, skip, limit, agg, fields = {}} = {}) {
    let query;

    if (agg) {
      const aggMain = [{$match: filter}, ...agg];

      if (sort !== void 0) {
        aggMain.push({$sort: sort});
      }

      if (skip !== void 0) {
        aggMain.push({$skip: skip});
      }

      if (limit !== void 0) {
        aggMain.push({$limit: limit});
      }

      query = this.storage.aggregate(aggMain);
    }
    else {
      query = this.storage.find(filter, fields);

      if (sort !== void 0) {
        query = query.sort(sort);
      }

      if (skip !== void 0) {
        query = query.skip(skip);
      }

      if (limit !== void 0) {
        query = query.limit(limit);
      }
    }

    const entity = await query.toArray();

    return entity || [];
  }

  /**
   * синхронизирует модель с записью в хранилище
   *
   * @param {Object} entity сущность
   */
  async save(entity) {
    this.__checkEntity(entity);

    const id = entity.fields[this.idKey];

    if (id === void 0) {
      return this.__create(entity);
    }

    return this.__update(entity);
  }

  /**
   * удаляет запись из хранилища
   *
   * @param {Object} entity сущность
   */
  async delete(entity) {
    this.__checkEntity(entity);

    const id = entity.fields[this.idKey];

    if (id === void 0) {
      throw new ModelError(
        `Для удаления необходимо поле "${this.idKey}"`);
    }

    await this.storage.deleteOne({
      [this.idKey]: id
    });

    return entity;
  }

  /**
   * удаляет набор сущностей по фильтру
   *
   * @param filter
   */
  async deleteAll(filter) {
    await this.storage.deleteMany(filter);
  }

  /**
   * обновляет запись в хранилище
   *
   * @param {Object} entity сущность
   */
  async __update(entity) {
    const fields = Object.assign({}, entity.fields);
    const idValue = fields[this.idKey];
    const id = this.normalizeID(idValue);

    delete fields[this.idKey];

    await this.storage.updateOne({
      [this.idKey]: id
    }, fields); // {$set: fields}

    return entity;
  }

  /**
   * создает запись в хранилище
   *
   * @param {Object} entity сущность
   */
  async __create(entity) {
    const value = await this.storage.insertOne(entity.fields);

    entity.fields[this.idKey] = value.insertedId;

    return entity;
  }

  /**
   * проверяет валидна ли сущность
   *
   * @param {Object} entity сущность
   *
   * @private
   */
  __checkEntity(entity) {
    if (!(entity instanceof this.Model)) {
      throw new TypeError(
        `Аргумент entity должен быть объектом класса ${this.modelName}`);
    }
  }

  /**
   * возвращает название ключа идентификатора
   *
   * @return {String}
   */
  get idKey() {
    return '_id';
  }

  /**
   * нормализует идентификатор
   *
   * @param id
   *
   * @returns {*}
   */
  normalizeID(id) {
    //if (!ObjectID.isValid(id)) {
    if (typeof id === 'string') {
      id = new ObjectID(id);
    }

    return id;
  }
}

module.exports = ModelStorage;
