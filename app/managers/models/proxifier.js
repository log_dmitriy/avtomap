/* модуль для проксификации моделей */

const instanceHandlers = {
  get(target, key) {
    const value = target.fields[key];

    if (value !== void 0) {
      return value;
    }

    return target[key];
  },

  set(target, key, value) {
    if (key === 'fields') {
      target.fields = value;
    }
    else {
      target.fields[key] = value;
    }

    return true;
  },

  deleteProperty(target, key) {
    return delete target.fields[key];
  }
};

/**
 * проксифицирует модели
 *
 * @param {Object} Model      конструктор модели
 * @param {Object} [handlers] дополнительные перехватчики
 *
 * @returns {Proxy}
 */
module.exports = function(Model, handlers) {
  const classHandlers = {
    construct(Model, args) {
      const instance = new Model(...args);
      const instHandlers = (handlers === void 0) ?
        instanceHandlers :
        Object.assign({}, instanceHandlers, handlers);

      return new Proxy(instance, instHandlers);
    }
  };

  return new Proxy(Model, classHandlers);
};
