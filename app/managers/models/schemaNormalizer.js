const Joi = require('joi');

const changeListType = (list, type) => {
  if (['integer', 'number'].includes(type)) {
    list = list.map(item => parseFloat(item));
  }

  return list;
};

const typeMap = {
  objectID: () => Joi.any(),
  string: () => Joi.string(),
  date: () => Joi.date(),
  boolean: () => Joi.boolean(),
  number: () => Joi.number(),
  integer: () => typeMap.number().integer(),
  object: (info, stackSchemas) => {
    if (Reflect.has(info, 'values')) {
      return normalize(info.values, stackSchemas);
    }

    return Joi.object();
  },
  array: (info, stackSchemas) => {
    let type = Joi.array();

    if (Reflect.has(info, 'values')) {
      type = type.items(generateType(info.values, stackSchemas));
    }

    return type;
  },
  link: (info, stackSchemas) => {
    const {schema, path} = generatePath(info.path, stackSchemas);
    const newType = Joi.reach(schema, path);

    if (newType === void 0) {
      throw new Error(`Путь ${info.path} не найден.`);
    }

    return newType;
  },
  '*': () => Joi.any()
};

const propertyMap = {
  /* тип значения */
  type: (type, info, stackSchemas) => {
    const strType = info.type;

    if (!Reflect.has(typeMap, strType)) {
      throw new Error(`Неизвестный тип "${strType}"`);
    }

    return typeMap[info.type](info, stackSchemas);
  },

  /* значение по-умолчанию */
  default: (type, info) => {
    let def = info.default;

    if (def === 'now') {
      def = Date.now;
    }

    return type.default(def, 'description');
  },

  /* обязательное значение */
  required: (type, info) => type.required(),

  /* уникальное значение */
  unique: (type, info) => type.meta({unique: true}),

  /* является email-адресом */
  email: (type, info) => type.email(),

  /* минимальное значение */
  min: (type, info) => type.min(info.min),

  /* максимальное значение */
  max: (type, info) => type.max(info.max),

  /* точность значение (кол-во знаков после запятой) */
  precision: (type, info) => type.precision(info.precision),

  /* шаг, в который входит значение */
  step: (type, info) => type.multiple(info.step),

  /* подпись */
  label: (type, info) => type.label(info.label),

  /* описание */
  comment: (type, info) => type.description(info.comment),

  /* список возможных значений */
  whiteList: (type, info) => type.valid(info.whiteList),

  /* подписи к списку значений */
  list: (type, info) => {
    if (!Reflect.has(info, 'whiteList')) {
      const list = Object.keys(info.list);
      const whiteList = changeListType(list, info.type);

      type = type.valid(whiteList);
    }

    return type.meta({list: info.list});
  },

  /* зависимость от значения другого поля */
  dependency: (type, info, stackSchemas) => {
    const {field, values} = info.dependency;

    if (!Array.isArray(values) || !isObject(values[0])) {
      const thenSchema = addProperties(
        Joi.any(), info.dependency, stackSchemas);

      type = type.when(field, {
        is: values,
        then: thenSchema
      });
    }
    else {
      values.forEach(subInfo => {
        type = type.when(field, {
          is: subInfo.valuesDependencies,
          then: generateType(subInfo, stackSchemas)
        });
      });
    }

    return type.when(field, {
      is: Joi.any(),
      then: Joi.forbidden()
    });
  },

  /* спрятанное св-во */
  hidden: (type, info) => type.meta({hidden: true}),

  /* разрешать пустое значение */
  withEmpty: (type, info) => type.allow('')
};

/**
 * генерирует тип
 *
 * @param {Object} info         информация о типе
 * @param {Object} stackSchemas стек родительских схем
 *
 * @return {Object}
 */
function generateType(info, stackSchemas) {
  let type = propertyMap.type(void 0, info, stackSchemas);

  type = addProperties(type, info, stackSchemas);

  return type;
}

/**
 * добавляет св-ва к полю
 *
 * @param {Object}  type         поле
 * @param {Object}  info         информация о поле
 * @param {Object}  stackSchemas стек родительских схем
 *
 * @return {Object}
 */
function addProperties(type, info, stackSchemas) {
  Object.keys(info).forEach(name => {
    if (name === 'type' || !Reflect.has(propertyMap, name)) {
      return;
    }

    type = propertyMap[name](type, info, stackSchemas);
  });

  return type;
}

/**
 * проверяет нормализована ли схема
 *
 * @param {Object} schema схема
 *
 * @return Boolean
 */
function isNormalized(schema) {
  return isObject(schema) && schema.isJoi;
}

/**
 * проверяет, является ли значение объектом
 *
 * @param {*} value
 *
 * @return {Boolean}
 */
function isObject(value) {
  const typeString = Object.prototype.toString.call(value);

  return typeString === '[object Object]';
}

/**
 * генерирует путь к схеме и находит саму схему
 *
 * @param path          {String}  путь в полю в схеме
 * @param stackSchemas  {Array}   стек схем
 *
 * @return {Object}
 */
function generatePath(path, stackSchemas) {
  let schema;

  if (path.startsWith('@root.')) {
    path = path.replace('@root.', '');
    schema = stackSchemas[0];
  }
  else {
    schema = stackSchemas[stackSchemas.length - 1];
  }

  return {path, schema};
}


/**
 * нормализует схему
 *
 * @param {Object} schema         схема
 * @param {Object} [stackSchemas] стек родительских схем
 *
 * @return {Object}
 */
function normalize(schema, stackSchemas = []) {
  if (!isObject(schema)) {
    throw new TypeError('Схема должна быть объектом.');
  }

  const fields = Object.keys(schema);

  let normalSchema = Joi.object();
  const index = stackSchemas.push(normalSchema) - 1;

  for (const field of fields) {
    const info = schema[field];

    normalSchema = normalSchema.keys({
      [field]: generateType(info, stackSchemas)
    });
    stackSchemas[index] = normalSchema;
  }

  return normalSchema.meta({rawSchema: schema});
}

module.exports = {
  normalize,
  isNormalized
};
