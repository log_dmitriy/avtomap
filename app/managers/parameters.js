/* линтеровский jsdoc не умеет в дестрктуризацию параметров */
/* eslint valid-jsdoc: off */

const decorateFunctionsMixin = require('../mixins/decorateFunctions');
const Manager = require('./interface');

/** менеджер параметров маршрутов */
class ParametersManager extends Manager {
  /**
   * конструктор
   *
   * @param {String} options.path путь к директории
   */
  constructor({path}) {
    super(path);
  }

  /**
   * регистрирует обработчик параметра(ов)
   *
   * @param  {Object|Function} handler  обработчик
   * @param  {String}          key      ключ обработчика
   *
   * @return {undefined}
   */
  async register(handler, key) {
    let name;
    let join;

    if (typeof handler === 'function') {
      name = key;
      key = 'global';
    }
    else {
      name = handler.name;
      key = handler.key || key;
      join = handler.join;
      handler = handler.handler;
    }

    handler = this.__decorateFunction(handler);

    if (join === true) {
      handler = this.__joinFunction(handler, name);
    }

    const params = this.has(key) ? this.get(key) : [];

    params.push({handler, name});

    await super.register(params, key);
  }

  /**
   * сливает параметры в один обработчик
   *
   * @private
   *
   * @param {Function} fn     обработчик
   * @param {Array}    names  названия параметров
   *
   * @return {Function}
   */
  __joinFunction(fn, names) {
    if (!Array.isArray(names)) {
      throw new TypeError('Аргумент names должен быть массивом');
    }

    const flagName = `__calledFlag_${names.join('')}`;

    return function(req, res, next, value) {
      const params = req.params;

      if (req[flagName]) {
        next();

        return;
      }

      for (const name of names) {
        if (!(name in params)) {
          next();

          return;
        }
      }

      req[flagName] = true;
      fn(req, res, next, params);
    };
  }

  /**
   * присоединяет параметры к маршрутам/приложению
   *
   * @param  {Object}       application express-приложение / express-роут
   * @param  {String|Array} keys        ключ с параметрами и их обработчиками
   *
   * @return {undefined}
   */
  assignIn(application, keys = 'global') {
    if (!Array.isArray(keys)) {
      const params = this.get(keys);

      this.__assign(application, params, keys);

      return;
    }

    keys.forEach(key => {
      const params = this.get(key);

      this.__assign(application, params, key, key);
    });
  }

  /**
   * присоединяет параметры к маршрутам/приложению
   *
   * @param {Object} application
   * @param {Object} params
   * @param {String} key
   * @param {String} prefix
   *
   * @private
   */
  __assign(application, params, key, prefix = '') {
    if (!params) {
      return;
    }

    params.forEach(({handler, name}) => {
      // обычные роуты не поддерживают массивы параметров
      if (Array.isArray(name) && key !== 'global') {
        name.forEach(nm => add(nm, handler));
      }
      else {
        add(name, handler);
      }
    });

    /**
     * добавляет параметр к приложению
     *
     * @param name
     * @param handler
     */
    function add(name, handler) {
      if (prefix) {
        name = `${prefix}_${name}`;
      }

      application.param(name, handler);
    }
  }

  /**
   * возвращает middleware для инициализации работы с параметрами
   *
   * @returns {Function}
   */
  getMiddleware() {
    return function(req, res, next) {
      req.parameters = {};
      next();
    };
  }
}
Object.assign(ParametersManager.prototype, decorateFunctionsMixin);

/* facade */
exports.create = async function(...args) {
  const manager = new ParametersManager(...args);

  await manager.load();

  return manager;
};
exports.ParametersManager = ParametersManager;
