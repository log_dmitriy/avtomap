const express = require('express');
const Manager = require('./interface');
const decorateFunctionsMixin = require('../mixins/decorateFunctions');

const SEP_DUBLICATE = /\/+/g;

/**
 * менеджер роутов
 */
class RoutesManager extends Manager {
  /**
   * @param  {String} routesPath    путь к маршрутам
   * @param  {Object} mwsManager    менеджер middleware'ов
   * @param  {Object} paramManager  менеджер параметров
   * @param  {String} rootMount     точка монтирования корня
   */
  constructor(routesPath, mwsManager, paramManager, rootMount = '/api/') {
    super(routesPath);

    this.routes = new Map();
    this.names = new Map();
    this.mwsManager = mwsManager;
    this.paramManager = paramManager;
    this.rootMount = rootMount;
  }

  /**
   * регистрирует роутер
   *
   * @param  {Object} route     маршрутизатор и адрес монтирования
   *
   * @return {undefined}
   */
  async register(route) {
    const {router: routes, mount='/', paramsKey} = route;
    const storageRoutes = this.has(mount) ? this.get(mount) : [];

    let router = routes;

    if (Object.getPrototypeOf(routes) !== express.Router) {
      this.addNamedRoutes(router, mount);

      router = this.buildExpressRouter(routes, paramsKey);
    }

    storageRoutes.push(router);

    await super.register(storageRoutes, mount);
  }

  /**
   * добавляет именовыенные маршруты
   *
   * @param {Object} routes именованные роуты
   * @param {String} mount  адрес монтирования маршрута
   *
   * @return {undefined}
   */
  addNamedRoutes(routes, mount='/') {
    const names = Object.keys(routes);

    names.forEach(name => {
      const {path: routePath='/'} = routes[name];

      if (typeof routePath !== 'string') {
        return;
      }

      const re = /(:\w+)/g;
      const urlItems = [this.rootMount, mount];
      const paramIndexes = Object.create(null);

      let lastIndex = 0;
      let param;

      while ((param = re.exec(routePath))) {
        const prevString = routePath.substring(lastIndex, param.index);
        const normalParam = param[0].replace(':', '');

        urlItems.push(prevString);
        urlItems.push(param[0]);

        paramIndexes[normalParam] = urlItems.length - 1;

        lastIndex = re.lastIndex;
      }

      const endString = routePath.substring(lastIndex);

      if (endString) {
        urlItems.push(endString);
      }

      this.names.set(name, {
        urlItems,
        paramIndexes
      });
    });
  }

  /**
   * возвращает путь по имени и параметрам
   *
   * @param  {String} name   название маршрута
   * @param  {Object} params параметры маршруты
   *
   * @return {String}        путь
   */
  getUrl(name, params = {}) {
    const parts = this.names.get(name);

    if (!parts) {
      return '';
    }

    const {urlItems, paramIndexes} = parts;
    const items = Array.from(urlItems);

    Object.keys(params).forEach(param => {
      const index = paramIndexes[param];

      if (!index && index !== 0) {
        return;
      }

      items[index] = params[param];
    });

    return items.join('').replace(SEP_DUBLICATE, '/');
  }

  /**
   * возвращает название роута текущего запроса в express-приложении
   *
   * @param {http.IncomingMessage} request запрос
   *
   * @return {String}
   */
  getRouteName(request) {
    const route = request.route;

    return this.routes.get(route);
  }

  /**
   * присоединяет роуты к express-приложению
   *
   * @param {Object} application express-приложение
   *
   * @return {undefined}
   */
  assignIn(application) {
    const rootRouter = express.Router();

    if (this.paramManager) {
      this.paramManager.assignIn(rootRouter);
    }

    this.forEach((routers, mount) => {
      routers.forEach(router => rootRouter.use(mount, router));
    });

    application.use(this.rootMount, rootRouter);
  }

  /**
   * экспортирует все именованные маршруты
   *
   * @return {Object} карта именованных маршрутов
   */
  exportRoutes() {
    const exportedMap = Object.create(null);

    this.names.forEach((params, name) => {
      exportedMap[name] = this.getUrl(name);
    });

    return exportedMap;
  }

  /**
   * делает express роутер из объекта описывающего именованные роуты
   *
   * @param  {Object} namedRoutes именованные роуты
   * @param  {String} paramsKey   ключ к параметрам
   *
   * @return {Object}             express router
   */
  buildExpressRouter(namedRoutes, paramsKey) {
    const router = express.Router();
    const names = Object.keys(namedRoutes);

    if (this.paramManager && paramsKey) {
      this.paramManager.assignIn(router, paramsKey);
    }

    names.forEach(name => {
      let {
        method = 'get',
        path = '/',
        middlewares = [],
        handlers = []
      } = namedRoutes[name];

      if (this.mwsManager) {
        middlewares = this.mwsManager.getAll(middlewares);
      }

      handlers = this.__decorateFunctions(handlers);

      router[method](path, middlewares, handlers);



      const lastIndexRoute = router.stack.length - 1;
      const route = router.stack[lastIndexRoute].route;

      this.routes.set(route, name);
    });

    return router;
  }
}
Object.assign(RoutesManager.prototype, decorateFunctionsMixin);

/* facade */
exports.create = async function(...args) {
  const manager = new RoutesManager(...args);

  await manager.load();

  return manager;
};
exports.RoutesManager = RoutesManager;
