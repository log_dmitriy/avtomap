/*eslint prefer-rest-params: "off"*/

module.exports = {
  /**
   * декорирует функции
   *
   * @param  {Array}  functions функции для декорирования
   *
   * @return {Array} декорированные функции
   */
  __decorateFunctions(functions = []) {
    const result = functions.map(this.__decorateFunction.bind(this));

    return result;
  },

  /**
   * декорирует функцию
   *
   * @param  {Function}  fn декорируемая функция
   *
   * @return {Function} декорированная функция
   */
  __decorateFunction(fn) {
    const typestr = Object.prototype.toString.call(fn);

    if (typestr === '[object AsyncFunction]') {
      fn = this.__asyncWrapper(fn);
    }

    return fn;
  },

  /**
   * оборачивает асинхронную функцию в обертку
   *   для работы в express-стиле
   *
   * @param  {Function} func  асинхронная функция
   *
   * @return {Function} обернутая функция
   */
  __asyncWrapper(func) {
    return function(req, res, next) {
      func(...arguments)
        .catch(next);
    };
  }
};
