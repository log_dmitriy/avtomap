/* eslint max-params: off */
/* eslint max-len: off */

const moment = require('moment');

const sendChangePoint = async (
  point, pointCL, models, mailer, siteurl, i18n
) => {
  try {
    const PointNotification = models.get('point_notification');

    const notifications = await PointNotification.storage.getAll({
      pointId: point._id
    });

    if (!notifications.length) {
      return;
    }

    const User = models.get('user');
    const userIds = notifications.map(notif => notif.userId)
      .filter(uid => String(uid) !== String(pointCL.author._id));

    const users = await User.storage.getAll({
      _id: {$in: userIds},
      pointNotification: true
    }, {
      fields: {email: 1}
    });

    if (!users.length) {
      return;
    }

    const authorUrl = `${siteurl}/profile`;
    const pointUrl = `${siteurl}/?center=${point.x},${point.y}&zoom=16&point=${point._id}`;

    pointCL.setLocaleTransformer(i18n.get.bind(i18n));

    const settings = ['add_point', 'ru', {
      id: pointCL.pointId,
      created: moment(pointCL.created).format('DD.MM.YYYY HH:mm'),
      authorLogin: pointCL.author.login,
      text: pointCL.toFieldsString(),

      authorUrl,
      pointUrl
    }, {subject: 'Openspeedcam.net объект изменен'}];
    const promises = users.map(({email}) =>
      mailer.sendTemplate(email, ...settings));

    await Promise.all(promises);
  }
  catch (error) {
    console.error(error);
  }
};

const sendAddComment = async (
  point, user, comment, models, mailer, siteurl
) => {
  try {
    const PointNotification = models.get('point_notification');

    const notifications = await PointNotification.storage.getAll({
      pointId: point._id
    });

    if (!notifications.length) {
      return;
    }

    const User = models.get('user');
    const userIds = notifications.map(notif => notif.userId)
      .filter(uid => String(uid) !== String(user._id));

    const users = await User.storage.getAll({
      _id: {$in: userIds},
      pointNotification: true
    }, {
      fields: {email: 1}
    });

    if (!users.length) {
      return;
    }

    const authorUrl = `${siteurl}/profile`;
    const commentUrl = `${siteurl}/?zoom=16&center=${point.x},${point.y}&point=${point._id}&comments=${point._id}&comment=${comment._id}`;

    const settings = ['add_comment', 'ru', {
      id: comment._id,
      created: moment(comment.created).format('DD.MM.YYYY HH:mm'),
      text: comment.text,
      authorLogin: comment.author.login,

      authorUrl,
      commentUrl
    }, {subject: 'Openspeedcam.net новый комментарий объекта'}];
    const promises = users.map(({email}) =>
      mailer.sendTemplate(email, ...settings));

    await Promise.all(promises);
  }
  catch (error) {
    console.error(error);
  }
};

module.exports = {sendChangePoint, sendAddComment};
