/* модуль для работы с сессиями приложения */

const session = require('express-session');
const RedisStore = require('connect-redis')(session);

module.exports = function({
  secret, name, resave, saveUninitialized, storeOptions
} = {}) {
  const store = new RedisStore(storeOptions);

  return session({
    cookie: {
      name,
      path: '/',
      httpOnly: true,
      secure: false,
      maxAge: 1 * 24 * 60 * 60 * 1000 // сутки
    },
    name,
    secret,
    resave,
    saveUninitialized,
    store
  });
};
