/**
 * модуль для отдачи статических файлов
 */

const express = require('express');

module.exports = function(publicPath, options = {}) {
  return express.static(publicPath, Object.assign(options, {
    // Отправка файла индекса каталога.
    index: false,
    // Установка альтернативных вариантов расширений файлов.
    extensions: false, // ['html', 'html']
    // Генерации etag
    etag: false,
    // Предоставление файлов с точкой.
    //  allow, deny, ignore
    dotfiles: 'ignore',
    // Установка в заголовке Last-Modified даты последнего
    //  изменения файла в ОС. 
    lastModified: true,
    // Установка значения свойства max-age в заголовке Cache-Control
    maxAge: 0,
    // Перенаправление к заключительному символу /,
    //  если имя пути - это каталог.
    redirect: false
    // Функция для установки заголовков HTTP, предоставляемых с файлом.
    /*setHeaders: function (res, path, stat) {
      res.set('header', 'value');
    }*/
  }));
};
