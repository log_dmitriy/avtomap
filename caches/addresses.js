
module.exports = async function(cache, models) {
  const Point = models.get('point');
  const countries = await Point.storage.distinct('country');
  const regions = await Point.storage.distinct('region', {country: 'RU'});

  if (countries.length > 0) {
    await cache.addInSet('countries', ...countries);
  }

  if (regions.length > 0) {
    await cache.addInSet('regions', ...regions);
  }
};
