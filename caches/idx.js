
module.exports = async function(cache, models) {
  const Point = models.get('point');
  const lastPoint = await Point.storage.get({}, {
    sort: {idx: -1}
  });

  const lastIdx = (lastPoint && lastPoint.idx) ? lastPoint.idx : 0;

  await cache.setValue('last_idx', lastIdx);
};
