const path = require('path');
const fsp = require('fs-promise');
const convict = require('convict');

const rootPath = path.join(__dirname, '..');
const publicPath = path.join(rootPath, 'public');
const uploadsPath = path.join(publicPath, 'uploads');

/**
 * абсолютизирует путь
 *
 * @param {String} value путь
 *
 * @return {String}
 */
function coercePath(value) {
  if (!path.isAbsolute(value)) {
    value = path.join(rootPath, value);
  }

  return value;
}
/* директории */
convict.addFormat({
  name: 'path.directory',
  validate(value) {
    let stat;

    try {
      stat = fsp.lstatSync(value);
    }
    catch (error) {
      if (error.code === 'ENOENT') {
        fsp.mkdirpSync(value);
        stat = fsp.lstatSync(value);
      }
      else if (error.code === 'EACCES') {
        throw new Error(`Нету доступа к директории ${value}!`);
      }
      else {
        throw error;
      }
    }

    if (!stat.isDirectory()) {
      throw new Error(`${value} не является директорией!`);
    }
  },
  coerce: coercePath
});
/* файлы */
convict.addFormat({
  name: 'path.file',
  validate(value) {
    let stat;

    try {
      stat = fsp.lstatSync(value);
    }
    catch (error) {
      if (error.code === 'ENOENT') {
        throw new Error(`Файл ${value} не существует!`);
      }

      if (error.code === 'EACCES') {
        throw new Error(`Нету доступа к файлу ${value}!`);
      }
    }

    if (!stat.isFile()) {
      throw new Error(`${value} не является файлом!`);
    }
  },
  coerce: coercePath
});

const config = convict({
  env: {
    doc: 'Среда выполнения приложения.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
    arg: 'env'
  },

  url: {
    public: {
      doc: 'Внешний адрес приложения.',
      format: String,
      default: 'http://localhost'
    }
  },
  api: {
    entry: {
      doc: 'Точка входа к api приложения.',
      format: String,
      default: '/api/'
    }
  },

  paths: {
    root: {
      doc: 'Путь к корню приложения.',
      format: 'path.directory',
      default: rootPath
    },
    public: {
      doc: 'Путь к публичным файлам.',
      format: 'path.directory',
      default: publicPath
    },
    index: {
      doc: 'Точка входа приложения.',
      format: String,
      default: path.join(publicPath, 'index.html')
    },
    files: {
      doc: 'Путь к загруженным файлам.',
      format: 'path.directory',
      default: uploadsPath
    }/*,
    icon: {
      doc: 'Путь к иконке сайта.',
      format: 'path.file',
      default: path.join(publicPath, 'favicon.ico')
    }*/
  },

  router: {
    path: {
      doc: 'Путь к корню маршрутам.',
      format: 'path.directory',
      default: path.join(rootPath, 'routes')
    }
  },

  middlewares: {
    path: {
      doc: 'Путь к корню middleware\'ов.',
      format: 'path.directory',
      default: path.join(rootPath, 'middlewares')
    }
  },

  models: {
    path: {
      doc: 'Путь к корню моделей.',
      format: 'path.directory',
      default: path.join(rootPath, 'models')
    },
    schemas: {
      path: {
        doc: 'Путь к корню схем.',
        format: 'path.directory',
        default: path.join(rootPath, 'models', 'schemas')
      }
    }
  },

  server: {
    port: {
      doc: 'Номер порта веб-сервера.',
      format: 'port',
      env: 'PORT',
      default: 8085
    },
    host: {
      doc: 'Адрес хоста веб-сервера.',
      format: String,
      default: 'localhost'
    }
  },

  bodyparser: {
    maxFields: {
      doc: 'Максимальное количество полей',
      format: Number,
      default: 1000
    },
    maxFieldsSize: {
      doc: 'Максимальный размер полей (байт)',
      format: Number,
      default: 1 * 1024 * 1024
    },
    maxFileSize: {
      doc: 'Максимальный размер файла (байт)',
      format: Number,
      default: 4 * 1024 * 1024
    },
    uploadDir: {
      doc: 'Директория для загрузки файлов.',
      format: 'path.directory',
      default: path.join(uploadsPath, 'tmp')
    },
    multiples: {
      doc: 'Множественная загрузка файлов.',
      format: Boolean,
      default: true
    }
  },

  logger: {
    streamType: {
      doc: 'Тип стрима для вывода данных.',
      format: String,
      default: 'console'
    },
    logDirectory: {
      doc: 'Директория с публичными файлами.',
      format: 'path.directory',
      default: path.join(rootPath, 'logs')
    }
  },

  cache: {
    port: {
      doc: 'Номер порта хранилища.',
      format: 'port',
      default: 6379
    },
    host: {
      doc: 'Адрес хоста хранилища.',
      format: String,
      default: 'localhost'
    },
    db: {
      doc: 'Номер хранилища.',
      format: Number,
      default: 1
    },
    managerPath: {
      doc: 'Путь к корню заполнителей кеша.',
      format: 'path.directory',
      default: path.join(rootPath, 'caches')
    }
  },

  session: {
    secret: {
      doc: 'Соль для значения ключа сессии.',
      format: String,
      default: 'f346f1dd8679'
    },
    name: {
      doc: 'Название ключа сессии.',
      format: String,
      default: 'avtomap.sid'
    },
    resave: {
      doc: 'Пересохранение сессии, даже если не было изменений.',
      format: Boolean,
      default: false
    },
    saveUninitialized: {
      doc: 'Сохранение неинициализированной (новой и не измененной) сессии.',
      format: Boolean,
      default: false
    },

    storeOptions: {
      port: {
        doc: 'Номер порта хранилища.',
        format: 'port',
        default: 6379
      },
      host: {
        doc: 'Адрес хоста хранилища.',
        format: String,
        default: 'localhost'
      },
      db: {
        doc: 'Номер хранилища.',
        format: Number,
        default: 0
      },
      pass: {
        doc: 'Пароль для хранилища.',
        format: String,
        default: ''
      },
      prefix: {
        doc: 'Префикс для ключей.',
        format: String,
        default: 'sess:'
      }
    }
  },

  storage: {
    host: {
      doc: 'Адрес сервера БД.',
      format: String,
      default: 'localhost'
    },
    port: {
      doc: 'Порт сервера БД.',
      format: 'port',
      default: 27017
    },
    database: {
      doc: 'Название БД',
      format: String,
      default: ''
    },
    user: {
      doc: 'Имя пользователя.',
      format: String,
      default: ''
    },
    password: {
      doc: 'Пароль пользователя.',
      format: String,
      default: ''
    }
  },

  mailer: {
    from: {
      doc: 'Адрес отправителя.',
      format: String,
      default: 'avtomap'
    },
    test: {
      doc: 'Тестовый режим, для работы локально.',
      format: Boolean,
      default: false
    },
    service: {
      doc: 'Настройки сервиса SMTP.',
      format: Object,
      default: void 0
    },
    path: {
      doc: 'Путь к корню шаблонов писем.',
      format: 'path.directory',
      default: path.join(rootPath, 'mail_templates')
    }
  },

  parameters: {
    path: {
      doc: 'Путь к корню обработчиков параметров.',
      format: 'path.directory',
      default: path.join(rootPath, 'parameters')
    }
  },

  i18n: {
    path: {
      doc: 'Путь к корню файлов интернационализации.',
      format: 'path.directory',
      default: path.join(rootPath, 'lang')
    },
    cookieName: {
      doc: 'Название ключа в cookie',
      format: String,
      default: 'lang'
    },
    queryParameter: {
      doc: 'Название ключа в запроса',
      format: String,
      default: 'lang'
    },
    defaultLocale: {
      doc: 'Локаль по-умолчанию',
      format: String,
      default: 'ru'
    }
  },

  services: {
    google: {
      api: {
        doc: 'API гугл карт',
        format: String,
        default: ''
      }
    }
  },

  geocoder: {
    list: {
      google: {
        apiKey: {
          doc: 'Ключ к API',
          format: String,
          default: ''
        }
      },
      openstreetmap: {
        email: {
          doc: 'Контактный адрес',
          format: String,
          default: ''
        }
      },
      geonames: {
        name: 'openspeedcam.net'
      }
    },
    def: {
      doc: 'Геокодер по-умолчанию',
      type: String,
      default: 'geonames'
    }
  }
});

const env = config.get('env');
const envFilePath = path.join(__dirname, `${env}.json`);

config.loadFile(envFilePath);

config.validate({allowed: true});

module.exports = config;
