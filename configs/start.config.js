const path = require('path');

const mainApplication = {
  // название приложения
  name: 'avtomap',
  // рабочая директория приложения
  cwd: path.join(__dirname, '..'),
  // путь к скрипту относительно "pm2 start"
  script: path.join('bin', 'www'),
  // аргументы приложения
  args: '',
  // абсолютный путь к интерпретатору
  interpreter: 'node',
  // аргументы интерпретатора
  interpreter_args: '',
  // аргументы для интерпретатора node.js
  node_args: '',

  // количество экземпляров приложения
  // 1 = 1ядро, 0 == CPU, -1 == CPU-1 etc...
  instances: 1,
  // режим запуска приложения ("cluster", "fork")
  exec_mode: 'cluster',
  // отслеживание изменения файлов в приложении и перезапуск его
  watch: false, // []
  // игнорирование файлов для отслеживания
  ignore_watch: ['\/\\]\./', 'node_modules'],
  // перезапуск приложения при превышении квоты памяти
  max_memory_restart: void 0, // "150M"
  // переменные окружения приложения
  env: {
    DEBUG: 'avtomap:*',
    NODE_ENV: 'development'
  },
  // inject when doing pm2 restart app.yml --env <envname>
  env_production: {NODE_ENV: 'production'},
  // поддержка source map файла когда выскакивает ошибка
  source_map_support: false,

  // формат даты логов
  log_date_format: 'YYYY-MM-DD HH:mm Z',
  // путь к файлу с ошибками
  error_file: null,
  // путь к файлу с сообщениями
  out_file: null,
  log_file: path.join('logs', 'pm_log.log'),
  // if set to true, avoid to suffix logs/pid file with the process id
  combine_logs: false,
  // тоже что и combine_logs
  merge_logs: false,
  // путь к файлу pid
  pid_file: void 0,
  // формат логов
  log_type: 'json',

  // min uptime of the app to be considered started
  min_uptime: void 0, // "1M"
  // время принудительной перезагрузки, если приложение не слушает
  listen_timeout: void 0, // 8000 // ms
  // время перед отправкой сигнала final SIGKILL
  kill_timeout: void 0, // 1600 // ms
  // считать приложение запущенным после process.send('ready')
  wait_ready: true,
  // максимальное кол-во перезагрузок
  max_restarts: 10,
  // время между перезагрузками
  restart_delay: 4000, // ms
  // перезапуск после краха приложения
  autorestart: true,
  // перезапуск приложения по крону
  cron_restart: void 0, // "1 0 * * *"
  // start wit vizion features (versioning control metadatas)
  vizion: true,
  // a list of commands which will be executed after you perform
  //  a Pull/Upgrade operation from Keymetrics dashboard
  post_update: [],
  //  if true, you can start the same script several times which
  //    is usually not allowed by PM2
  force: false
};

const commonDeploy = {
  user: '<username>',
  host: '<ipaddress>',
  repo: 'git@bitbucket.org:log_dmitriy/avtomap.git',
  ssh_options: ['StrictHostKeyChecking=no', 'PasswordAuthentication=no'],
  'pre-deploy-local': 'npm run test',
  'pre-setup': 'apt-get install git',
  'post-setup': 'ls -la'
};
const deploy = {
  production: Object.assign({
    ref: 'origin/master',
    path: '<path/to/project/production>',
    'post-deploy': 'npm i && npm start --env production'
  }, commonDeploy),
  dev: Object.assign({
    ref: 'origin/develop',
    path: '<path/to/project/development>',
    'post-deploy': 'npm i && npm start'
  }, commonDeploy)
};

module.exports = {
  apps: [mainApplication],
  deploy
};
