const path = require('path');
const webpack = require('webpack');
const utils = require('./utils');
const config = require('../../configs');

const sep = `\\${path.sep}`;
const isProd = process.env.NODE_ENV === 'production';
const PUBLIC_URL = config.get('url.public');
const GOOGLE_API = config.get('services.google.api');

const nodeScriptsPath = utils.frontend('./node-scripts');

module.exports = {
  devtool: '#cheap-module-eval-source-map',
  entry: {
    app: utils.frontend('./src/app.js')
  },
  output: {
    path: utils.public(),
    filename: 'js/[name].js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      '@': utils.frontend('./src'),
      'node-scripts': nodeScriptsPath
    }
  },
  resolveLoader: {
    modules: [
      'node_modules',
      utils.frontend('./loaders')
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      },
      __PUBLIC_URL__: JSON.stringify(PUBLIC_URL),
      __GOOGLE_API__: JSON.stringify(GOOGLE_API)
    })
  ],
  node: {
    global: true,
    Buffer: true,
    crypto: 'empty',
    net: 'empty',
    dns: 'empty'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          extractCSS: isProd,
          preserveWhitespace: false,
          postcss: [
            require('autoprefixer')({
              browsers: ['last 3 versions']
            })
          ],
          loaders: {
            scss: 'vue-style-loader!css-loader!sass-loader'
          }
        }
      },
      {
        test: /\.css$/,
        loader: 'css-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          utils.frontend('.'),
          utils.resolve('./app'),
          utils.resolve('./models'),
          new RegExp(
            `${sep}node_modules${sep}(hoek|joi|isemail|topo|vue-)${sep}`)
        ],
        query: {
          plugins: [
            'transform-es2015-arrow-functions',
            'transform-es2015-classes',
            'transform-new-target'
          ]
        }
      },
      {
        test: /\.js$/,
        loader: 'node-script-loader',
        include: [nodeScriptsPath]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'file-loader',
        options: {
          limit: 10000,
          name: 'img/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'fonts/[name].[hash:7].[ext]'
        }
      }
    ]
  }
};
