const merge = require('webpack-merge');
const utils = require('./utils');
const baseConfig = require('./base');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

module.exports = merge(baseConfig, {
  devtool: 'inline-cheap-source-map',
  plugins: utils.webpackRemovePlugin.concat([
    ...baseConfig.plugins,
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: utils.frontend('./template/index.template.html'),
      inject: true
    }),
    new FriendlyErrorsPlugin()
  ])
});
