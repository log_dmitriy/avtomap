const merge = require('webpack-merge');
const path = require('path');
const utils = require('./utils');
const webpack = require('webpack');
const baseConfig = require('./base');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(baseConfig, {
  devtool: '#source-map',
  output: {
    path: utils.public(),
    filename: 'js/[name].[chunkhash].js',
    chunkFilename: 'js/[id].[chunkhash].js',
    publicPath: '/'
  },
  plugins: utils.webpackRemovePlugin.concat([
    ...baseConfig.plugins,
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: utils.frontend('./template/index.template.html'),
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      },
      chunksSortMode: 'dependency'
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].[contenthash].css'
    }),
    new OptimizeCSSPlugin({
      cssProcessorOptions: {
        safe: true
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks(module, count) {
        return (
          module.resource &&
          /\.js$/.test(module.resource) &&
          module.resource.indexOf(
            path.join(__dirname, '../../node_modules')
          ) === 0
        );
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
      chunks: ['vendor']
    }),
    new UglifyJSPlugin({
      sourceMap: false,
      uglifyOptions: {
        ie8: false,
        warnings: false
      }
    })
  ])
});
