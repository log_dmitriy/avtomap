const merge = require('webpack-merge');
const baseConfig = require('./base');

const config = merge(baseConfig, {
  devtool: '#inline-source-map',
  plugins: [
    ...baseConfig.plugins
  ]
});

delete config.entry;

module.exports = config;
