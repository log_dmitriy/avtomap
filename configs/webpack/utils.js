const path = require('path');
const RemoveWebpackPlugin = require('remove-webpack-plugin');

exports.resolve = (file = './') =>
  path.resolve(__dirname, './../../', file);

exports.frontend = (file = './') =>
  exports.resolve(path.resolve('./frontend', file));

exports.public = (file = './') =>
  exports.resolve(path.resolve('./public', file));

exports.webpackRemovePlugin = [
  new RemoveWebpackPlugin(exports.public('./js'), 'hide'),
  new RemoveWebpackPlugin(exports.public('./css'), 'hide'),
  new RemoveWebpackPlugin(exports.public('./fonts'), 'hide'),
  new RemoveWebpackPlugin(exports.public('./img'), 'hide'),
  new RemoveWebpackPlugin(exports.public('./index.html'), 'hide')
];
