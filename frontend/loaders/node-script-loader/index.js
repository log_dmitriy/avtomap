module.exports = function(content) {
  this.cacheable(true);

  const callback = this.async();
  const result = require(this.resourcePath);
  const promise = (result && result.then) ?
    result : Promise.resolve(result);

  promise.then(data => {
    const strData = JSON.stringify(data);

    callback(null, `module.exports = ${strData};`);
  })
  .catch(error => callback(error));
};

// перед вызовом loader'а
// module.exports.pitch = function(remainingRequest, precedingRequest, data) {}

// чтобы автоматически не конвертировало в utf-8
// module.exports.raw = true;
