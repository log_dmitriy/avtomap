const config = require('../../configs');
const {create: createRtMan} = require('../../app/managers/router');
const {create: createMwMan} = require('../../app/managers/middlewares');
const dirPath = config.get('router.path');
const dirMwPath = config.get('middlewares.path');

module.exports = (async function() {
  const mwManager = await createMwMan(dirMwPath);
  const manager = await createRtMan(dirPath, mwManager);

  return manager.exportRoutes();
})();
