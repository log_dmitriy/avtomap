const userSchema = require('../../models/schemas/user');

module.exports = (async function() {
  const roles = userSchema.roles.values.list;
  const items = Object.keys(roles).map(name => {
    const item = {
      text: roles[name].label,
      value: name
    };

    return item;
  });

  return items;
})();
