import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App.vue';
import store from './store';
import router from './router';
import {sync} from 'vuex-router-sync';
import VueResource from 'vue-resource';
import VueI18n from 'vue-i18n';
import VueMoment from '@/plugins/moment';
import {getLang} from '@/libs/lang';

const lang = getLang();

Vue.config.productionTip = false;
Vue.config.errorHandler = function(error, vm, info) {
  store.dispatch('applicationError', error);

  console.error(error, vm, info);
};

Vue.use(VueMoment, {locale: lang});
Vue.use(VueI18n);
Vue.use(Vuetify);
Vue.use(VueResource);

sync(store, router);

const i18n = new VueI18n({locale: lang});
const app = new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
});

router.onReady(() => {
  store.dispatch('switchLanguage', lang)
    .then(() => {
      i18n.setLocaleMessage(lang, store.getters.getLanguageMessages);
    })
    .then(() => store.dispatch('account/checkAuthenticate'))
    .then(() => {
      document.getElementById('loader').remove();
      document.getElementById('style-loader').remove();
      app.$mount('#app');
    });
});
