export default {
  model: {
    prop: 'value',
    event: 'change'
  },
  props: {
    info: {
      type: Object,
      required: true
    },
    value: {}
  },
  watch: {
    value(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }

      if (this.filterValue !== void 0) {
        newValue = this.filterValue(newValue);
      }

      this.proxyValue = newValue;
    },
    proxyValue(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }

      this.$emit('change', newValue);
    }
  },
  data() {
    return {
      proxyValue: this.value
    };
  },
  methods: {
    translateList(list) {
      return list.map(item => {
        item.text = this.$i18n.t(item.text);

        return item;
      });
    }
  }
};
