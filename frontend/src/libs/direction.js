/* eslint-disable */

/**
 * @return {Number}
 */
function radians(value) {
  return value * (Math.PI / 180);
}

/**
 * @return {Number}
 */
function degrees(value) {
  return value * (180 / Math.PI);
}

/**
 * @return {Number}
 */
export const getDistance = function(positionA, positionB) {
  const dist = 6378137;
  const fb = radians(positionB.lat() - positionA.lat());
  const bv = radians(positionB.lng() - positionA.lng());
  const em = Math.sin(fb / 2)
    * Math.sin(fb / 2)
    + Math.cos(radians(positionA.lat()))
    * Math.cos(radians(positionB.lat()))
    * Math.sin(bv / 2)
    * Math.sin(bv / 2);
  const ms = 2 * Math.atan2(Math.sqrt(em), Math.sqrt(1 - em));

  return dist * ms;
};

/**
 * @return {Number}
 */
export const getBearing = function(cx, ex, bx, dx) {
  cx = radians(cx);
  ex = radians(ex);
  bx = radians(bx);
  dx = radians(dx);
  let ax = dx - ex;
  const fx = Math.log(
    Math.tan(bx / 2 + Math.PI / 4)
    / Math.tan(cx / 2 + Math.PI / 4)
  );

  if (Math.abs(ax) > Math.PI) {
    if (ax > 0) {
      ax = -(2 * Math.PI - ax);
    }
    else {
      ax = (2 * Math.PI + ax);
    }
  }

  return (degrees(Math.atan2(ax, fx)) + 360) % 360;
};

/**
 * @return {Array}
 */
export const createDirectionPatch = function(n, o, d, r, p, f) {
  let k = Array();
  n = n - 0;
  o = o - 0;
  let j = [new google.maps.LatLng(n,o)];
  let m = [new google.maps.LatLng(n,o)];
  let q = r;
  q = 360 - q + 90;
  if (d == 0) {
    f = 180
  }
  f = f / 2;
  let g = Math.PI / 180;
  let a = 180 / Math.PI;
  let c = (p / 6378000) * a;
  let e = c / Math.cos(n * g);
  function h(s) {
    let t = Math.PI * (s / 180);
    let sx = o + (e * Math.cos(t));
    let sy = n + (c * Math.sin(t));
    j.push(new google.maps.LatLng(sy,sx))
  }
  function b(s) {
    let t = Math.PI * (s / 180);
    let sx = o + (e * Math.cos(t));
    let sy = n + (c * Math.sin(t));
    m.unshift(new google.maps.LatLng(sy,sx))
  }
  if (d != 0) {
    b(q)
  }
  h(q + f);
  h(q + f / 2);
  h(q);
  h(q - f / 2);
  h(q - f);
  if (d == 2 || d == 0) {
    j.push(new google.maps.LatLng(n,o));
    h(q + f - 180);
    h(q + f / 2 - 180);
    h(q - 180);
    h(q - f / 2 - 180);
    h(q - f - 180)
  }
  k[0] = j;
  k[1] = m;
  return k
};