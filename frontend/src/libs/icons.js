import Circle_Grey from '@/assets/img/Circle_Grey.png';

import speed_limit_10 from '@/assets/img/icons/10.png';
import speed_limit_20 from '@/assets/img/icons/20.png';
import speed_limit_30 from '@/assets/img/icons/30.png';
import speed_limit_40 from '@/assets/img/icons/40.png';
import speed_limit_50 from '@/assets/img/icons/50.png';
import speed_limit_60 from '@/assets/img/icons/60.png';
import speed_limit_70 from '@/assets/img/icons/70.png';
import speed_limit_80 from '@/assets/img/icons/80.png';
import speed_limit_90 from '@/assets/img/icons/90.png';
import speed_limit_100 from '@/assets/img/icons/100.png';
import speed_limit_110 from '@/assets/img/icons/110.png';
import speed_limit_120 from '@/assets/img/icons/120.png';
import speed_limit_130 from '@/assets/img/icons/130.png';
import speed_limit_140 from '@/assets/img/icons/140.png';
import speed_limit_150 from '@/assets/img/icons/150.png';

import bad_road from '@/assets/img/icons/bad_road.png';
import begin_village from '@/assets/img/icons/begin_village.png';
import control_cam_red from '@/assets/img/icons/control_cam_red.png';
import crosswalk from '@/assets/img/icons/crosswalk.png';
import cautiouslychildren from '@/assets/img/icons/cautiouslychildren.png';
import dang_intersection from '@/assets/img/icons/dang_intersection.png';
import dang_other from '@/assets/img/icons/dang_other.png';
import dang_turn from '@/assets/img/icons/dang_turn.png';
import dummy from '@/assets/img/icons/dummy.png';
import end_village from '@/assets/img/icons/end_village.png';
import mobile_ambush from '@/assets/img/icons/mobile_ambush.png';
import mobile_ambush_confirm from '@/assets/img/icons/mobile_ambush_confirm.png';
import overtaking_proh from '@/assets/img/icons/overtaking_proh.png';
import railroad_crossing from '@/assets/img/icons/railroad_crossing.png';
import rec_policeman from '@/assets/img/icons/rec_policeman.png';
import speed_cam_other from '@/assets/img/icons/speed_cam_other.png';
import speed_cam_start from '@/assets/img/icons/speed_cam_start.png';
import speed_cam_stop from '@/assets/img/icons/speed_cam_stop.png';
import static_cam from '@/assets/img/icons/static_cam.png';
import stationary_porst_dps from '@/assets/img/icons/stationary_porst_dps.png';
import traffic_control_ot from '@/assets/img/icons/traffic_control_ot.png';
import video_control from '@/assets/img/icons/video_control.png';

export default {
  Circle_Grey,
  speed_limit_10,
  speed_limit_20,
  speed_limit_30,
  speed_limit_40,
  speed_limit_50,
  speed_limit_60,
  speed_limit_70,
  speed_limit_80,
  speed_limit_90,
  speed_limit_100,
  speed_limit_110,
  speed_limit_120,
  speed_limit_130,
  speed_limit_140,
  speed_limit_150,
  bad_road,
  begin_village,
  control_cam_red,
  crosswalk,
  cautiouslychildren,  
  dang_intersection,
  dang_other,
  dang_turn,
  dummy,
  end_village,
  mobile_ambush,
  mobile_ambush_confirm,
  overtaking_proh,
  railroad_crossing,
  rec_policeman,
  speed_cam_other,
  speed_cam_start,
  speed_cam_stop,
  static_cam,
  stationary_porst_dps,
  traffic_control_ot,
  video_control
};
