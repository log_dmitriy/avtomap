import {getCookie} from '@/libs/cookie';

export const getLang = () => getCookie('lang') || 'ru';
