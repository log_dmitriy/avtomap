const factory = require('../../../app/managers/models/factory');

const PointModel = require('../../../models/point');
const PointSchema = require('../../../models/schemas/point.json');

const PointCLModel = require('../../../models/point_changelog');

module.exports = {
  point: factory(PointModel, PointSchema),
  pointCL: PointCLModel
};
