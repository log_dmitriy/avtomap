/* global google */
/* eslint id-length: off*/
import {point as PointModel} from '@/libs/models';
import {createDirectionPatch} from '@/libs/direction';
import Icons from '@/libs/icons';
import moment from 'moment';

/**
 * Расширяет модель точки.
 */
export default class Point extends PointModel {
  /**
   * Конструктор точки.
   * @param  {Object} fields
   */
  constructor(fields) {
    super(fields);

    this.lineBetweenPoint = null;
    this.parent = null;
    this.inited = false;
    this.info = {};
  }

  /**
   * Инициализация точки
   */
  init() {
    if (this.marker === void 0) {
      throw new Error('Не установлен маркер');
    }

    this.map = this.marker.getMap();

    this.drawDirection();
    this.updateDirection();
    this.updateIcon();

    if (this.isChilded()) {
      for (const key in this.fields) {
        if (!this.hasFieldInWhiteList(key)) {
          delete this.fields[key];
        }
      }
    }

    this.inited = true;
  }

  /**
   * Устанавливает маркер
   */
  setMarker(marker) {
    this.marker = marker;

    if (this.marker.$model === void 0) {
      this.marker.$model = this;
    }
  }

  /**
   * Рисует область предупреждения
   */
  drawDirection() {
    this.$directionView = this.drawDirectionView();
    this.$directionLine = this.drawDirectionLine();
  }

  /**
   * Перерисовывать область предупреждения
   */
  redrawDirection() {
    this.$directionView.setMap(null);
    this.$directionView = this.drawDirectionView();
    this.updateDirection();
  }

  /**
   * Рисует область предупреждения
   */
  drawDirectionView() {
    const colorList = {
      0: '#FFD800',
      '-1': '#FF1427',
      1: '#23FF48'
    };

    const color = this.isChilded() ?
      this.parent.fields.rating :
      this.fields.rating;

    return new google.maps.Polygon({
      strokeColor: '#ffffff',
      strokeOpacity: 100,
      strokeWeight: 2,
      clickable: false,
      fillColor: colorList[color],
      fillOpacity: 0.35,
      map: this.map
    });
  }

  /**
   * Рисует линию
   */
  drawDirectionLine() {
    return new google.maps.Polyline({
      map: this.map,
      geodesic: true,
      strokeOpacity: 0.8,
      strokeColor: '#000000',
      strokeWeight: 1,
      icons: [{
        icon: {
          path: `M-1.465,0l1.5-2.811 
            M-0.035-2.811L1.465,0 
            M-0.035-0.653L-1.465,0 
            M1.407-0.109l-1.442-0.545`,
          strokeOpacity: 0.7,
          scale: 1.5
        },
        offset: 0
      }]
    });
  }

  /**
   * Обновляет направление полигонов
   */
  updateDirection() {
    const paths = createDirectionPatch(
      this.fields.y,
      this.fields.x,
      this.fields.dirType,
      this.fields.direction,
      this.fields.distance,
      this.fields.angle);


    this.$directionView.setPath(paths[0]);
    this.$directionLine.setPath(paths[1]);
  }

  /**
   * Обновляет иконку
   */
  updateIcon() {
    let iconType = this.fields.type;

    // 365
    if (this.fields.type === 'mobile_ambush') {
      const diff = moment().diff(moment(this.fields.confirmed));
      const days = moment.duration(diff).as('d');

      if (days >= 365) {
        iconType = 'mobile_ambush_confirm';
      }
    }
    else if (this.fields.type === 'speed_limit') {
      iconType = `${this.fields.type}_${this.fields.speed}`;
    }
    else if (this.hasChildrens()) {
      iconType = `${this.fields.type}_start`;
    }
    else if (this.isChilded()) {
      const postfix = this.isLastChild() ? 'stop' : 'other';

      iconType = `${this.parent.fields.type}_${postfix}`;
    }

    const iconUrl = Icons[this.getNormalIcon(iconType)];

    if (iconUrl !== void 0 && iconUrl !== null) {
      this.marker.setIcon(iconUrl);
    }
  }

  /**
   * Обновляет иконки доп. точек
   */
  updateChildIcons() {
    if (!this.hasChildrens()) {
      return;
    }

    for (const child of this.fields.childrens) {
      child.updateIcon();
    }
  }

  /**
   * Перерисовывает область предупреждения у доп. точек
   */
  redrawChildDirection() {
    for (const child of this.fields.childrens) {
      child.redrawDirection();
    }
  }

  /**
   * Возвращает иконку
   *
   * Сначала ищет иконку по нужному типу
   * если не находит, подставляет '_start'
   * это нужно для точек у которых могут быть доп. точки
   * 
   * @param  {String}  type
   * @return {String}
   */
  getNormalIcon(type) {
    let result = type;

    if (!Icons.hasOwnProperty(result)) {
      result += '_start';
    }

    return result;
  }

  /**
   * Очищает полигоны
   */
  clear() {
    this.clearListeners();

    this.$directionView.setMap(null);
    this.$directionLine.setMap(null);
    this.marker.setMap(null);
  }

  /**
   * показывает или скрывает точку
   *
   * @param {Boolean} show  показывать/скрывать точку
   */
  toggle(show) {
    if (this.hasChildrens()) {
      this.fields.childrens.forEach(child => {
        this.lineBetweenPoint.setVisible(show);
        child.toggle(show);
      });
    }

    this.marker.setVisible(show);
    this.$directionView.setVisible(show);
    this.$directionLine.setVisible(show);
  }

  /**
   * Очищает слушателей
   */
  clearListeners() {
    google.maps.event.clearInstanceListeners(this.marker);
  }

  /**
   * Создает линию между дополнительными точками
   */
  createLineBetweenPoint() {
    if (this.lineBetweenPoint !== null) {
      return;
    }

    this.lineBetweenPoint = new google.maps.Polyline({
      strokeColor: '#000000',
      strokeOpacity: 1.0,
      strokeWeight: 3,
      map: this.map
    });

    const path = this.lineBetweenPoint.getPath();

    path.push(this.marker.getPosition());
  }

  /**
   * Добавляет новую позицию в кривую
   * @param  {{lat, lng}} position
   */
  pushInPathByLineBetween(position) {
    if (this.lineBetweenPoint === null) {
      return;
    }

    const path = this.lineBetweenPoint.getPath();

    path.push(position);
  }

  /**
   * Добавляет новую позицию в кривую в начало массива
   * @param  {{lat, lng}} position
   */
  unshiftInPathByLineBetween(position) {
    if (this.lineBetweenPoint === null) {
      return;
    }

    const path = this.lineBetweenPoint.getPath();

    path.insertAt(1, position);
  }

  /**
   * Обновляет положение кривой привязанной к точке
   * @param  {Number} index
   * @param  {{lat, lng}} position
   */
  updatePositionInPathByLineBetween(index, position) {
    if (this.lineBetweenPoint === null) {
      return;
    }

    const path = this.lineBetweenPoint.getPath();

    path.setAt(index, position);
  }

  /**
   * Удаляет точку в кривой
   * @param  {Number} index
   */
  removePositionInPathByLineBetween(index) {
    if (this.lineBetweenPoint === null) {
      return;
    }

    const path = this.lineBetweenPoint.getPath();

    path.removeAt(index);
  }

  /**
   * Устанавливает родительску точку
   */
  setParent(parent) {
    this.parent = parent;
  }

  /**
   * Если точка является потомком
   * 
   * @return {Boolean}
   */
  isChilded() {
    return this.parent !== null;
  }

  /**
   * Если точка родительская и имеет потомков
   * 
   * @return {Boolean}
   */
  hasChildrens() {
    if (this.isChilded()) {
      return false;
    }

    return this.fields.childrens !== void 0
      && this.fields.childrens.length > 0;
  }

  /**
   * Если точка является последней
   * 
   * @return {Boolean}
   */
  isLastChild() {
    if (!this.isChilded()) {
      return false;
    }

    const childs = this.parent.fields.childrens;
    const index = childs.indexOf(this);

    return index === childs.length - 1;
  }

  /**
   * Возвращает позицию потомка в списке
   * 
   * @param  {Point} child
   * @return {Number}
   */
  indexOfChild(child) {
    if (!this.hasChildrens()) {
      return false;
    }

    return [this, ...this.fields.childrens].indexOf(child);
  }

  /**
   * Проверяет, имеется ли поле в белом списке
   * 
   * @param  {String}  key
   * @return {Boolean}
   */
  hasFieldInWhiteList(key) {
    if (!this.isChilded()) {
      return true;
    }

    const whiteList = ['_id', 'x', 'y', 'dirType',
      'direction', 'distance', 'angle'];

    return whiteList.indexOf(key) >= 0;
  }

  /**
   * Удаляет потомка
   * @param  {Point} child
   */
  removeChild(child) {
    const index = this.indexOfChild(child);

    child.clear();
    this.removePositionInPathByLineBetween(index);
    // index - 1, т.к. 0 это родительский элемент
    this.fields.childrens.splice(index - 1, 1);
  }

  /**
   * Удаляет потомков
   */
  removeChilds() {
    for (const child of [...this.fields.childrens]) {
      this.removeChild(child);
    }
  }

  /**
   * Удаляет все точки кроме текущей.
   */
  removeOtherPoint() {
    const parent = this.getParent();
    const points = [parent, ...parent.fields.childrens];
    const indexThis = points.indexOf(this);

    points.splice(indexThis, 1);

    for (const point of [...points]) {
      const index = points.indexOf(point);

      point.clear();
      parent.removePositionInPathByLineBetween(index);

      if (point.isChilded()) {
        parent.fields.childrens.splice(index - 1, 1);
      }
    }
  }

  /**
   * Возвращает родителя
   * 
   * @return {Point}
   */
  getParent() {
    if (this.isChilded()) {
      return this.parent;
    }

    return this;
  }

  /**
   * проверяет данные объекта на валидность
   *
   * @param {String}  [field] поле, которое нужно отвалидировать
   */
  async validate(field) {
    if (!this.fields.childrens) {
      return super.validate(field);
    }

    const schema = this.schema;
    const fields = this.toJSON();

    fields.childrens = fields.childrens.map(child => child.toJSON());

    return Point.validateFields(fields, schema, this.validateOptions);
  }
}
