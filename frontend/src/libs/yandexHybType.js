/**
 * класс для отображения тайлов яндекс-гибрид
 */
export default class YandexHybType {
  /**
   * конструктор
   *
   * @param tileSize
   * @param lang
   */
  constructor(tileSize, lang = 'ru') {
    this.tileSize = tileSize;
    this.lang = lang;
    this.maxZoom = 18;
    this.minZoom = 3;
    this.opacity = 0.9;
    this.name = 'Яндекс Гибрид';
    this.alt = 'Яндекс Гибрид';
  }

  /**
   * получение тайла
   *
   * @param coord
   * @param zoom
   * @param ownerDocument
   *
   * @return {*|Element|any}
   */
  getTile(coord, zoom, ownerDocument) {
    const canvas = ownerDocument.createElement('canvas');

    canvas.width = this.tileSize.width;
    canvas.height = this.tileSize.height;

    const ctx = canvas.getContext('2d');

    this.loadImg('sat', coord, zoom)
      .then(img => ctx.drawImage(img, 0, 0))
      .then(() => this.loadImg('vec', coord, zoom))
      .then(img => ctx.drawImage(img, 0, 0));

    return canvas;
  }

  /**
   * загрузка изображения
   *
   * @param type
   * @param coord
   * @param zoom
   *
   * @return {Promise}
   */
  loadImg(type, coord, zoom) {
    return new Promise((resolve, reject) => {
      const img = new Image(this.tileSize.width, this.tileSize.height);

      img.onload = function() {
        resolve(this);
      };
      img.src = this.getUrl(type, coord, zoom);
    });
  }

  /**
   * Построение адреса
   *
   * @param type
   * @param coord
   * @param zoom
   *
   * @return {String}
   */
  getUrl(type, coord, zoom) {
    const ltype = (type === 'vec') ? 'skl' : 'sat';
    const version = (type === 'vec') ? '2.12.1' : '1.36.0';

    return `https://${type}0${(coord.x + coord.y) % 4 + 1}` +
      `.maps.yandex.net/tiles?l=${ltype}&v=${version}` +
      `&x=${coord.x}&y=${coord.y}&z=${zoom}&lang=${this.lang}`;
  }
}
