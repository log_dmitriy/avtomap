function atanh(x) {
  return 0.5 * Math.log((1 + x) / (1 - x));
}

export default class YandexProjection {
  constructor(globalMap = google.maps) {
    this.$globalMap = globalMap;
    this.pixelOrigin_ = new this.$globalMap.Point(128,128);
    this.pixelsPerLonDegree_ = 256 / 360;
    this.pixelsPerLonRadian_ = 256 / (2 * Math.PI)
  }

  fromLatLngToPoint(latLng) {
    var origin = this.pixelOrigin_;
    var exct = 0.0818197;
    var z = Math.sin(latLng.lat() / 180 * Math.PI);

    var arg1 = origin.x + latLng.lng() * this.pixelsPerLonDegree_;
    var arg2 = Math.abs(
      origin.y - this.pixelsPerLonRadian_ * (
        atanh(z) - exct * atanh(exct * z)
      )
    );

    return new this.$globalMap.Point(arg1, arg2);
  }

  fromPointToLatLng(point) {
    var origin = this.pixelOrigin_;
    var lng = (point.x - origin.x) / this.pixelsPerLonDegree_;
    var latRadians = (point.y - origin.y) / -this.pixelsPerLonRadian_;
    var lat = Math.abs(
      (2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2) * 180 / Math.PI
    );
    var Zu = lat / (180 / Math.PI);
    var Zum1 = Zu + 1;
    var exct = 0.0818197;
    var yy = -Math.abs(((point.y) - 128));
    while (Math.abs(Zum1 - Zu) > 0.00000001) {
      Zum1 = Zu;
      Zu = Math.asin(
        1 - ((1 + Math.sin(Zum1)) * Math.pow(1 - exct * Math.sin(Zum1), exct)) / (Math.exp((2 * yy) / -(256 / (2 * Math.PI))) * Math.pow(1 + exct * Math.sin(Zum1),exct))
     );
    }

    if (point.y>256/2) {
      lat = -Zu * 180 / Math.PI;
    } else {
      lat = Zu * 180 / Math.PI;
    }

    return new this.$globalMap.LatLng(lat, lng);
  }
};
