/* global google */
/* eslint id-length: off*/
/* eslint max-statements: off */

import Point from '@/libs/point';

// миксин для работы с точками на карте
export default {
  data() {
    return {
      infoFields: {},
      sortedFields: []
    };
  },

  watch: {
    infoFields(newVal, prevVal) {
      const infoFields = this.infoFields;
      const fields = Object.keys(infoFields);
      const sortedFields = fields.sort((curName, nextName) => {
        let cur = infoFields[curName].sort;
        let next = infoFields[nextName].sort;

        if (cur === void 0) {
          cur = Infinity;
        }

        if (next === void 0) {
          next = Infinity;
        }

        return cur - next;
      });

      this.sortedFields = sortedFields;
    }
  },

  methods: {
    getNormalFields(
      values = {},
      withHiddenFields = [],
      defaultValues = {}
    ) {
      return Point.getNormalFields(
        values, withHiddenFields, defaultValues);
    },

    getInfoFields(point) {
      const defaultValues = this.point.getNormalFields();
      const allInfos = this.getNormalFields(point.fields, true, defaultValues);
      const infos = this.clearOfHiddenFields(allInfos);

      point.info = infos;

      Object.keys(infos).forEach(fieldName => {
        const field = infos[fieldName];

        this.$set(point.fields, fieldName, field.value);
      });

      Object.keys(point.fields).forEach(fieldName => {
        if (fieldName in allInfos) {
          return;
        }

        this.$delete(point.fields, fieldName);
      });

      return infos;
    },

    /**
     * очищает поля от скрытых
     *
     * @param {Object} fields поля
     *
     * @returns {Object}
     */
    clearOfHiddenFields(fields) {
      const rawFields = this.point.schema._meta[0].rawSchema;
      const clearedFields = {};

      Object.keys(fields).forEach(fieldKey => {
        const rawField = rawFields[fieldKey];

        if (rawField.hidden === true) {
          return;
        }

        if (!this.point.hasFieldInWhiteList(fieldKey)) {
          return;
        }

        clearedFields[fieldKey] = fields[fieldKey];
      });

      return clearedFields;
    },

    includeValueInList(value, list) {
      return Point.includeValueInList(value, list);
    },

    __normalizeOfDepend(dependencyValues, dependFieldValue) {
      return Point.__normalizeOfDepend(
        dependencyValues, dependFieldValue);
    },

    __normalizeList(rawList, type) {
      return Point.__normalizeList(rawList, type);
    }
  }
};
