/* global google */
/* eslint id-length: off*/

import debounce from 'lodash.debounce';
import Vue from 'vue';
import {mapActions, mapGetters} from 'vuex';

import Point from '@/libs/point';
import Icons from '@/libs/icons';

const DELAY_LOAD_POINTS = 500;
/**
 * zoom при котором подгружать точки
 *
 * @type {Number}
 */
const ZOOM_LOAD_POINT = 14;

// миксин для работы с точками на карте
export default {
  data() {
    return {
      // точки грузятся
      loadingPoints: false,
      // границы с загруженными точками
      loadedBounds: [],
      // загруженные точки
      points: {},

      // id точки у которой показывать комментарии
      commentPoint: null,
      // выбранный комментарий
      selectedComment: null,

      changePanel: '',

      togglePanel: false,
      lastLatLng: null,
      marker: null,
      pointSelected: null,
      loadedPoints: false
    };
  },

  computed: {
    ...mapGetters({generateUrl: 'api/fullUrl'})
  },

  watch: {
    pointSelected(cur, prev) {
      if (
        Boolean(cur) === true
        && this.loadedPoints === true
        && (prev === void 0 || prev !== cur)
      ) {
        this.showPoint(cur);
      }
    },

    loadedPoints(cur, prev) {
      if (cur === true && Boolean(this.pointSelected)) {
        this.showPoint(this.pointSelected);
      }
    },

    zoom(newZoom, oldZoom) {
      if (
        Object.is(newZoom, null) || newZoom === void 0 ||
        Object.is(oldZoom, null) || oldZoom === void 0
      ) {
        return;
      }

      if (newZoom < ZOOM_LOAD_POINT && oldZoom >= ZOOM_LOAD_POINT) {
        this.togglePoints(false);
      }
      else if (newZoom >= ZOOM_LOAD_POINT && oldZoom < ZOOM_LOAD_POINT) {
        this.togglePoints(true);
      }
    }
  },

  created() {
    this.$on('bounds_changed', debounce(this.loadPoints,
      DELAY_LOAD_POINTS));

    this.$on('map_click', latLng => {
      this.lastLatLng = latLng;
    });
  },

  methods: {
    ...mapActions({
      showSuccess: 'fireSuccess',
      responseError: 'fireResponseError',
      serverError: 'fireServerError',
      showError: 'fireError'
    }),

    /**
     * загружает точки на карте в выделенной области
     *
     * @param {Object} bounds область загрузки
     */
    async loadPoints(bounds) {
      if (this.zoom < ZOOM_LOAD_POINT) {
        return;
      }

      if (
        this.loadingPoints === true ||
        this.isLoadedBounds(bounds) === true
      ) {
        return;
      }

      this.loadingPoints = true;
      this.loadedPoints = false;

      const southWest = bounds.getSouthWest();
      const northEast = bounds.getNorthEast();

      const url = this.generateUrl({name: 'point.list'});

      try {
        const params = {
          type: 'bounds',
          x1: southWest.lng(),
          y1: southWest.lat(),
          x2: northEast.lng(),
          y2: northEast.lat()
        };
        const response = await Vue.http.get(url, {params});
        const points = response.body || [];

        points.forEach(point => {
          if (point._id in this.points) {
            return;
          }

          const model = this.createPoint(point);

          this.addPoint(model);
        });

        this.memLoadedBounds(bounds);
        this.loadedPoints = true;
      }
      catch (error) {
        if (error instanceof Error) {
          throw error;
        }

        if (error.status < 500) {
          await this.responseError(error);
        }
        else {
          await this.serverError(error);
        }
      }
      finally {
        this.loadingPoints = false;
      }
    },

    /**
     * показывает или скрывает точки с карты
     *
     * @param {Boolean} show  показывать/скрывать точки
     */
    togglePoints(show = true) {
      const ids = Object.keys(this.points) || [];

      ids.forEach(id => {
        const point = this.points[id];

        point.toggle(show);
      });
    },

    /**
     * запоминает загруженную область
     *
     * @param {Object} curBound
     */
    memLoadedBounds(newBounds) {
      const newCoords = newBounds.toJSON();

      const unioned = this.loadedBounds.find(bounds => {
        if (bounds.intersects(newBounds) === false) {
          return false;
        }

        const coords = bounds.toJSON();

        if (
          (newCoords.east === coords.east &&
          newCoords.west === coords.west)
          ||
          (newCoords.south === coords.south &&
          newCoords.north === coords.north)
        ) {
          bounds.union(newBounds);

          return true;
        }

        return false;
      });

      if (!unioned) {
        this.loadedBounds.push(newBounds);
      }
    },

    /**
     * проверяет загружены ли точки в данную область
     *
     * @param {Object} curBound
     *
     * @returns {Boolean}
     */
    isLoadedBounds(curBound) {
      const neLatLng = curBound.getNorthEast();
      const swLatLng = curBound.getSouthWest();
      const result = this.loadedBounds.find(bounds => {
        let finded = curBound.equals(bounds);

        if (finded === false) {
          finded = bounds.contains(neLatLng) &&
            bounds.contains(swLatLng);
        }

        return finded;
      });

      return Boolean(result);
    },

    /**
     * создает точку с заданными полями
     *
     * @param {Object} fields
     * @param {Point|Null} parent
     */
    createPoint(fields, parent = null) {
      const marker = new google.maps.Marker({map: this.map});
      const model = new Point(fields);

      if (parent) {
        model.setParent(parent);
      }

      model.setMarker(marker);
      model.init();

      marker.setPosition({
        lng: model.fields.x,
        lat: model.fields.y
      });
      marker.$model = model;

      if (model.hasChildrens()) {
        model.createLineBetweenPoint();
        model.fields.childrens = model.fields.childrens.map(child => {
          const point = this.createPoint(child, model);

          model.pushInPathByLineBetween(point.marker.getPosition());

          return point;
        });
        model.updateChildIcons();
      }

      return model;
    },

    /**
     * добавляет точку на карту
     *
     * @param point
     */
    addPoint(point) {
      this.registerListeners(point);

      this.points[point.fields._id] = point;
    },

    registerListeners(point) {
      point.marker.addListener('click', event => {
        if (this.marker) {
          return;
        }

        this.showPoint(point);
      });
    },

    showPoint(point) {
      if (typeof point === 'string') {
        point = this.points[point];
      }

      if (!point || this.$refs['point-info'].checkOpened(point)) {
        return;
      }

      this.$refs['point-info'].showPoint(point);
    },

    /**
     * удаляет точку из карты
     *
     * @param pointId
     */
    deletePoint(pointId) {
      const point = this.points[pointId];

      point.marker.setMap(null);

      delete this.points[pointId];
    },

    /**
     * очищает карту от маркера
     */
    clearMarker() {
      this.marker = null;
    },

    /**
     * удаляет маркер
     */
    deleteMarker() {
      this.marker.$model.clear();
      this.marker.$model = null;
      this.marker.setMap(null);
      this.clearMarker();
    },

    /**
     * делает маркер редактируемым
     *
     * @param marker
     */
    setEditableMarker(marker) {
      marker.setDraggable(true);

      this.marker = marker;
    },

    /**
     * делает маркер нередактируемым
     *
     * @param marker
     * @param {Boolean} delete_marker
     */
    setUneditableMarker(marker, delete_marker = true) {
      const model = marker.$model;
      const point = model.isChilded() ? model.parent : model;
      const id = point.fields._id;

      marker.setDraggable(false);
      this.registerListeners(point);

      if (id === void 0 && delete_marker) {
        this.deleteMarker();
      }
      else {
        this.clearMarker();
      }
    },

    /**
     * Событие - добавляет маркер на карту.
     *
     * @param {{lat: Number, lang: Number}}
     */
    onAddMarker(latLng) {
      const marker = new google.maps.Marker({
        position: latLng,
        map: this.map,
        draggable: true
      });

      const model = new Point({
        x: latLng.lng(),
        y: latLng.lat(),
      }, marker);

      model.setMarker(marker);
      model.init();

      marker.$model = model;

      this.setEditableMarker(marker);
    },

    onAddOtherMarker({parent, point}) {
      // Прекращаем редактировать родительскую точку
      this.setUneditableMarker(parent.marker, false);

      // Устанавливаем позицию для точки потомка
      point.fields.x = parent.fields.x + .003;
      point.fields.y = parent.fields.y;

      // Создаем маркер для точки потомка
      point.setMarker(new google.maps.Marker({
        position: new google.maps.LatLng(
          point.fields.y,
          point.fields.x
        ),
        map: this.map
      }));

      // Устанавливает потомку ссылку на родитель
      point.setParent(parent);

      // Инициализируем точку потомок
      point.init();

      // Создаем линию между точками
      parent.createLineBetweenPoint();
      parent.unshiftInPathByLineBetween(point.marker.getPosition());

      // Обновляем иконки
      parent.updateChildIcons();

      // Активируем созданную точку на редактирование
      this.setEditableMarker(point.marker);
    },

    onSwitchMarkerTo(to) {
      this.setEditableMarker(to);
    },

    /**
     * событие - отменяет редактирование маркера
     */
    onCancelMarker(delete_marker = true) {
      if (this.marker === null) {
        return;
      }

      this.setUneditableMarker(this.marker, delete_marker);
    },

    /**
     * Событие - дает редактировать точку
     *
     * @param {Object} point
     */
    onEditPoint(point) {
      if (this.marker !== null) {
        this.setUneditableMarker(this.marker);
      }

      this.setEditableMarker(point.marker);
    },

    /**
     * Событие - устанавливает выбранную точку
     *
     * @param {Object} point
     */
    onOpenPointInfo(point) {
      this.pointSelected = point.fields._id;
      this.commentPoint = point.fields._id;
    },

    /**
     * Событие – закрывает выбранную точку
     * @param {Object} point
     */
    onClosePointInfo(point) {
      this.pointSelected = null;
      this.commentPoint = null;
    },

    /**
     * Событие - переключение панели
     * 
     * @param  {Boolean} toggle
     */
    onTogglePanel(toggle) {
      this.togglePanel = toggle;

      if (toggle === false) {
        this.changePanel = '';
      }
    },

    /**
     * Событие - устанавливает тип открытой панельки
     * 
     * @param  {String} change
     */
    onChangePanel(change) {
      this.changePanel = change;
    },

    /**
     * Событие сохранения точки
     *
     * @param {Object}
     */
    onSavePoint(point) {
      const pointId = point.fields._id;

      this.setUneditableMarker(point.marker);

      if (this.points.hasOwnProperty(pointId)) {
        return;
      }

      this.addPoint(point);
    },

    /**
     * Событие удаления точки
     *
     * @param {String}
     */
    onDeletePoint(id) {
      if (this.points.hasOwnProperty(id)) {
        this.deletePoint(id);
      }

      this.deleteMarker();
    },

    /**
     * Изменяет иконку активного маркера.
     *
     * @param {String} type
     */
    onSetIcon(type) {
      const url = Icons[type];

      if (url !== void 0 && url !== null) {
        this.marker.setIcon(url);
      }
    },

    /**
     * открывает панель с комментариями
     *
     * @param {String} pointId
     */
    onOpenComments(pointId) {
      this.commentPoint = pointId;
    },

    /**
     * Закрывает панель с комментариями
     */
    onCancelComments() {
      this.commentPoint = null;
    }
  }
};
