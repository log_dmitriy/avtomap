import debounce from 'lodash.debounce';

const DELAY = 200;

export default {
  beforeDestroy() {
    document.removeEventListener('scroll', this.onScrolled);
  },

  mounted() {
    document.addEventListener('scroll', this.onScrolled);
  },

  methods: {
    onScrolled: debounce(function() {
      const $point = this.$refs.point.$el;
      const bottom = $point.getBoundingClientRect().bottom;
      const height = document.body.clientHeight;

      if (bottom <= height) {
        this.scrolledToBottom().catch(error => {
          throw error;
        });
      }
    }, DELAY)
  }
};
