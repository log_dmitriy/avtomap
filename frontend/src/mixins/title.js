import {mapGetters} from 'vuex';

export default {
  data() {
    return {
      prevTitle: ''
    };
  },

  computed: {
    ...mapGetters([
      'currentTitle',
      'currentLanguage'
    ]),
    ...mapGetters('modal', {
      modalActive: 'active'
    })
  },

  watch: {
    currentTitle(val) {
      document.title = val;
    },
    currentLanguage() {
      this.$store.commit('SET_TITLE', this.getTitle());
    },
    modalActive(value) {
      if (value) {
        this.setPrevTitle();
      }
      else {
        this.$store.commit('SET_TITLE', this.getTitle(this.getPrevTitle()));
      }
    }
  },

  mounted() {
    this.$store.commit('SET_TITLE', this.getTitle());
  },

  methods: {
    getTitle(title = this.$options.title) {
      return this.$i18n.t(title);
    },
    setPrevTitle() {
      this.prevTitle = this.$options.title;
    },
    getPrevTitle() {
      return this.prevTitle;
    }
  }
};
