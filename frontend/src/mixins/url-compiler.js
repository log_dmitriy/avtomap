import {URL} from '@/libs/url';

export default {
  methods: {
    compileUrl(key, value) {
      const url = new URL(window.location.href);

      url.searchParams.set(key, value);

      return `${url.pathname}${url.search}`;
    },

    cleanUrl(key) {
      const url = new URL(window.location.href);

      url.searchParams.delete(key);

      return `${url.pathname}${url.search}`;
    },
  }
};
