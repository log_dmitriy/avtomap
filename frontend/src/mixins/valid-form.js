export default {
  data() {
    return {
      errors: {},
      loading: false,
      disabled: false
    };
  },

  created() {
    const fields = this.fields || [];

    fields.forEach(field => {
      this.$set(this.errors, field, []);
    });
  },

  methods: {
    resetInputs() {
      this.fields.forEach(field => {
        this.errors[field] = [];
      });
    },

    /**
     * запускает операцию
     *
     * @param  {Promise} operation операция
     *
     * @return {Promise}
     */
    async runOperation(operation) {
      this.resetInputs();
      this.loading = true;

      try {
        await operation;

        this.disabled = true;
      }
      catch (error) {
        if (error.body) {
          Object.keys(error.body).forEach(field => {
            if (!(field in this.errors)) {
              return;
            }

            const errorMessage = this.$i18n.t(error.body[field]);

            this.errors[field].push(errorMessage);
          });
        }

        throw error;
      }
      finally {
        this.loading = false;
      }
    }
  }
};
