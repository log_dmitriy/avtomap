import Vue from 'vue';
import Router from 'vue-router';
import appRoutes from 'node-scripts/app-routes';

Vue.use(Router);

const routes = Object.keys(appRoutes).map(name => {
  const path = appRoutes[name];

  return {name, path};
});

export default new Router({
  mode: 'abstract',

  routes
});
