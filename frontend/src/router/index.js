import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';

/**
 * возвращает хэш роута
 *
 * @param {Object} route роут
 *
 * @returns {String}
 */
function getHash(route) {
  const hash = route.name + route.path;

  return hash;
}

Vue.use(Router);

function beforeRouteEnter(to, from, next) {
  next(vm => {
    if (!('asyncData' in vm)) {
      return;
    }

    store.dispatch('loadingResource', getHash(to));

    vm.asyncData()
      .then(() => store.dispatch('loadedResource', getHash(to)))
      .catch(error => store.dispatch('applicationError', error));
  });
}
function beforeRouteUpdate(to, from, next) {
  if (!('asyncData' in this)) {
    next();

    return;
  }

  const hash = getHash(to);

  Promise.resolve()
    .then(() => store.dispatch('loadingResource', hash))
    .then(() => next())
    .then(() => this.asyncData())
    .then(() => store.dispatch('loadedResource', hash))
    .catch(error => store.dispatch('applicationError', error));
}
function beforeRouteLeave(to, from, next) {
  if (!('rejectAsyncData' in this)) {
    next();

    return;
  }

  this.rejectAsyncData()
    .then(() => {
      store.dispatch('loadedResource', getHash(to));

      next();
    })
    .catch(next);
}

// будет применяться к каждому компоненту
Vue.mixin({
  beforeRouteEnter,
  beforeRouteUpdate,
  beforeRouteLeave
});


const loadView = viewName => () => {
  const component = import(`@/views/${viewName}.vue`);

  return component;
};


const router = new Router({
  mode: 'history',
  routes: [
    {path: '/', name: 'map', component: loadView('map')},
    {
      path: '/history',
      name: 'history',
      component: loadView('history/index'),
      children: [{
        path: '/',
        name: 'history.all',
        component: loadView('history/all')
      }, {
        path: ':pid',
        name: 'history.point',
        component: loadView('history/point'),
        props: true
      }]
    },
    {path: '/help', name: 'help', component: loadView('help')},
    {path: '/about', name: 'about', component: loadView('about')},
    {path: '/export', name: 'export', component: loadView('export')},
    {
      path: '/agreements',
      name: 'agreements',
      component: loadView('agreements')
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: loadView('contacts')
    },
    {
      path: '/reset',
      component: loadView('reset-password/index'),
      children: [{
        path: 'confirm/:email/:confirmKey',
        name: 'confirmPassword',
        component: loadView('reset-password/confirm'),
        props: true
      }]
    },
    {
      path: '/profile/:login',
      component: loadView('profile/index'),
      children: [{
        path: '/',
        name: 'profile.detail',
        component: loadView('profile/detail'),
        props: true
      }, {
        path: 'change',
        name: 'profile.change',
        component: loadView('profile/change'),
        props: true
      }]
    }
  ]
});

router.onError(error => {
  store.dispatch('applicationError', error);
});

export default router;
