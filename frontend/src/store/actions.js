import * as types from './mutations-types';
import {setCookie} from '@/libs/cookie';

/**
 * Подгружает перевод из файла.
 * @param {*} state 
 * @param {String} languageIdentity 
 */
export const getLangMessages = async (state, languageIdentity) =>
  require(`@/../../lang/${languageIdentity}.json`);

/**
 * Подгружает перевод сообщений валидации из файла.
 * @param {*} state
 * @param {String} languageIdentity
 */
export const getValidateLangMessages =
  async (state, languageIdentity) =>
    require(`@/../../lang/validation/${languageIdentity}.json`);

/**
 * Переключает язык.
 * @param {*} param0 
 * @param {String} languageIdentity 
 */
export const switchLanguage = async (
  {dispatch, commit, state},
  languageIdentity
) => {
  if (!state.languages[languageIdentity].messages) {
    commit(types.PRELOADER_ACTIVATE);

    const messages = await dispatch(
      'getLangMessages', languageIdentity);
    const validateMessages = await dispatch(
      'getValidateLangMessages', languageIdentity);

    commit(types.SET_LANGUAGE_MESSAGES, {
      identity: languageIdentity,
      messages
    });

    commit('SET_VALIDATE_MESSAGES', {
      identity: languageIdentity,
      messages: validateMessages
    });

    commit(types.PRELOADER_DEACTIVATE);
  }

  setCookie('lang', languageIdentity, {expires: 31536000});
  commit(types.SWITCH_LANGUAGE, languageIdentity);
};

export const fireError = async ({commit, dispatch}, message) => {
  if (message !== '') {
    commit(types.CHANGE_NOTIFY, {
      message,
      type: 'error'
    });
  }
};

/**
 * обертка над ошибками во время запроса
 *
 * @param  {Function} options.commit
 * @param  {Function} options.dispatch
 * @param  {Object}   response
 *
 * @return {Promise}
 */
export const fireResponseError = async ({commit, dispatch}, response) => {
  const {body = {}} = response;
  const {message = ''} = body;

  await dispatch('fireError', message);

  if (response.status && response.status === 401) {
    await dispatch('unauthorizedError');
  }

  const error = new Error('error response');

  error.body = body;

  throw error;
};

export const fireSuccess = async ({commit}, message) => {
  if (message !== '') {
    commit(types.CHANGE_NOTIFY, {
      message,
      type: 'success'
    });
  }
};

/**
 * обертка над удачными запросами
 *
 * @param  {Function} options.commit
 * @param  {Object}   response
 *
 * @return {Promise}
 */
export const fireResponseSuccess = async ({commit, dispatch}, response) => {
  const {body = {}} = response;
  const {message = ''} = body;

  await dispatch('fireSuccess', message);
};

/**
 * обертка над ошибками сервера
 *
 * @param  {Function} options.commit
 * @param  {Object}   response
 *
 * @return {Promise}
 */
export const fireServerError = async ({commit}, response) => {
  const {body = {}} = response;
  const {message = 'Ошибка сервера'} = body;

  commit(types.CHANGE_NOTIFY, {
    message,
    type: 'error'
  });

  const error = new Error('error response');

  error.body = body;

  throw error;
};

export const applicationError = async ({commit}, error) => {
  if (error.stack) {
    console.error(error.stack);
  }

  commit(types.CHANGE_NOTIFY, {
    message: 'Неизвестная ошибка',
    type: 'error'
  });
};

export const loadingResource = async ({state, commit}, resource) => {
  commit(types.PRELOADER_ADD_RESOURCE, resource);

  if (state.preloader.resources.length === 1) {
    commit(types.SWITCH_OVERLAY, {active: true});
    commit(types.PRELOADER_ACTIVATE);
  }
};

export const loadedResource = async ({state, commit}, resource) => {
  commit(types.PRELOADER_REMOVE_RESOURCE, resource);

  if (state.preloader.resources.length === 0) {
    commit(types.SWITCH_OVERLAY, {active: false});
    commit(types.PRELOADER_DEACTIVATE);
  }
};

export const unauthorizedError = async ({commit}) => {
  commit(types.CHANGE_NOTIFY, {
    message: 'У вас недостаточно прав',
    type: 'error'
  });
};
