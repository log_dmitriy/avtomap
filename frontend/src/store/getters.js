/**
 * Возвращает идентификатор текущего языка.
 *
 * @param {*} state состояние
 */
export const currentLanguage = state => state.language;

/**
 * Возвращает список доступных языков.
 * @param {*} state 
 */
export const getLanguagesList = state => state.languages;

/**
 * Возвращает перевод для текущего языка.
 * @param {*} state 
 */
export const getLanguageMessages = state =>
  state.languages[state.language].messages;

/**
 * Возвращает заголовок.
 * @param {*} state 
 */
export const currentTitle = state => {
  let result_title = state.mainTitle;

  if (state.title) {
    result_title = `${state.title} | ${result_title}`;
  }

  return result_title;
};

/**
 * Возвращает состояние прелоадера.
 * @param {*} state 
 */
export const preloader = state => state.preloader;

/**
 * Возвращает текущее сообщение приложения.
 * @param  {*} state
 */
export const notify = state => state.notify;

/**
 * Возвращает состояние активности перекрывающего окна.
 * @param  {*} state
 */
export const overlay = state => state.overlay;
