import Vue from 'vue';
import Vuex from 'vuex';
import account from './plugins/account';
import api from './plugins/api';
import modal from './plugins/modal';
import map from './plugins/map';
import notifications from './plugins/notifications';
import {getLang} from '@/libs/lang';

import * as actions from './actions';
import * as getters from './getters';
import mutations from './mutations';

const lang = getLang();

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    mainTitle: 'OpenSpeedcam',
    title: '',
    language: lang,
    languages: {
      ru: {name: 'Русский', messages: null},
      en: {name: 'English', messages: null}
    },
    preloader: {
      active: false,
      resources: []
    },
    overlay: false,

    notify: {
      type: 'info',
      message: '',
      active: false
    }
  },
  actions,
  getters,
  mutations,

  plugins: [api, account, modal, map, notifications]
});
