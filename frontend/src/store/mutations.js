import Vue from 'vue';
import * as types from './mutations-types';
import IModel from '@/../../app/managers/models/imodel';

export default {
  /**
   * Устанавливает заголовок приложения.
   *
   * @param {*} state       состояние
   * @param {String} title  заголовк
   *
   * @return {undefined}
   */
  [types.SET_TITLE](state, title) {
    state.title = title;
  },

  /**
   * Устанавливает переводы для указанного языка.
   *
   * @param {*}      state    состояние
   * @param {Object} language локаль
   *
   * @return {undefined}
   */
  [types.SET_LANGUAGE_MESSAGES](state, language) {
    Vue.set(state.languages[language.identity], 'messages', language.messages);
  },

  SET_VALIDATE_MESSAGES(state, payload) {
    IModel.setDefaultValidateOptions({language: payload.messages});
  },

  /**
   * Переключает язык.
   *
   * @param {*}      state            состояние
   * @param {String} languageIdentity локаль
   *
   * @return {undefined}
   */
  [types.SWITCH_LANGUAGE](state, languageIdentity) {
    state.language = languageIdentity;
  },

  /**
   * Активирует прелоадер.
   *
   * @param {*} state состояние
   *
   * @return {undefined}
   */
  [types.PRELOADER_ACTIVATE](state) {
    state.preloader.active = true;
  },

  /**
   * Деактивирует прелоадер.
   *
   * @param {*} state состояние
   *
   * @return {undefined}
   */
  [types.PRELOADER_DEACTIVATE](state) {
    state.preloader.active = false;
  },

  /**
   * Добавляет ресурс в загрузку
   *
   * @param {*}       state     состояние
   * @param {String}  resource  название (или хэ) ресурса
   *
   * @return {undefined}
   */
  [types.PRELOADER_ADD_RESOURCE](state, resource) {
    state.preloader.resources.push(resource);
  },

  /**
   * Удаляет ресурс из загрузки
   *
   * @param {*}       state     состояние
   * @param {String}  resource  название (или хэ) ресурса
   *
   * @return {undefined}
   */
  [types.PRELOADER_REMOVE_RESOURCE](state, resource) {
    const index = state.preloader.resources.indexOf(resource);

    if (index === -1) {
      return;
    }

    state.preloader.resources.splice(index, 1);
  },

  /**
   * Меняет сообщение приложения.
   *
   * @param {*} state состояние
   *
   * @return {undefined}
   */
  [types.CHANGE_NOTIFY](state, {type, message}) {
    state.notify.type = type;
    state.notify.message = message;
    state.notify.active = true;
  },

  /**
   * Меняет показ сообщения приложения.
   *
   * @param {*} state состояние
   *
   * @return {undefined}
   */
  [types.SWITCH_NOTIFY](state, {active}) {
    state.notify.active = active;
  },

  /**
   * Меняет показ перекрывающего окна.
   *
   * @param {*} state состояние
   *
   * @return {undefined}
   */
  [types.SWITCH_OVERLAY](state, {active}) {
    state.overlay = active;
  }
};
