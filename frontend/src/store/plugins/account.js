/* eslint valid-jsdoc: off */

import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';

Vue.use(Vuex);
Vue.use(VueResource);

const IS_AUTH = 'IS_AUTH';

/**
 * плагин хранилища пользователя
 *
 * @param {Object} store хранилище
 *
 * @return {undefined}
 */
export default function(store) {
  store.registerModule('account', {
    // модуль в своем пространстве имен
    namespaced: true,

    state() {
      return {
        authenticated: false,
        user: {}
      };
    },

    getters: {
      authenticated(state, getters) {
        return state.authenticated;
      },
      user(state) {
        return state.user;
      }
    },

    actions: {
      /**
       * обрабатывает ошибку в ответе
       *
       * @param  {Function} options.dispatch
       * @param  {Object}   response
       *
       *  @return {Promise}
       */
      async responseError({dispatch}, response) {
        if (response instanceof Error) {
          throw response;
        }

        if (response.status < 500) {
          await dispatch('fireResponseError', response, {root: true});
        }
        else {
          await dispatch('fireServerError', response, {root: true});
        }
      },

      /**
       * проверяет аутентифицирован ли пользователь на сервере
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async checkAuthenticate({commit, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({name: 'aaa.check'});

        const response = await Vue.http.get(url);
        const {authenticated, user = {}} = response.body;

        commit(IS_AUTH, {authenticated, user});
      },

      /**
       * проверяет имеет ли пользователь права на ресурс
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async checkRole({commit, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({name: 'aaa.checkrole'});

        const response = await Vue.http.post(url, payload);
        const {hasPerms} = response.body;

        return hasPerms;
      },

      /**
       * аутентифицирует пользователя
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async login({commit, dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({name: 'aaa.auth'});

        try {
          const response = await Vue.http.post(url, payload);
          const user = response.body.user;

          await dispatch('fireResponseSuccess', response, {root: true});

          commit(IS_AUTH, {
            authenticated: true,
            user
          });
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      },

      /**
       * выход
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async logout({commit, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({name: 'aaa.logout'});

        await Vue.http.post(url);

        commit(IS_AUTH, {
          authenticated: false,
          user: {}
        });
      },

      /**
       * регистрирует пользователя
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async register({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({name: 'registration'});

        try {
          const response = await Vue.http.post(url, payload);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      },

      /**
       * отправляет письмо с изменение пароля
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async resetPassword({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({name: 'resetPassword'});

        try {
          const response = await Vue.http.post(url, payload);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      },

      /**
       * меняет пароль
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async confirmPassword({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'confirmPassword',
          params: payload.params
        });

        try {
          const response = await Vue.http.post(url, payload.body);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      },

      /**
       * возвращает данные пользователя
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async profile({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'profile.detail',
          params: {
            login: payload.login
          }
        });

        let response;

        try {
          response = await Vue.http.get(url);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }

        return response.body;
      },

      /**
       * меняет данные пользователя
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async profileChange({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'profile.change',
          params: payload.params
        });

        try {
          const response = await Vue.http.put(url, payload.body);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      },

      /**
       * меняет роли пользователя
       *
       * @param  {Function} options.commit
       * @param  {Object}   options.rootGetters
       * @param  {Object}   payload
       *
       * @return {Promise}
       */
      async profileChangeRoles({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'profile.change.roles',
          params: payload.params
        });

        try {
          const response = await Vue.http.put(url, payload.body);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      },

      async changeNotifications({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'notifications.user',
          params: payload.params
        });

        try {
          const response = await Vue.http.post(url, payload.body);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('responseError', error);
        }
      }
    },

    mutations: {
      [IS_AUTH](state, payload) {
        state.authenticated = payload.authenticated;
        state.user = payload.user;
      }
    }
  });
}
