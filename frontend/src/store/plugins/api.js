/* eslint no-undef: off */

import router from '../../router/api';

/**
 * плагин хранилища api backend'а
 *
 * @param {Object} store хранилище
 *
 * @return {undefined}
 */
export default function(store) {
  store.registerModule('api', {
    // модуль в своем пространстве имен
    namespaced: true,

    state() {
      return {router};
    },

    getters: {
      router(state, getters) {
        return state.router;
      },
      url(state, getters) {
        return info => {
          const address = getters.router.match(info);

          return address.fullPath;
        };
      },
      fullUrl(state, getters) {
        const urlFn = getters.url;

        return info => __PUBLIC_URL__ + urlFn(info);
      }
    },

    actions: {},
    mutations: {}
  });
}
