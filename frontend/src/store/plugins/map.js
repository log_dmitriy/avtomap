/**
 * плагин хранилища карты
 *
 * @param {Object} store хранилище
 *
 * @return {undefined}
 */
export default function(store) {
  store.registerModule('map', {
    namespaced: true,

    state() {
      return {
        center: {
          lat: 60.0,
          lng: 90.0
        },
        zoom: 4
      };
    },

    getters: {
      center(state) {
        return state.center;
      },
      zoom(state) {
        return state.zoom;
      }
    },

    mutations: {
      SET_ZOOM(state, payload) {
        state.zoom = payload;
      },
      SET_CENTER_LAT(state, payload) {
        state.center.lat = typeof payload === 'function' ? payload() : payload;
      },
      SET_CENTER_LNG(state, payload) {
        state.center.lng = typeof payload === 'function' ? payload() : payload;
      },
      SET_CENTER(state, payload) {
        const lat = (typeof payload.lat === 'function') ?
          payload.lat() : payload.lat;
        const lng = (typeof payload.lng === 'function') ?
          payload.lng() : payload.lng;

        state.center = {lat, lng};
      }
    },

    actions: {
      setZoom({commit}, zoom) {
        commit('SET_ZOOM', zoom);
      },
      setCenter({commit}, center) {
        commit('SET_CENTER', center);
      },
      setCenterLat({commit}, lat) {
        commit('SET_CENTER_LAT', lat);
      },
      setCenterLng({commit}, lng) {
        commit('SET_CENTER_LNG', lng);
      }
    }
  });
}
