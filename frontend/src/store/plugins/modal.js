import * as types from '../mutations-types';

/**
 * плагин хранилища модального окна
 *
 * @param {Object} store хранилище
 *
 * @return {undefined}
 */
export default function(store) {
  store.registerModule('modal', {
    namespaced: true,

    state() {
      return {
        active: false,
        action: null,
        params: {}
      };
    },

    getters: {
      active(state) {
        return state.active;
      },

      action(state) {
        return state.action;
      },

      params(state) {
        return state.params;
      }
    },

    mutations: {
      [types.MODAL_OPEN](state) {
        state.active = true;
      },
      [types.MODAL_CLOSE](state) {
        state.active = false;
      },
      [types.MODAL_CHANGE_ACTION](state, action) {
        state.action = action;
      },
      MODAL_CHANGE_PARAMS(state, params = {}) {
        state.params = params;
      }
    },

    actions: {
      open({commit}, action) {
        commit(types.MODAL_OPEN);
        commit(types.MODAL_CHANGE_ACTION, action);
      },

      close({commit}) {
        commit(types.MODAL_CLOSE);
        commit(types.MODAL_CHANGE_ACTION, null);
      }
    }
  });
}
