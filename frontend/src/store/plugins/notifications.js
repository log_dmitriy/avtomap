import Vue from 'vue';

/**
 * плагин хранилища нотификаций
 *
 * @param {Object} store хранилище
 *
 * @return {undefined}
 */
export default function(store) {
  store.registerModule('notifications', {
    namespaced: true,

    actions: {
      async getPointNotification({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'notifications.point',
          params: payload.params
        });

        try {
          const response = await Vue.http.get(url);
          const {checked} = response.body;

          await dispatch('fireResponseSuccess', response, {root: true});

          return checked;
        }
        catch (error) {
          await dispatch('fireResponseError', error, {root: true});
        }
      },

      async changePointNotification({dispatch, rootGetters}, payload) {
        const url = rootGetters['api/fullUrl']({
          name: 'notifications.point.change',
          params: payload.params
        });

        try {
          const response = await Vue.http.post(url, payload.body);

          await dispatch('fireResponseSuccess', response, {root: true});
        }
        catch (error) {
          await dispatch('fireResponseError', error, {root: true});
        }
      }
    }
  });
}
