import Vue from 'vue';
import Vuetify from 'vuetify';
import languageSelector from '@/components/languageSelector';

Vue.use(Vuetify);

describe('languageSelector.vue', () => {
  it('имеется метод created', () => {
    expect(typeof languageSelector.created).to.equal('function');
  })
})
