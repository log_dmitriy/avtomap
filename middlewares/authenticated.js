exports.name = 'authenticated';
exports.middleware = function(req, res, next) {
  if (req.isUnauthenticated()) {
    res.throw(401, next);
  }
  else {
    next();
  }
};
