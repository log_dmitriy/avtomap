exports.name = 'authorized';
exports.middleware = async (req, res, next) => {
  const router = req.app.get('router');
  const {authorize} = req.app.get('aaa');
  const routeName = router.getRouteName(req);

  if (routeName === void 0) {
    const error = new Error(
      'Проверка авторизации у неизвестного маршрута');

    next(error);

    return;
  }

  const authorized = await authorize(req, routeName);

  if (!authorized) {
    res.throw(401, next);

    return;
  }

  next();
};
