const url = require('url');

exports.name = 'global';
exports.middleware = function(req, res, next) {
  req.fullurl = url.resolve(req.siteurl, req.originalUrl);

  next();
};
