exports.name = 'global';
exports.middleware = function(req, res, next) {
  let roles;

  if (req.isUnauthenticated() || req.user === void 0) {
    roles = ['guest'];
  }
  else {
    roles = req.user.roles;
  }

  req.roles = roles;

  next();
};
