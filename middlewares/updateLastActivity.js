/**
 * Created by Rochnyak Dmitriy on 12.09.17.
 */
exports.name = 'global';
exports.middleware = function(req, res, next) {
  const models = req.app.get('models');
  const User = models.get('user');

  if (req.user) {
    const now = new Date();
    const lastActivity = req.user.lastActivity || now;
    const diff = (now - lastActivity) / 1000 / 60;

    // обновляем каждые 5 минут
    if (diff > 5) {
      req.user.lastActivity = now;

      // Необязательно заставлять пользователя ждать
      User.storage.save(req.user).catch(error => {
        console.error(error);
      });
    }
  }

  next();
};
