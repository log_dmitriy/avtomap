exports.name = 'owner';
exports.middleware = function(req, res, next) {
  // админу все дороги открыты
  if (req.roles.includes('admin')) {
    next();

    return;
  }

  if (
    !('user' in req.parameters) ||
    String(req.user._id) !== String(req.parameters.user._id)
  ) {
    res.throw(401, next);
  }


  next();
};
