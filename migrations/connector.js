const path = require('path');

const rootPath = path.join(__dirname, '..');
const config = require(path.join(rootPath, 'configs'));
const dbConnector = require(path.join(rootPath, 'app', 'database'));
const configs = config.get('storage');

module.exports = async function(collectionName) {
  const db = await dbConnector(configs);

  return {db, collection: db.collection(collectionName)};
};
