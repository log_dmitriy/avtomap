const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('user');

    await collection.createIndex({activated: 1});
    await collection.createIndex({login: 1}, {unique: true});
    await collection.createIndex({email: 1}, {unique: true});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('user');

    await collection.dropIndexes();
    await collection.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};
