const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('confirmuser');

    await collection.createIndex({activateKey: 1, email: 1});
    await collection.createIndex({userId: 1});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('confirmuser');

    await collection.dropIndexes();
    await collection.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};
