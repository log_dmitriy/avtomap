const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('confirmresetpassword');

    await collection.createIndex({confirmKey: 1, email: 1});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('confirmresetpassword');

    await collection.dropIndexes();
    await collection.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};
