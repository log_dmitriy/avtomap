const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('point');

    await collection.createIndex({x: 1, y: 1});
    await collection.createIndex({country: 1, region: 1});
    await collection.createIndex({'author._id': 1});
    await collection.createIndex({created: 1});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('point');

    await collection.dropIndexes();
    await collection.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};
