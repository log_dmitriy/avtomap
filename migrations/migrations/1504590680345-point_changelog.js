const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('pointchangelog');

    await collection.createIndex({pointId: 1});
    await collection.createIndex({created: 1});
    await collection.createIndex({'author._id': 1});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('pointchangelog');

    await collection.dropIndexes();
    await collection.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};
