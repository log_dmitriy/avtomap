const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('comment');

    await collection.createIndex({'author._id': 1});
    await collection.createIndex({roles: 1});
    await collection.createIndex({parentId: 1});
    await collection.createIndex({pointId: 1});
    await collection.createIndex({created: 1});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('comment');

    await collection.dropIndexes();
    await collection.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};
