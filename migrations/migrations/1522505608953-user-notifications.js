const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('user');
    const notifPoinCol = db.collection('pointnotification');
    const notifCommentCol = db.collection('commentnotification');

    await collection.createIndex({pointNotification: 1});
    await collection.createIndex({commentNotification: 1});

    await notifPoinCol.createIndex({userId: 1, pointId: 1});
    await notifCommentCol.createIndex({userId: 1, pointId: 1});
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('user');
    const notifPoinCol = db.collection('pointnotification');
    const notifCommentCol = db.collection('commentnotification');

    await collection.dropIndex({pointNotification: 1});
    await collection.dropIndex({commentNotification: 1});

    await notifPoinCol.dropIndexes();
    await notifPoinCol.drop();
    await notifCommentCol.dropIndexes();
    await notifCommentCol.drop();
  })()
  .then(() => next())
  .catch(error => next(error));
};


