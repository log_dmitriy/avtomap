const connector = require('../connector');

exports.up = function(next) {
  (async function() {
    const {db, collection} = await connector('__COLLECTION_NAME__');

    // код писать сюда
  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('__COLLECTION_NAME__');

    // код писать сюда
  })()
  .then(() => next())
  .catch(error => next(error));
};
