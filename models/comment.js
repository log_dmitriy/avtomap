const IModel = require('../app/managers/models/imodel');

/** класс лога изменений точки */
class Comment extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields = {}) {
    super(fields);
  }
}

module.exports = Comment;
