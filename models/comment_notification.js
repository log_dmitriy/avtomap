const IModel = require('../app/managers/models/imodel');

/** класс модели подписки пользователя на комментарии точки */
class CommentNotification extends IModel {}

module.exports = CommentNotification ;
