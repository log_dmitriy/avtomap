const crypto = require('crypto');

const IModel = require('../app/managers/models/imodel');

/** класс модели восстановления пароля */
class ConfirmResetPassword extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields) {
    if (!fields.confirmKey) {
      fields.confirmKey = ConfirmResetPassword.generateKey();
    }

    super(fields);
  }

  /**
   * генерирует ключ
   *
   * @return {String}
   */
  static generateKey() {
    const hash = crypto.createHash('sha256');
    const bytes = crypto.randomBytes(12);

    hash.update(bytes);

    return hash.digest('hex');
  }
}

module.exports = ConfirmResetPassword;
