const crypto = require('crypto');

const IModel = require('../app/managers/models/imodel');

/** класс модели подтверждения пользователя */
class ConfirmUser extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields) {
    if (!fields.activateKey) {
      fields.activateKey = ConfirmUser.generateKey();
    }

    super(fields);
  }

  /**
   * генерирует ключ
   *
   * @return {String}
   */
  static generateKey() {
    const hash = crypto.createHash('sha256');
    const bytes = crypto.randomBytes(12);

    hash.update(bytes);

    return hash.digest('hex');
  }
}

module.exports = ConfirmUser;
