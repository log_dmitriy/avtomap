const path = require('path');
const fsp = require('fs-promise');
const gm = require('gm');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

const IModel = require('../app/managers/models/imodel');
const watermarkPath = path.join(__dirname, 'assets', 'watermark.png');

const MAIN_SIZES = {width: 800, height: 800};
const THUMB_SIZES = {width: 80, height: 80};

function getNowDate() {
  const now = moment();

  return now.format('YYYY-MM-DD');
}

function createMainImage(filePath, imagePath) {
  return new Promise((resolve, reject) => {
    gm(filePath)
      .resize(MAIN_SIZES.width, MAIN_SIZES.height)
      .noProfile() // remove EXIF
      .quality(50)
      .command('composite')
      .gravity('Center')
      .in(watermarkPath)
      .write(imagePath, error => {
        if (error) {
          return reject(error);
        }

        resolve(imagePath);
      });
  });
}
function createThumbnail(imagePath, thumbnailPath) {
  return new Promise((resolve, reject) => {
    gm(imagePath)
      .resize(THUMB_SIZES.width, THUMB_SIZES.height)
      .noProfile() // remove EXIF
      .write(thumbnailPath, error => {
        if (error) {
          return reject(error);
        }

        resolve(thumbnailPath);
      });
  });
}

async function createImages(filePath, uploadPath) {
  const dir = path.join(uploadPath, getNowDate(), uuidv1());

  const imagePath = path.format({
    dir,
    name: 'main',
    ext: '.jpg'
  });
  const thumbnailPath = path.format({
    dir,
    name: 'thumbnail',
    ext: '.jpg'
  });

  await fsp.mkdirp(dir);
  await createMainImage(filePath, imagePath);
  await createThumbnail(imagePath, thumbnailPath);

  return {
    imagePath,
    thumbnailPath
  };
}

/** класс файла */
class File extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @returns {undefined}
   */
  constructor(fields = {}) {
    super(fields);
  }

  /**
   * Удаляет файл со всеми его копиями
   * 
   * @param {String}  publicPath  Путь к директории с файлами
   * 
   * @returns {Promise}
   */
  async remove(publicPath) {
    if (!this.path) {
      return;
    }

    const dir = path.join(publicPath, path.dirname(this.path));

    await fsp.emptyDir(dir);
    await fsp.rmdir(dir);
  }

  /**
   * Создает файл-изображение из файла-источника
   * 
   * @param {String}  file        путь к файл-источник
   * @param {String}  uploadPath  Относительный путь к картинкам
   * @param {String}  publicPath  Путь к директории с файлами
   * 
   * @returns {Promise}
   */
  static async createImageFromFile(file, uploadPath, publicPath) {
    if (!['image/png', 'image/jpeg'].includes(file.type)) {
      throw new Error('Неизвестный mime-тип.');
    }

    const {imagePath, thumbnailPath} = await createImages(
      file.path, uploadPath, publicPath);
    const size = (await fsp.stat(imagePath)).size;

    function formatPath(fpath) {
      return `/${path.relative(publicPath, fpath)}`;
    }

    return new File({
      size,
      path: formatPath(imagePath),
      thumbnail: formatPath(thumbnailPath)
    });
  }
}

module.exports = File;
