const IModel = require('../app/managers/models/imodel');

/** класс модели точки */
class Point extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields = {}) {
    super(fields);

    this.hiddenList = ['location', 'updated'];
  }
}

module.exports = Point;
