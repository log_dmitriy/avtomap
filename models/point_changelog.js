const moment = require('moment');
const IModel = require('../app/managers/models/imodel');
const Point = require('./point');

const blackList = ['pointId', 'author', 'created', '_id',
  'localeTransformer'];

/** класс лога изменений точки */
class PointChangelog extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields = {}) {
    if (fields.created === void 0) {
      fields.created = new Date();
    }

    super(fields);

    this.localeTransformer = str => str;
  }

  /**
   * Запоминает разницу между старым и новым значением поля
   *
   * @param {String} fieldName  название поле
   * @param {*}      oldValue   старое значение
   * @param {*}      newValue   новое значение
   */
  add(fieldName, oldValue, newValue) {
    const result = PointChangelog.diffField(
      fieldName, oldValue, newValue);

    if (result === void 0) {
      return;
    }

    this.fields[fieldName] = result;
  }

  /**
   * @param localeTransformer
   */
  setLocaleTransformer(localeTransformer) {
    this.localeTransformer = localeTransformer;
  }


  /**
   * Находит разницу между старым и новым значением поля
   *
   * @param {String} fieldName  название поле
   * @param {*}      oldValue   старое значение
   * @param {*}      newValue   новое значение
   */
  static diffField(fieldName, oldValue, newValue) {
    if (oldValue === newValue) {
      return;
    }

    if (oldValue === null) {
      const info = Point.getFieldInfo(fieldName);

      if (info.default === newValue) {
        return;
      }
    }

    let result;

    if (fieldName === 'childrens') {
      result = PointChangelog.diffChilds(oldValue, newValue);

      if (result === false) {
        return;
      }
    }
    else if (Array.isArray(oldValue) || Array.isArray(newValue)) {
      result = PointChangelog.diffArrayField(oldValue, newValue);

      if (result === false) {
        return;
      }
    }
    else {
      result = {oldValue, newValue};
    }

    return result;
  }

  /**
   * Находит разницу между старым и новым массивами,
   *  если она есть
   *
   * @param {Array}      [oldValue]   старый массив
   * @param {Array}      [newValue]   новый массив
   *
   * @return {Object|Boolean}
   */
  static diffArrayField(oldValue = [], newValue = []) {
    const removed = [];
    const added = [];

    // поиск удаленных элементов
    oldValue.forEach(item => {
      if (newValue.includes(item)) {
        return;
      }

      removed.push(item);
    });

    // поиск добавленных элементов
    newValue.forEach(item => {
      if (oldValue.includes(item)) {
        return;
      }

      added.push(item);
    });

    // никаких изменений
    if (removed.length === 0 && added.length === 0) {
      return false;
    }

    return {removed, added};
  }

  /**
   * Находит разницу между старым и новым массивами потомков
   *
   * @param {Array}      [oldValue]   старый массив потомков
   * @param {Array}      [newValue]   новый массив потомков
   *
   * @return {Object|Boolean}
   */
  static diffChilds(oldValue = [], newValue = []) {
    const lengthOld = oldValue.length;
    const lengthNew = newValue.length;

    if (lengthOld === 0 && lengthNew === 0) {
      return false;
    }

    const diffLength = lengthOld - lengthNew;
    const removed = diffLength > 0 ? diffLength : 0;
    const added = diffLength < 0 ? Math.abs(diffLength) : 0;
    const changed = [];

    const minLength = Math.min(lengthOld, lengthNew);

    for (let ind = 0; ind < minLength; ind++) {
      const oldChild = oldValue[ind];
      const newChild = newValue[ind];
      const changedChild = {};

      for (const fieldName in oldChild) {
        const ov = oldChild[fieldName];
        const nv = newChild[fieldName];
        const result = PointChangelog.diffField(fieldName, ov, nv);

        if (result === void 0) {
          continue;
        }

        changedChild[fieldName] = result;
      }

      changed.push(changedChild);
    }

    return {added, removed, changed};
  }


  /**
   * @param subItem
   * @returns {string}
   */
  toFieldsString(subItem) {
    const item = subItem || this.fields;

    let stroke = '';

    for (const fieldName in item) {
      if (
        (item.hasOwnProperty && item.hasOwnProperty(fieldName))
        ||  blackList.includes(fieldName)
      ) {
        continue;
      }

      const info = Point.normalizeField({
        name: fieldName,
        force: true
      });
      const changedField = item[fieldName];
      const complete = this.fieldString(changedField, info);
      const completeStr = Array.isArray(complete) ?
        complete.join(' ') : complete;

      stroke += `${completeStr} `;
    }

    return stroke;
  }

  /**
   * @param field
   * @param info
   * @returns {*}
   */
  fieldString(field, info) {
    if (info.type === 'array' && field.added) {
      return this.__arrayString(field, info);
    }
    else if (info.type === 'other-point') {
      return this.__childPointsString(field, info);
    }
    else if (info.type === 'boolean') {
      return this.__booleanString(field, info);
    }
    else {
      return this.__otherString(field, info);
    }
  }

  /**
   * @param field
   * @param info
   * @returns {*[]}
   * @private
   */
  __arrayString(field, info) {
    const isList = info.list !== void 0;

    const reduceItem = (cur, value) => {
      const sep = (cur === '') ? '' : ', ';

      if (isList) {
        value = info.list.find(
          item => item.value === value).text;
      }

      value = this.localeTransformer(value);

      return cur + sep + value;
    };

    const label = this.localeTransformer(info.label);
    const added = field.added.reduce(reduceItem, '');
    const removed = field.removed.reduce(reduceItem, '');

    const tmplAdd = (added === '') ? '' :
      this.localeTransformer('add_array_field', {label, added});
    const tmplRemoved = (removed === '') ? '' :
      this.localeTransformer('remove_array_field', {label, removed});

    return [tmplAdd, tmplRemoved];
  }

  /**
   * @param field
   * @param info
   * @returns {*[]}
   * @private
   */
  __childPointsString(field, info) {
    const diffFields = this.localeTransformer('diff_other-point_field', {
      removed: field.removed,
      added: field.added
    });

    const changedFields = field.changed.map((item, ind) => {
      const index = ind + 1;
      const changed = this.toFieldsString(item);

      if (!changed) {
        return '';
      }

      return this.localeTransformer('change_other-point_child',
        {changed, index});
    });

    return [diffFields, changedFields.join(' ')];
  }

  /**
   * @param field
   * @param info
   * @private
   */
  __booleanString(field, info) {
    const label = this.localeTransformer(info.label);
    const isAdd = field.newValue === true;
    const templateKey = isAdd ?
      'add_boolean_field' : 'remove_boolean_field';

    return this.localeTransformer(templateKey, {label});
  }

  /**
   * @param field
   * @param info
   * @private
   */
  __otherString(field, info) {
    const isList = Boolean(info.list);
    const isDate = info.type === 'date';
    const label = this.localeTransformer(info.label);

    const formatValue = value => {
      if (value === null) {
        return value;
      }

      if (isList) {
        value = info.list.find(item => item.value === value).text;
        value = this.localeTransformer(value);
      }
      else if (isDate) {
        value = moment(value).format('YYYY-MM-DD HH:mm:ss');
      }

      return value;
    };

    const newValue = formatValue(field.newValue);
    const oldValue = formatValue(field.oldValue);

    if (newValue === null) {
      return this.localeTransformer('remove_field', {label});
    }

    if (oldValue === null) {
      return this.localeTransformer('add_field',
        {label, value: newValue});
    }

    return this.localeTransformer('change_field',
      {label, oldValue, newValue});
  }
}

module.exports = PointChangelog;
