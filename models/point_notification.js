const IModel = require('../app/managers/models/imodel');

/** класс модели подписки пользователя на точку */
class PointNotification extends IModel {}

module.exports = PointNotification;
