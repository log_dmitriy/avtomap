/* eslint no-template-curly-in-string: off */

const crypto = require('crypto');

const IModel = require('../app/managers/models/imodel');

/** класс модели пользователя */
class User extends IModel {
  /**
   * конструктор
   *
   * @param {Object} fields поля объекта
   *
   * @return {undefined}
   */
  constructor(fields) {
    super(fields);

    this.hiddenList = ['password', 'salt'];
  }

  /**
   * устанавливает пароль
   *
   * @param {String} password пароль
   *
   * @return {undefined}
   */
  setPassword(password) {
    this.fields.salt = User.__generateSalt();
    this.fields.password = this.__cipherPassword(password);
  }

  /**
   * проверяет пароль на соответствие текущему
   *
   * @param  {String} password проверяемый пароль
   *
   * @return {Boolean}
   */
  checkPassword(password) {
    const checkablePassword = this.__cipherPassword(password);

    return checkablePassword === this.fields.password;
  }

  /**
   * генерирует зашифрованный пароль
   *
   * @param  {String} password пароль
   *
   * @return {String} зашифрованный пароль
   *
   * @private
   */
  __cipherPassword(password) {
    if (!password) {
      throw TypeError('Аргумент password обязателен!');
    }

    const cipher = crypto.createCipher('aes192', password);

    cipher.write(this.fields.salt);

    return cipher.final('hex');
  }

  /**
   * возвращает информационные значения пользователя
   *
   * @returns {Object}
   */
  getInfoFields() {
    return {
      _id: this.fields._id,
      login: this.fields.login,
      email: this.fields.email
    };
  }

  /**
   * генерирует соль
   *
   * @return {String}
   *
   * @private
   */
  static __generateSalt() {
    const hash = crypto.createHash('sha256');
    const bytes = crypto.randomBytes(16);

    hash.update(bytes);

    return hash.digest('hex');
  }
}

module.exports = User;
