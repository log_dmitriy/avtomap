const handler = async (req, res, next, values) => {
  const models = req.app.get('models');
  const ConfirmUser = models.get('confirm_user');

  const row = await ConfirmUser.storage.get({
    activateKey: values.activateKey,
    email: values.email
  });

  if (!row[ConfirmUser.storage.idKey]) {
    res.throw(404);
  }

  req.parameters.confirmUser = new ConfirmUser(row);

  next();
};

module.exports = {
  name: ['email', 'activateKey'],
  handler,
  key: 'register',
  join: true
};
