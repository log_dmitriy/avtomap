const handler = async (req, res, next, id) => {
  const models = req.app.get('models');
  const Comment = models.get('comment');

  const row = await Comment.storage.getById(id);

  if (!row[Comment.storage.idKey]) {
    res.throw(404);
  }

  req.parameters.comment = new Comment(row);

  next();
};

module.exports = {
  name: 'id',
  handler,
  key: 'comment'
};
