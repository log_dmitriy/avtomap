const handler = async (req, res, next, values) => {
  const models = req.app.get('models');
  const ConfirmResetPassword = models.get('confirm_reset_password');

  const row = await ConfirmResetPassword.storage.get({
    confirmKey: values.confirmKey,
    email: values.email
  });

  if (!row[ConfirmResetPassword.storage.idKey]) {
    res.throw(404);
  }

  req.parameters.confirmResetPassword = new ConfirmResetPassword(row);

  next();
};

module.exports = {
  name: ['email', 'confirmKey'],
  handler,
  key: 'reset_password',
  join: true
};
