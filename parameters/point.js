const handler = async (req, res, next, id) => {
  const models = req.app.get('models');
  const Point = models.get('point');

  const row = await Point.storage.getById(id, {location: 0});

  if (!row[Point.storage.idKey]) {
    res.throw(404);
  }

  req.parameters.point = new Point(row);

  next();
};

module.exports = {
  name: 'id',
  handler,
  key: 'point'
};
