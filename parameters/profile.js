const handler = async (req, res, next, login) => {
  const models = req.app.get('models');
  const User = models.get('user');

  const row = await User.storage.get({login});

  if (!row[User.storage.idKey]) {
    res.throw(404);
  }

  req.parameters.user = new User(row);

  next();
};

module.exports = {
  name: 'login',
  handler,
  key: 'global'
};
