const countries = async (req, res) => {
  const cache = req.app.get('cache');
  const values = await cache.getSet('countries');

  res.status(200);
  res.send(values);
};

const regions = async (req, res) => {
  const cache = req.app.get('cache');
  const values = await cache.getSet('regions');

  res.status(200);
  res.send(values);
};

module.exports = {
  router: {
    'addresses.countries': {
      method: 'get',
      path: '/countries',
      middlewares: ['authorized'],
      handlers: [countries]
    },
    'addresses.regions': {
      method: 'get',
      path: '/regions',
      middlewares: ['authorized'],
      handlers: [regions]
    }
  },
  mount: '/addresses'
};
