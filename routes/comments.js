/* eslint max-len: off */

const sanitizeHtml = require('sanitize-html');
const {sendAddComment} = require('../app/notifications');

/**
 * проверяет может ли автор работать с роутом
 *
 * @param {Object}  req
 * @param {Object}  comment
 *
 * @returns {Boolean}
 */
function checkAuthor(req, comment) {
  if (req.roles.includes('admin') || req.roles.includes('moderator')) {
    return true;
  }

  return String(req.user._id) === String(comment.author._id);
}

/**
 * очищает текст
 *
 * @param {String} text
 *
 * @return {String}
 */
function clean(text) {
  return sanitizeHtml(text, {
    allowedTags: []
    // allowedAttributes: {}
  });
}

const list = async (req, res) => {
  const models = req.app.get('models');
  const Comment = models.get('comment');
  const point = req.parameters.point;
  const filter = {pointId: point._id};
  const sort = {created: 1};
  const agg = [{
    $group: {
      _id: {$ifNull: ['$parentId', '$_id']},
      created: {$first: '$created'},
      childrens: {
        $push: '$$ROOT'
      }
    }
  }];

  const aggRows = await Comment.storage.getAll(filter, {sort, agg});
  const rows = aggRows
    .reduce((accum, value) => accum.concat(value.childrens), [])
    .map(comment => new Comment(comment).toJSON());

  res.status(200);
  res.send(rows);
};

const afterHook = async (point, user, models) => {
  const fields = {
    userId: user._id,
    pointId: point._id
  };

  const PointNotification = models.get('point_notification');
  const notification = await PointNotification.storage.get(fields);

  if (!notification._id) {
    const objNotification = new PointNotification(fields);

    await PointNotification.storage.save(objNotification);
  }
};

function prepareBodyFiles(files = []) {
  let savedFiles = (files || []);

  if (!Array.isArray(savedFiles)) {
    savedFiles = [savedFiles];
  }

  savedFiles = savedFiles.map(sfile => JSON.parse(sfile));

  return savedFiles;
}

async function filesLoad(req, res, prevFiles = []) {
  const config = req.app.get('config');
  const models = req.app.get('models');

  const File = models.get('file');
  const publicPath = config.get('paths.public');
  const uploadPath = config.get('paths.files');

  const rawFiles = req.files.files || [];
  const savedFiles = prepareBodyFiles(req.body.files);

  if ((savedFiles.length + rawFiles.length) > 4) {
    res.throw(400, 'Максимальное кол-во изображений - 4');
  }

  const files = (await Promise.all(
    rawFiles.map(file =>
      File.createImageFromFile(file, uploadPath, publicPath)
    )
  )).map(file => file.toJSON());

  const deleteProcesses = [];

  const filtered = prevFiles.filter(file => {
    if (!savedFiles.find(saved => saved.path === file.path)) {
      deleteProcesses.push(
        new File(file).remove(publicPath)
      );

      return false;
    }

    return true;
  });

  await Promise.all(deleteProcesses);

  return filtered.concat(files);
}

const add = async (req, res) => {
  const models = req.app.get('models');
  const Comment = models.get('comment');

  const body = req.body;

  const point = req.parameters.point;
  const comment = new Comment({
    text: clean(body.text),
    author: req.user.getInfoFields(),
    pointId: point._id
  });

  if (body.parentId) {
    const parent = await Comment.storage.get({
      _id: Comment.storage.normalizeID(body.parentId),
      parentId: null
    });

    if (!parent) {
      res.throw(400, 'Нельзя ответить на данный комментарий');
    }

    comment.parentId = parent._id;
  }

  try {
    await comment.validate();
  }
  catch (error) {
    res.throw(400, 'Комментарий не должен быть пустым');
  }

  comment.created = new Date();
  comment.files = await filesLoad.call(this, req, res);

  await Comment.storage.save(comment);
  await afterHook(point, req.user, models);

  sendAddComment(point, req.user, comment, models,
    req.app.get('mailer'), req.siteurl);

  res.status(200);
  res.send(comment.toJSON());
};

const change = async (req, res) => {
  const models = req.app.get('models');
  const Comment = models.get('comment');

  const body = req.body;
  const comment = req.parameters.comment;

  if (!checkAuthor(req, comment)) {
    res.throw(401);
  }

  comment.text = clean(body.text);

  try {
    await comment.validate();
  }
  catch (error) {
    res.throw(400, void 0, error);
  }
  
  comment.updated = new Date();
  comment.files = await filesLoad.call(this, req, res, comment.files);

  await Comment.storage.save(comment);

  res.status(200);
  res.send(comment.toJSON());
};

const remove = async (req, res) => {
  const config = req.app.get('config');
  const publicPath = config.get('paths.public');

  const models = req.app.get('models');
  const Comment = models.get('comment');
  const File = models.get('file');

  const comment = req.parameters.comment;

  if (!checkAuthor(req, comment)) {
    res.throw(401);
  }

  await Comment.storage.delete(comment);

  await Promise.all(
    (comment.files || [])
      .map(file => new File(file).remove(publicPath))
  );

  if (comment.parentId === null) {
    await Comment.storage.deleteAll({parentId: comment._id});
  }

  res.status(200);
  res.send({});
};

const complain = async (req, res) => {
  const mailer = req.app.get('mailer');

  const models = req.app.get('models');
  const User = models.get('user');

  const user = req.user;
  const comment = req.parameters.comment;
  const point = req.parameters.point;
  const text = clean(req.body.text || '');

  const authorUrl = `${req.siteurl}/profile/${user.login}`;
  const commentUrl = `${req.siteurl}/?zoom=16&center=${point.x},${point.y}&point=${point._id}&comments=${point._id}&comment=${comment._id}`;

  const moderators = await User.storage.getAll({
    roles: 'moderator'
  }, {
    fields: {email: 1}
  });
  // const emails = moderators.map(moder => moder.email);

  const settings = ['complain', 'ru', {
    authorUrl,
    commentUrl,
    text,
    commentText: comment.text,
    userLogin: user.login
  }, {subject: 'Пожаловаться на комментарий'}];
  const promises = moderators.map(({email}) =>
    mailer.sendTemplate(email, ...settings));

  await Promise.all(promises);

  res.status(200);
  res.send({
    message: 'Сообщение отправлено'
  });
};

module.exports = {
  router: {
    'comments.list': {
      method: 'get',
      path: '/:point_id',
      handlers: [list]
    },
    'comments.add': {
      method: 'post',
      path: '/:point_id/add',
      middlewares: ['authorized'],
      handlers: [add]
    },
    'comments.change': {
      method: 'put',
      path: '/:point_id/change/:comment_id',
      middlewares: ['authorized'],
      handlers: [change]
    },
    'comments.delete': {
      method: 'delete',
      path: '/:point_id/delete/:comment_id',
      middlewares: ['authorized'],
      handlers: [remove]
    },

    'comments.complain': {
      method: 'post',
      path: '/:point_id/complain/:comment_id',
      middlewares: ['authorized'],
      handlers: [complain]
    }
  },
  mount: '/comments',
  paramsKey: ['point', 'comment']
};
