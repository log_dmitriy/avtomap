/* eslint max-statements: off */

const runExport = require('../app/export/run');

const Iconv = require('iconv').Iconv;
const encoder = new Iconv('UTF-8', 'CP1251');
const zlib = require('zlib');

const exportFn = async (req, res) => {
  const Point = req.app.get('models').get('point');
  const body = req.body;

  let generator;
  let fillFn;

  try {
    const exportData = await runExport(body, req.i18n, Point);

    generator = exportData.generator;
    fillFn = exportData.fill;
  }
  catch (error) {
    if (error instanceof Error) {
      throw error;
    }

    res.throw(400, void 0, error);
  }

  const output = zlib.createGzip();

  output.pipe(res);

  res.setHeader('Content-Type', 'text/plain;charset=cp1251');
  res.setHeader('Content-Encoding', 'gzip');
  res.setHeader('Content-Disposition',
    `attachment; filename="${body.format}.txt"`);

  generator.on('data', chunk => {
    try {
      chunk = encoder.convert(chunk);
    }
    catch (error) {
      // no body
    }

    output.write(chunk);
  });

  generator.on('error', error => output.emit('error', error));
  generator.on('close', () => output.end());
  generator.on('finish', () => output.end());

  fillFn();

  generator.end();
};

module.exports = {
  router: {
    export: {
      method: 'post',
      path: '/',
      middlewares: ['authorized'],
      handlers: [exportFn]
    }
  },
  mount: '/export'
};
