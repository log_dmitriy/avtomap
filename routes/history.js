/* eslint max-statements: off */

/**
 * отдает фильтр для историй
 *
 * @returns {{created: {$gte: Date}}}
 */
function getDateFilter() {
  const startDate = new Date();

  startDate.setHours(startDate.getHours() - 24);

  return {
    created: {
      $gte: startDate
    }
  };
}

const counts = async (req, res) => {
  const query = req.query;

  const models = req.app.get('models');
  const Point = models.get('point');
  const PointCL = models.get('point_changelog');
  const Comment = models.get('comment');

  let body = {};

  if (query.pid) {
    const pointRow = await Point.storage.getById(query.pid, {location: 0});
    const changed = await PointCL.storage.count({
      pointId: Point.storage.normalizeID(query.pid)
    });

    if (!pointRow) {
      res.throw(404);
    }

    const point = (new Point(pointRow)).toJSON();

    body = {point, changed};
  }
  else {
    const filter = getDateFilter();

    const created = await Point.storage.count({});
    const changed = await PointCL.storage.count({});
    const comments = await Comment.storage.count({});

    const createdDay = await Point.storage.count(filter);
    const changedDay = await PointCL.storage.count(filter);
    const commentsDay = await Comment.storage.count(filter);

    body = {
      created, changed, comments,
      createdDay, changedDay, commentsDay
    };
  }

  res.status(200);
  res.send(body);
};

const created = async (req, res) => {
  const query = req.query;
  const Point = req.app.get('models').get('point');

  const filter = {};
  const sort = {created: -1};
  const page = query.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  if (query.pid) {
    filter[Point.storage.idKey] = Point.storage.normalizeID(query.pid);
  }

  let rows = await Point.storage.getAll(filter, {sort, skip, limit});

  rows = rows.map(row => {
    const instance = new Point(row);

    return instance.toJSON(['created']);
  });

  res.status(200);
  res.send(rows);
};

const changed = async (req, res) => {
  const query = req.query;
  const PointCL = req.app.get('models').get('point_changelog');

  const filter = {};
  const sort = {created: -1};
  const page = query.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  if (query.pid) {
    filter.pointId = PointCL.storage.normalizeID(query.pid);
  }

  let rows = await PointCL.storage.getAll(filter, {sort, limit, skip});

  rows = rows.map(row => {
    const instance = new PointCL(row);

    return instance.toJSON();
  });

  res.status(200);
  res.send(rows);
};

const comments = async (req, res) => {
  const models = req.app.get('models');
  const Point = models.get('point');
  const Comment = models.get('comment');

  const filter = {};
  const sort = {created: -1};
  const page = req.query.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  const rows = await Comment.storage.getAll(filter, {sort, limit, skip});
  const map = {};
  const pointIds = rows.map(row => {
    if (!map[row.pointId]) {
      map[row.pointId] = [];
    }

    map[row.pointId].push(row);

    return row.pointId;
  });

  const points = await Point.storage.getAll(
    {_id: {$in: pointIds}}, {x: 1, y: 1, _id: 1});

  points.forEach(point => {
    const comments = map[point._id];

    comments.forEach(comment => {
      comment.x = point.x;
      comment.y = point.y;
    });
  });

  res.status(200);
  res.send(rows);
};

module.exports = {
  router: {
    'history.counts': {
      method: 'get',
      path: '/counts',
      handlers: [counts]
    },
    'history.created': {
      method: 'get',
      path: '/created',
      handlers: [created]
    },
    'history.changed': {
      method: 'get',
      path: '/changed',
      handlers: [changed]
    },
    'history.comments': {
      method: 'get',
      path: '/comments',
      handlers: [comments]
    }
  },
  mount: '/history',
  paramsKey: 'history'
};
