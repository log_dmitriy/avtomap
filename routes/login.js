const authenticate = {
  method: 'post',
  path: '/login',
  handlers: [
    async (req, res) => {
      const {authenticate, AuthenticateError} = req.app.get('aaa');

      try {
        await authenticate(req, res);

        res.status(200);
        res.send({
          message: getMessage(),
          user: req.user.toJSON()
        });
      }
      catch (error) {
        if (error instanceof AuthenticateError) {
          res.throw(400, null, {
            message: getMessage()
          });
        }

        throw error;
      }

      /**
       * возвращает сообщение установленное пасспортом в сессию
       *
       * @return {String}
       */
      function getMessage() {
        const messages = req.session.messages || [];

        delete req.session.messages;

        return messages[0] || '';
      }
    }
  ]
};

const logout = {
  method: 'post',
  path: '/logout',
  middlewares: ['authenticated'],
  handlers: [(req, res) => {
    req.logOut();

    res.status(200);
    res.send({message: 'Вы вышли'});
  }]
};

const checkAuth = {
  method: 'get',
  path: '/check',
  handlers: [(req, res) => {
    const authenticated = req.isAuthenticated();
    const user = (req.user === void 0) ? {} : req.user.toJSON();

    res.status(200);
    res.send({authenticated, user});
  }]
};

const checkRole = {
  method: 'post',
  path: '/check/role',
  handlers: [async (req, res) => {
    const {authorize} = req.app.get('aaa');
    const authenticated = req.isAuthenticated();
    const resource = req.body.resource || '';
    let hasPerms = false;

    if (authenticated) {
      hasPerms = await authorize(req, resource);
    }

    res.status(200);
    res.send({hasPerms});
  }]
};

module.exports = {
  router: {
    'aaa.auth': authenticate,
    'aaa.logout': logout,
    'aaa.check': checkAuth,
    'aaa.checkrole': checkRole,
  },
  mount: '/auth'
};
