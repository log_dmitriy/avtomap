const user_change = async (req, res) => {
  const models = req.app.get('models');
  const User = models.get('user');

  const user = req.parameters.user;
  const {pointNotification = user.pointNotification} = req.body;

  user.pointNotification = Boolean(pointNotification);

  await User.storage.save(user);

  res.status(200);
  res.send({
    message: 'Данные пользователя успешно изменены'
  });
};

/**
 * @param modelype
 * @returns {Function}
 */
function getRouteChangeHandler(modelype) {
  return async (req, res) => {
    const models = req.app.get('models');
    const ModelNotification = models.get(modelype);

    const user = req.parameters.user;
    const point = req.parameters.point;
    const {subscribe = true} = req.body;

    const filter = {
      userId: user._id,
      pointId: point._id
    };
    const [notification] = await ModelNotification.storage.getAll(filter);

    const objNotification = new ModelNotification(
      Object.assign({}, notification, filter));

    if (subscribe) {
      await ModelNotification.storage.save(objNotification);
    }
    else if (notification) {
      await ModelNotification.storage.delete(objNotification);
    }

    res.status(200);
    res.send({
      message: 'Данные подписки сохранены'
    });
  };
}
const point_change = getRouteChangeHandler('point_notification');

/**
 * @param modelype
 * @returns {Function}
 */
function getRouteGetHandler(modelype) {
  return async (req, res) => {
    const models = req.app.get('models');
    const ModelNotification = models.get(modelype);

    const user = req.parameters.user;
    const point = req.parameters.point;

    const filter = {
      userId: user._id,
      pointId: point._id
    };
    const notification = await ModelNotification.storage.get(filter);

    res.status(200);
    res.send({
      checked: Boolean(notification._id)
    });
  };
}
const point_get = getRouteGetHandler('point_notification');

module.exports = {
  router: {
    'notifications.user': {
      method: 'post',
      path: '/user',
      middlewares: ['authorized', 'owner'],
      handlers: [user_change]
    },

    'notifications.point.change': {
      method: 'post',
      path: '/point/:point_id',
      middlewares: ['authorized', 'owner'],
      handlers: [point_change]
    },

    'notifications.point': {
      method: 'get',
      path: '/point/:point_id',
      middlewares: ['authorized', 'owner'],
      handlers: [point_get]
    }
  },
  mount: '/notifications/:login',
  paramsKey: ['profile', 'point']
};
