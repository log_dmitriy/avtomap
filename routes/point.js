/* eslint id-length: off */

const {sendChangePoint} = require('../app/notifications');

/**
 * фильтрует передаваемые параметры
 *
 * @param {Object} query
 *
 * @return {Object}
 */
function filterQuery(query = {}) {
  let filteredQuery;

  if (query.type === 'bounds') {
    const x1 = parseFloat(query.x1);
    const y1 = parseFloat(query.y1);
    const x2 = parseFloat(query.x2);
    const y2 = parseFloat(query.y2);

    if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2)) {
      return;
    }

    const southWest = [x1, y1];
    const northEast = [x2, y2];

    filteredQuery = {
      location: {
        $geoWithin: {
          $box: [southWest, northEast]
        }
      }
    };
  }

  return filteredQuery;
}

const list = async (req, res) => {
  const Point = req.app.get('models').get('point');
  const filter = filterQuery(req.query);

  if (filter === void 0) {
    res.throw(400, 'Неизвестный запрос');
  }

  let rows = await Point.storage.getAll(filter);

  rows = rows.map(row => {
    const instance = new Point(row);

    return instance.toJSON();
  });

  res.status(200);
  res.send(rows);
};

const detail = async (req, res) => {
  const User = req.app.get('models').get('user');
  const point = req.parameters.point;
  const pointInfo = req.parameters.point.toJSON();

  if (point.author !== void 0) {
    const author = await User.storage.getById(point.author);

    if (author[User.storage.idKey]) {
      pointInfo.author = (new User(author)).toJSON();
    }
  }

  res.status(200);
  res.send(pointInfo);
};


const beforeSave = async (point, req, res, {setAddrs}) => {
  const geocoder = req.app.get('geocoder');
  const cache = req.app.get('cache');

  if (setAddrs) {
    const {country=null, region} = await geocoder.reverse({
      lat: point.y,
      lon: point.x,
      language: 'ru'
    });

    await cache.addInSet('countries', country);

    if (country === 'RU') {
      await cache.addInSet('regions', region);
    }

    point.country = country;
    point.region = region;
  }

  point.location = [point.x, point.y];
  point.updated = new Date();
};
const setFieldsInPoint = (point, newValues, pointCL) => {
  const blackList = ['author', '_id'];

  for (const key in newValues) {
    if (
      !newValues.hasOwnProperty(key) ||
      blackList.includes(key)
    ) {
      continue;
    }

    const oldValue = point[key];
    const newValue = newValues[key];

    pointCL.add(key, oldValue, newValue);
    point[key] = newValue;
  }
};
const afterHook = async (point, user, models) => {
  const fields = {
    userId: user._id,
    pointId: point._id
  };

  const PointNotification = models.get('point_notification');
  const notification = await PointNotification.storage.get(fields);

  if (!notification._id) {
    const objNotification = new PointNotification(fields);

    await PointNotification.storage.save(objNotification);
  }
};

const add = async (req, res) => {
  const models = req.app.get('models');
  const Point = models.get('point');
  const body = req.body;
  const point = new Point(body);

  point.author = req.user.getInfoFields();

  try {
    await point.validate();
  }
  catch (error) {
    res.throw(400, void 0, error);
  }

  const cache = req.app.get('cache');
  const lastIdx = Number(await cache.getValue('last_idx')) + 1;

  point.created = new Date();
  point.idx = lastIdx;

  await beforeSave(point, req, res, {setAddrs: true});
  await Point.storage.save(point);
  await afterHook(point, req.user, models);

  // может не эвейтить?
  await cache.setValue('last_idx', lastIdx);

  res.status(200);
  res.send(point.toJSON());
};
const change = async (req, res) => {
  const models = req.app.get('models');
  const Point = models.get('point');
  const PointChangelog = models.get('point_changelog');

  const point = req.parameters.point;
  const body = req.body;
  const setAddrs = (point.x !== body.x) || (point.y !== body.y);
  const created = point.created;
  const pointCL = new PointChangelog();

  setFieldsInPoint(point, body, pointCL);

  point.normalize();

  try {
    const validatedFields = await point.validate();

    point.fields = validatedFields;
  }
  catch (error) {
    res.throw(400, void 0, error);
  }

  point.created = created;
  await beforeSave(point, req, res, {setAddrs});
  await Point.storage.save(point);
  await afterHook(point, req.user, models);

  pointCL.pointId = point._id;
  pointCL.author = req.user.getInfoFields();

  await PointChangelog.storage.save(pointCL);

  sendChangePoint(point, pointCL, models,
    req.app.get('mailer'), req.siteurl, req.i18n);

  res.status(200);
  res.send(point.toJSON());
};


const deletePoint = async (req, res) => {
  const models = req.app.get('models');
  const Point = models.get('point');
  const PointChangelog = models.get('point_changelog');
  const Comment = models.get('comment');
  const PointNotification = models.get('point_notification');
  const CommentNotification = models.get('comment_notification');

  const point = req.parameters.point;

  await Point.storage.delete(point);
  await PointChangelog.storage.deleteAll({pointId: point._id});
  await Comment.storage.deleteAll({pointId: point._id});
  await PointNotification.storage.deleteAll({pointId: point._id});
  await CommentNotification.storage.deleteAll({pointId: point._id});

  res.status(200);
  res.send({});
};

module.exports = {
  router: {
    'point.list': {
      method: 'get',
      path: '/',
      handlers: [list]
    },
    'point.detail': {
      method: 'get',
      path: '/:id',
      handlers: [detail]
    },
    'point.add': {
      method: 'put',
      path: '/',
      middlewares: ['authorized'],
      handlers: [add]
    },
    'point.change': {
      method: 'post',
      path: '/:id',
      middlewares: ['authorized'],
      handlers: [change]
    },
    'point.delete': {
      method: 'delete',
      path: '/:id',
      middlewares: ['authorized'],
      handlers: [deletePoint]
    }
  },
  mount: '/points',
  paramsKey: 'point'
};
