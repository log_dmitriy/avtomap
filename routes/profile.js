/* eslint max-statements: off */

const detail = async (req, res) => {
  const models = req.app.get('models');
  const Point = models.get('point');
  const PointCL = models.get('point_changelog');
  const Comment = models.get('comment');
  const user = req.parameters.user;

  const createdCnt = await Point.storage.count({
    'author._id': user._id});
  const updatedCnt = await PointCL.storage.count({
    'author._id': user._id});
  const commentCnt = await Comment.storage.count({
    'author._id': user._id});

  const body = {
    user: user.toJSON(),
    cnt: {
      created: createdCnt,
      changed: updatedCnt,
      comments: commentCnt
    }
  };

  res.status(200);
  res.send(body);
};

const change = async (req, res) => {
  const models = req.app.get('models');
  const User = models.get('user');

  const user = req.parameters.user;
  const {
    currentEmail = '', newEmail = '',
    currentPassword = '', newPassword = '',

    pointNotification
  } = req.body;

  const emailChange = (currentEmail !== '' || newEmail !== '');
  const passwordChange = (currentPassword !== '' || newPassword !== '');
  const notificationChange = (
    pointNotification !== user.pointNotification);

  if (!emailChange && !passwordChange && !notificationChange) {
    res.throw(400, 'необходимы данные для изменения');

    return;
  }

  if (emailChange) {
    if (currentEmail !== user.email) {
      res.throw(400, 'Неверно указан текущий email', {
        currentEmail: 'Неверно указан текущий email'
      });
    }

    user.email = newEmail;

    try {
      await user.validate();
    }
    catch (error) {
      error.newEmail = error.email;

      res.throw(400, void 0, error);
    }

    const emailUser = await User.storage.get({email: user.email});

    if (emailUser[User.storage.idKey]) {
      res.throw(400, void 0, {email: 'Такой email уже занят'});
    }
  }

  if (passwordChange) {
    if (!user.checkPassword(currentPassword)) {
      res.throw(400, 'Неверно указан текущий пароль', {
        currentPassword: 'Неверно указан текущий пароль'
      });
    }

    if (newPassword === '') {
      res.throw(400, 'пароль не может быть пустым', {
        newPassword: 'пароль не может быть пустым'
      });
    }

    user.setPassword(newPassword);
  }

  if (notificationChange) {
    user.pointNotification = pointNotification;
  }

  await User.storage.save(user);

  res.status(200);
  res.send({
    message: 'Данные пользователя успешно изменены'
  });
};

const createdPoints = async (req, res) => {
  const Point = req.app.get('models').get('point');
  const user = req.parameters.user;

  const filter = {'author._id': user._id};
  const sort = {created: -1};
  const page = req.query.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  let rows = await Point.storage.getAll(filter, {sort, skip, limit});

  rows = rows.map(row => {
    const instance = new Point(row);

    return instance.toJSON(['created']);
  });

  res.status(200);
  res.send(rows);
};
const changedPoints = async (req, res) => {
  const PointCL = req.app.get('models').get('point_changelog');
  const user = req.parameters.user;

  const filter = {'author._id': user._id};
  const sort = {created: -1};
  const page = req.query.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  let rows = await PointCL.storage.getAll(filter, {sort, limit, skip});

  rows = rows.map(row => {
    const instance = new PointCL(row);

    return instance.toJSON();
  });

  res.status(200);
  res.send(rows);
};
const comments = async (req, res) => {
  const models = req.app.get('models');
  const Point = models.get('point');
  const Comment = models.get('comment');

  const user = req.parameters.user;

  const filter = {'author._id': user._id};
  const sort = {created: -1};
  const page = req.query.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  const rows = await Comment.storage.getAll(filter, {sort, limit, skip});
  const map = {};
  const pointIds = rows.map(row => {
    if (!map[row.pointId]) {
      map[row.pointId] = [];
    }

    map[row.pointId].push(row);

    return row.pointId;
  });

  const points = await Point.storage.getAll(
    {_id: {$in: pointIds}}, {x: 1, y: 1, _id: 1});

  points.forEach(point => {
    const comments = map[point._id];

    comments.forEach(comment => {
      comment.x = point.x;
      comment.y = point.y;
    });
  });

  res.status(200);
  res.send(rows);
};

const changeRoles = async (req, res) => {
  const models = req.app.get('models');
  const User = models.get('user');

  const user = req.parameters.user;
  const role = req.body.role || 'user';

  if (role === 'admin' && !req.user.roles.includes('admin')) {
    res.throw(400, 'Только администратор может назначать администраторов');
  }

  user.roles = [role];

  await User.storage.save(user);

  res.status(200);
  res.send({
    message: 'Роли изменены'
  });
};

module.exports = {
  router: {
    'profile.detail': {
      method: 'get',
      path: '/',
      middlewares: ['authorized'],
      handlers: [detail]
    },
    'profile.change': {
      method: 'put',
      path: '/change',
      middlewares: ['authorized', 'owner'],
      handlers: [change]
    },
    'profile.points.created': {
      method: 'get',
      path: '/points/created',
      middlewares: ['authorized'],
      handlers: [createdPoints]
    },
    'profile.points.changed': {
      method: 'get',
      path: '/points/changed',
      middlewares: ['authorized'],
      handlers: [changedPoints]
    },
    'profile.points.comments': {
      method: 'get',
      path: '/points/comments',
      middlewares: ['authorized'],
      handlers: [comments]
    },

    'profile.change.roles': {
      method: 'put',
      path: '/change/roles',
      middlewares: ['authorized'],
      handlers: [changeRoles]
    }
  },
  mount: '/profile/:login',
  paramsKey: 'profile'
};
