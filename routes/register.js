/* eslint max-statements: off */

const HttpError = require('../app/errors/http');
const MailerError = require('../app/errors/mailer');

const firstStepRegister = async (req, res) => {
  const models = req.app.get('models');
  const mailer = req.app.get('mailer');
  const router = req.app.get('router');

  const User = models.get('user');
  const ConfirmUser = models.get('confirm_user');

  const {
    login = '',
    email = '',
    password = '',
    confirmPassword,
    confirmEmail
  } = req.body;

  if (password === '') {
    throw new HttpError(400, 'пароль не может быть пустым', {
      password: 'пароль не может быть пустым'
    });
  }

  if (confirmPassword !== password) {
    throw new HttpError(400, 'пароли должны совпадать', {
      confirmPassword: 'пароли должны совпадать'
    });
  }

  if (email !== confirmEmail) {
    throw new HttpError(400, 'email\'ы должны совпадать', {
      confirmEmail: 'email\'ы должны совпадать'
    });
  }

  const user = new User({login, email});

  user.setPassword(password);

  user.activated = false;
  user.roles = (user.login === 'Admin') ? ['admin'] : ['user'];
  user.registeredAt = new Date();
  user.lastActivity = new Date();

  try {
    await user.validate();

    const errors = {};
    const users = await User.storage.getAll({
      $or: [
        {email: user.email},
        {login: user.login}
      ]
    });

    for (const findUser of users) {
      if (findUser.login === user.login) {
        errors.login = 'Такой логин уже занят';
      }

      if (findUser.email === user.email) {
        errors.email = 'Такой email уже занят';
      }
    }

    if (Object.keys(errors).length > 0) {
      throw errors;
    }
  }
  catch (errors) {
    if (errors instanceof Error) {
      throw errors;
    }

    throw new HttpError(400, void 0, errors);
  }

  await User.storage.save(user);

  const confirmUser = new ConfirmUser({
    userId: user._id,
    email: user.email
  });

  await ConfirmUser.storage.save(confirmUser);

  try {
    await mailer.sendTemplate(user.email, 'confirm_register', req.lang, {
      url: req.siteurl + router.getUrl('confirmRegistration', {
        email: user.email,
        activateKey: confirmUser.activateKey
      })
    });
  }
  catch (error) {
    await ConfirmUser.storage.delete(confirmUser);
    await User.storage.delete(user);

    if (error instanceof MailerError) {
      throw new HttpError(400, 'не удалось отправить почту');
    }

    throw error;
  }

  res.status(200);
  res.send({
    message: 'На ваш почтовый адрес отправлено письмо с подтверждением'
  });
};

const secondStepRegister = async (req, res) => {
  const models = req.app.get('models');

  const confirmUser = req.parameters.confirmUser;
  const User = models.get('user');
  const ConfirmUser = models.get('confirm_user');
  const row = await User.storage.get({
    email: confirmUser.email
  });

  const user = new User(row);

  user.activated = true;

  await User.storage.save(user);
  await ConfirmUser.storage.delete(confirmUser);

  await new Promise((resolve, reject) => {
    req.login(user, error => {
      if (error) {
        return reject(error);
      }

      resolve();
    });
  });

  res.redirect('/');
};

module.exports = {
  router: {
    registration: {
      method: 'post',
      path: '/',
      handlers: [firstStepRegister]
    },
    confirmRegistration: {
      method: 'get',
      path: '/confirm/:email/:activateKey',
      handlers: [secondStepRegister]
    }
  },
  mount: '/registration',
  paramsKey: 'register'
};
