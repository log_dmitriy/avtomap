const HttpError = require('../app/errors/http');
const MailerError = require('../app/errors/mailer');

const resetPassword = async (req, res) => {
  const models = req.app.get('models');
  const mailer = req.app.get('mailer');

  const ConfirmResetPassword = models.get('confirm_reset_password');
  const User = models.get('user');

  const {email = ''} = req.body;

  const row = await User.storage.get({email});

  if (!row[User.storage.idKey]) {
    const message = 'пользователя с таким email\'ом не существует';

    throw new HttpError(400, message, {email: message});
  }

  const confirmResetPassword = new ConfirmResetPassword({email});

  try {
    await ConfirmResetPassword.storage.save(confirmResetPassword);

    const uri = `/reset/confirm/${email}/${confirmResetPassword.confirmKey}`;

    await mailer.sendTemplate(email, 'reset_password', req.lang, {
      url: req.siteurl + uri,
      login: row.login
    });
  }
  catch (error) {
    await ConfirmResetPassword.storage.delete(confirmResetPassword);

    if (error instanceof MailerError) {
      throw new HttpError(400, 'не удалось отправить почту');
    }

    throw error;
  }

  res.status(200);
  res.send({
    message: 'На ваш почтовый адрес отправлено письмо с подтверждением'
  });
};

const confirmPassword = async (req, res) => {
  const models = req.app.get('models');
  const User = models.get('user');
  const ConfirmResetPassword = models.get('confirm_reset_password');

  const {password, confirmPassword} = req.body;
  const confirmResetPassword = req.parameters.confirmResetPassword;

  if (password === '') {
    throw new HttpError(400, 'пароль не может быть пустым', {
      password: 'пароль не может быть пустым'
    });
  }
  else if (confirmPassword !== password) {
    throw new HttpError(400, 'пароли должны совпадать', {
      password: 'пароли должны совпадать'
    });
  }

  const row = await User.storage.get({
    email: confirmResetPassword.email
  });

  const user = new User(row);

  user.setPassword(password);

  await User.storage.save(user);
  await ConfirmResetPassword.storage.deleteAll(
    {email: confirmResetPassword.email});

  res.status(200);
  res.send({
    message: 'Пароль успешно изменен'
  });
};

module.exports = {
  router: {
    resetPassword: {
      method: 'post',
      path: '/',
      handlers: [resetPassword]
    },
    confirmPassword: {
      method: 'post',
      path: '/confirm/:email/:confirmKey',
      handlers: [confirmPassword]
    }
  },
  mount: '/reset',
  paramsKey: 'reset_password'
};
