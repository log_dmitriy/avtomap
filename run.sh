docker network create --driver=bridge avtomap-network

docker run -d \
  --name redis \
  --network=avtomap-network \
  -v ${PWD}/db:/data/db/redis \
  redis:3.2.11-alpine
docker run -d \
  --name mongodb \
  --network=avtomap-network \
  -v ${PWD}/db:/data/db \
  mongo:3.7.3-jessie --auth
docker run -d \
  --name avtomap_app \
  --network=avtomap-network \
  -p 8080:8080 \
  -v ${PWD}/app:/app/app \
  -v ${PWD}/bin:/app/bin \
  -v ${PWD}/Makefile:/app/Makefile \
  -v ${PWD}/caches:/app/caches \
  -v ${PWD}/configs:/app/configs \
  -v ${PWD}/frontend:/app/frontend \
  -v ${PWD}/lang:/app/lang \
  -v ${PWD}/logs:/app/logs \
  -v ${PWD}/mail_templates:/app/mail_templates \
  -v ${PWD}/middlewares:/app/middlewares \
  -v ${PWD}/migrations:/app/migrations \
  -v ${PWD}/models:/app/models \
  -v ${PWD}/parameters:/app/parameters \
  -v ${PWD}/public:/app/public \
  -v ${PWD}/routes:/app/routes \
  -v ${PWD}/seeds:/app/seeds \
  -v ${PWD}/test:/app/test \
  -v ${PWD}/.babelrc:/app/.babelrc \
  rochnyakdi/avtomap

# docker exec -it mongodb mongo admin
# db.createUser({
#   user: 'admin',
#   pwd: '',
#   roles: [ { role: 'userAdminAnyDatabase', db: 'admin' } ]
# });
# use avtomap;
# db.createUser(
#    {
#      user: 'avtomap',
#      pwd: '',
#      roles: ['readWrite', 'dbAdmin']
#    }
# );