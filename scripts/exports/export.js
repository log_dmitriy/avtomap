const config = require('../../configs');
const {create: createModelManager} = require('../../app/managers/models');
const {create: createI18nManager} = require('../../app/managers/i18n');
const dbConnector = require('../../app/database');
const runExport = require('../../app/export/run');

//const Iconv = require('iconv').Iconv;
//const encoder = new Iconv('UTF-8', 'CP1251');

let i18n;
let Point;

async function getGenerator(data) {
  if (!i18n || !Point) {
    const database = await dbConnector(config.get('storage'));
    const modelManager = await createModelManager(
      config.get('models'), database);
    const i18nManager = await createI18nManager(config.get('i18n'));

    i18n = i18nManager.get('ru');
    Point = modelManager.get('point');
  }

  return runExport(data, i18n, Point);
}

module.exports = async data => {
  let str = '';
  const {generator, fill} = await getGenerator(data);

  return new Promise((resolve, reject) => {
    generator.on('data', chunk => {
      //try {
      //  chunk = encoder.convert(chunk);
      //}
      //catch (error) {
      //  // no body
      //}

      str += chunk;
    });

    generator.on('error', error => reject(error));
    generator.on('close', () => resolve(str));
    generator.on('finish', () => resolve(str));

    fill();

    generator.end();
  });
}
