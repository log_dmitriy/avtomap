const path = require('path');
const {google} = require('googleapis');
const getExportBody = require('./export');

const TOKEN_PATH = path.join(__dirname, 'credentials.json');
const credentials = require('./client_secret.json');
const settings = require('./settings.json');

async function run() {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);
  const token = require(TOKEN_PATH);

  oAuth2Client.setCredentials(token);

  const drive = google.drive({
    version: 'v3',
    auth: oAuth2Client
  });

  await Promise.all(settings.map(async setting => {
    if (setting.confirmed) {
      const date = new Date();

      date.setDate(date.getDate() - setting.confirmed);
      setting.confirmed = date;
    }

    const body = await getExportBody(setting);

    if (body && setting.save_to) {
      await updateFile(drive, body, setting.save_to);
    }
  }));
}

async function updateFile(drive, body, fileId) {
  return new Promise((resolve, reject) => {
    drive.files.update({
      fileId: fileId,
      media: {
        mimeType: 'text/plain;charset=cp1251',
        body: body
      }
    }, (error, file) => {
      if (error) {
        return reject(error);
      }

      resolve(file);
    });
  });
}

/* function listFile(drive) {
  drive.files.list({}, (error, resp) => {
    if (error) {
      return console.error(error);
    }

    console.log(resp.data.files);
  });
}

run()
  .catch(error => console.error(error))
  .then(() => process.exit(0));
} */

run()
  .catch(error => console.error(error))
  .then(() => process.exit(0));
