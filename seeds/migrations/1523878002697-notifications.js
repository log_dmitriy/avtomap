const connector = require('../connector');

exports.up = up;
function up(next) {
  (async function() {
    const {db, collection} = await connector('user');

    const pointCol = db.collection('point');
    const pointCLCol = db.collection('pointchangelog');
    const commentCol = db.collection('comment');
    const notifPoinCol = db.collection('pointnotification');
    const notifCommentCol = db.collection('commentnotification');

    // Глобальные флаги
    await collection.updateMany({}, {$set: {
      pointNotification: true,
      commentNotification: true
    }});

    // Комментарии
    const notifComments = await commentCol.aggregate([
      {
        $group: {
          _id: {userId: '$author._id', pointId: '$pointId'}
        }
      },
      {
        $project: {
          userId: '$_id.userId',
          pointId: '$_id.pointId',
          _id: 0
        }
      }
    ]);
    const commArr = await notifComments.toArray();

    await notifCommentCol.insertMany(commArr);

    // Точки
    const notifPoint = await pointCol.aggregate([
      {
        $project: {
          userId: '$author._id',
          pointId: '$_id',
          _id: 0
        }
      }
    ]);
    /*const notifPointCL = await pointCLCol.aggregate([
      {
        $group: {
          _id: {userId: '$author._id', pointId: '$pointId'}
        }
      },
      {
        $project: {
          userId: '$_id.userId',
          pointId: '$_id.pointId',
          _id: 0
        }
      }/!*,
      {
        $count: 'cnt'
      }*!/
    ]);*/

    const pointArr = await notifPoint.toArray();
    //const pointCLArr = await notifPointCL.toArray();

    await notifPoinCol.insertMany(pointArr);

  })()
  .then(() => next())
  .catch(error => next(error));
};

exports.down = async function(next) {
  (async function() {
    const {db, collection} = await connector('user');
  })()
  .then(() => next())
  .catch(error => next(error));
};


up(function(err) {
  console.log(err);
})
