const assert = require('assert');
const path = require('path');
const request = require('supertest');
const fsp = require('fs-promise');
const express = require('express');

const config = require('../configs');
const filesDir = config.get('paths.files');
const uploadDir = config.get('bodyparser.uploadDir');
const bodyParser = require('../app/bodyparser');
const bodyparserMw = bodyParser({uploadDir});

describe('bodyparser', () => {
  it('Загрузка json данных', async () => {
    const server = createServer();

    await request(server).post('/')
      .set('Content-Type', 'application/json')
      .send({user: 'Ivan'})
      .expect(200)
      .expect({user: 'Ivan'});
  });

  it('Загрузка urlencoded данных', async () => {
    const server = createServer();

    await request(server).post('/')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send('name=Kolya')
      .send('name=Petr')
      .expect(200)
      .expect({name: ['Kolya', 'Petr']});
  });

  describe('Загрузка файлов: ', () => {
    const filePaths = [];

    before('заполнение путей к файлам', async () => {
      const files = await fsp.readdir(filesDir);

      files.forEach((filename) => {
        if (filename === 'tmp') {
          return;
        }

        const filePath = path.join(filesDir, filename);

        filePaths.push(filePath);
      });
    });

    afterEach('очистка tmp директории', async () => {
      const files = await fsp.readdir(uploadDir);

      for (let filename of files) {
        const filePath = path.join(uploadDir, filename);

        await fsp.unlink(filePath);
      }
    });

    it('один файл', async () => {
      const server = createServer();

      const {body:result} = await request(server).post('/')
        .set('Content-Type', 'multipart/form-data')
        .attach('first_file', filePaths[0])
        .expect(200);
      assert.ok(result.files, 'файл не загружен');

      const files = JSON.parse(result.files);
      assert.ok(files.first_file, 'файл first_file не загружен');

      const filePath = files.first_file.path;
      const fileStat = await fsp.stat(filePath);
      assert.ok(fileStat.isFile(), 'first_file не файл');
    });

    it('один файл и поля', async () => {
      const server = createServer();

      const {body:result} = await request(server).post('/')
        .set('Content-Type', 'multipart/form-data')
        .field('names', 'Dima')
        .field('names', 'Sergey')
        .attach('first_file', filePaths[0])
        .expect(200);

      assert.deepStrictEqual(result.names, ['Dima', 'Sergey'],
        'неправильно сформированы поля');
      assert.ok(result.files, 'файл не загружен');

      const files = JSON.parse(result.files);
      assert.ok(files.first_file, 'файл first_file не загружен');

      const filePath = files.first_file.path;
      const fileStat = await fsp.stat(filePath);
      assert.ok(fileStat.isFile(), 'first_file не файл');
    });

    it('несколько файлов', async () => {
      const server = createServer();

      const {body:result} = await request(server).post('/')
        .set('Content-Type', 'multipart/form-data')
        .attach('pictures', filePaths[0])
        .attach('pictures', filePaths[1])
        .attach('video', filePaths[2])
        .expect(200);
      assert.ok(result.files, 'файл не загружен');

      const files = JSON.parse(result.files);
      assert.ok(files.pictures, 'файлы pictures не загружены');
      assert.ok(files.video, 'файл video не загружен');

      for (let file of files.pictures) {
        const filePath = file.path;
        const fileStat = await fsp.stat(filePath);
        assert.ok(fileStat.isFile(), 'picture не файл');
      }

      const videoPath = files.video.path;
      const videoStat = await fsp.stat(videoPath);
      assert.ok(videoStat.isFile(), 'video не файл');
    });

    it('несколько файлов и поля', async () => {
      const server = createServer();

      const {body:result} = await request(server).post('/')
        .set('Content-Type', 'multipart/form-data')
        .attach('pictures', filePaths[0])
        .attach('pictures', filePaths[1])
        .attach('video', filePaths[2])
        .field('names', 'Dima')
        .field('names', 'Sergey')
        .expect(200);

      assert.deepStrictEqual(result.names, ['Dima', 'Sergey'],
        'неправильно сформированы поля');
      assert.ok(result.files, 'файл не загружен');

      const files = JSON.parse(result.files);
      assert.ok(files.pictures, 'файлы pictures не загружены');
      assert.ok(files.video, 'файл video не загружен');

      for (let file of files.pictures) {
        const filePath = file.path;
        const fileStat = await fsp.stat(filePath);
        assert.ok(fileStat.isFile(), 'picture не файл');
      }

      const videoPath = files.video.path;
      const videoStat = await fsp.stat(videoPath);
      assert.ok(videoStat.isFile(), 'video не файл');
    });
  });
});

function createServer(echo = false) {
  const server = express();
  server.use(bodyparserMw);

  server.use((req, res, next) => {
    const result = req.body;

    if (req.files) {
      result.files = JSON.stringify(req.files);
    }

    res.send(result);
  });

  return server;
}