const assert = require('assert');
const path = require('path');
const fsp = require('fs-promise');
const request = require('supertest');
const {stdout, stderr} = require('test-console');
const express = require('express');

const config = require('../configs');
const logger = require('../app/logger');
const logDirectory = config.get('logger.logDirectory');

describe('logger', () => {
  it('Ошибка неподдерживыаемого типа стрима', () => {
    assert.throws(() => logger({streamType: 'unknown'}), TypeError);
  });

  describe('Запись в файл: ', () => {
    const accessLogFile = path.join(logDirectory, 'access.log');
    const errorLogFile = path.join(logDirectory, 'error.log');

    let accessLogMw = null;
    let errorLogMw = null;

    before('создание middleware\'ов', async () => {
      const {access, error} = logger({streamType: 'file', logDirectory});

      accessLogMw = access;
      errorLogMw = error;
    });

    after('очистка middleware\'ов', async () => {
      accessLogMw = null;
      errorLogMw = null;

      await fsp.unlink(accessLogFile);
      await fsp.unlink(errorLogFile);
      await fsp.rmdir(logDirectory);
    });

    it('директория создана', async () => {
      let stats;

      try {
        stats = await fsp.stat(logDirectory);
      }
      catch (error) {}

      assert.ok(stats, `${logDirectory} не удалось открыть`);
      assert.ok(stats.isDirectory(), `${logDirectory} не директория`);
    });
    it('файлы логов созданы', async () => {
      let accessStats;
      let errorStats;

      try {
        accessStats = await fsp.stat(accessLogFile);
        errorStats = await fsp.stat(errorLogFile);
      }
      catch (error) {}

      assert.ok(accessStats, `${accessLogFile} не удалось открыть`);
      assert.ok(accessStats.isFile(), `${accessLogFile} не файл`);
      assert.ok(errorStats, `${errorLogFile} не удалось открыть`);
      assert.ok(errorStats.isFile(), `${errorLogFile} не файл`);
    });

    it('запись в access.log', async () => {
      const server = createServer(accessLogMw);
      const res = await request(server).get('/').expect(200);
      const content = await fsp.readFile(accessLogFile,
        {encoding: 'utf8'});
      const contentSize = Buffer.byteLength(content, 'utf8');

      assert.ok(contentSize > 0, 'данные не записаны');
      assert.strictEqual(content.match(/\n/g).length, 1,
        'отсутствует перенос строки');
    });
    it('запись в error.log', async () => {
      const server = createServer(errorLogMw);

      await request(server).get('/500').expect(500);
      await request(server).get('/404').expect(404);

      const content = await fsp.readFile(errorLogFile,
        {encoding: 'utf8'});
      const contentSize = Buffer.byteLength(content, 'utf8');

      assert.ok(contentSize > 0, 'данные не записаны');
      assert.strictEqual(content.match(/\n/g).length, 2,
        'отсутствует перенос строки');
    });
  });

  describe('Вывод в консоль: ', () => {
    let accessLogMw = null;
    let errorLogMw = null;

    before('создание middleware\'ов', async () => {
      const {access, error} = logger({streamType: 'console'});

      accessLogMw = access;
      errorLogMw = error;
    });

    after('очистка middleware\'ов', async () => {
      accessLogMw = null;
      errorLogMw = null;
    });

    it('вывод access логов', async () => {
      const server = createServer(accessLogMw);
      const inspect = stdout.inspect();

      await request(server).get('/').expect(200);

      inspect.restore();

      const content = inspect.output[0];
      const contentSize = Buffer.byteLength(content, 'utf8');

      assert.ok(contentSize > 0, 'данные не записаны');
      assert.strictEqual(content.match(/\n/g).length, 1,
        'отсутствует перенос строки');
    });

    it('вывод error логов', async () => {
      const server = createServer(errorLogMw);
      const inspect = stderr.inspect();

      await request(server).get('/500').expect(500);
      await request(server).get('/404').expect(404);

      inspect.restore();

      const contents = inspect.output;
      const contentSize = Buffer.byteLength(contents.join('\n'), 'utf8');

      assert.ok(contentSize > 0, 'данные не записаны');
      assert.strictEqual(contents.length, 2,
        'отсутствует перенос строки');
    });
  });
});

function createServer(logMw) {
  const app = express();

  app.use(logMw);
  app.get('/', (req, res) => {
    res.status(200).send('hello!');
  });
  app.get('/500', (req, res) => {
    res.status(500).send('bad :(');
  });
  app.get('/404', (req, res) => {
    res.status(404).send('not found');
  });

  return app;
}