const assert = require('assert');
const path = require('path');
const fsp = require('fs-promise');

const dbConnector = require('../app/database');
const config = require('../configs');

describe('database', () => {
  let db;

  before('Установка соединения с БД', async () => {
    db = await dbConnector(config.get('storage'));
  });

  after(async () => {
    await db.end();
  });

  it('Загрузка sql шаблонов', async () => {
    assert.ok(db.hasTemplate('plus'), 'Шаблон plus.sql не загружен');
    assert.ok(db.hasTemplate('tables'), 'Шаблон tables.sql не загружен');
    assert.ok(db.hasTemplate('subdir/plus'),
      'Шаблон subdir/plus.sql не загружен');
  });

  it('Установка шаблона', async () => {
    const rootPath = config.get('paths.root');
    const sqlFilePath = path.join(rootPath, 'migrations',
      'sql', 'user', 'create.sql');

    await db.setTemplate(sqlFilePath, 'custom');

    assert.ok(db.hasTemplate('custom/create'));
  });

  describe('Запрос по шаблону: ', async () => {
    it('множество значений', async () => {
      const resultPlus = await db.queryTemplate('plus');
      const resultSubdirPlus = await db.queryTemplate('subdir/plus', {
        first: 7,
        second: 5
      });

      assert.ok(Array.isArray(resultPlus),
        'в запросе plus ошибка, возвращает не массив');
      assert.strictEqual(resultPlus.length, 1,
        'в запросе plus ошибка, возвращает пустой массив');
      assert.strictEqual(resultPlus[0].num, 2,
        'в запросе plus ошибка, возвращает неверное значение');

      assert.ok(Array.isArray(resultSubdirPlus),
        'в запросе subdir/plus ошибка, возвращает не массив');
      assert.strictEqual(resultSubdirPlus.length, 1,
        'в запросе subdir/plus ошибка, возвращает пустой массив');
      assert.strictEqual(resultSubdirPlus[0].num, 12,
        'в запросе subdir/plus ошибка, возвращает неверное значение');
    });

    it('одно значение', async () => {
      const resultPlus = await db.queryTemplateOne('plus');
      const resultSubdirPlus = await db.queryTemplateOne('subdir/plus', {
        first: 7,
        second: 5
      });

      assert.ok(!!resultPlus,
        'в запросе plus ошибка, возвращает не объект');
      assert.strictEqual(resultPlus.num, 2,
        'в запросе plus ошибка, возвращает неверное значение');

      assert.ok(!!resultSubdirPlus,
        'в запросе subdir/plus ошибка, возвращает не объект');
      assert.strictEqual(resultSubdirPlus.num, 12,
        'в запросе subdir/plus ошибка, возвращает неверное значение');
    });
  });
});
