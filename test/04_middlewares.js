const path = require('path');
const assert = require('assert');

const config = require('../configs');
const {
  MiddlewareManager: MwManager,
  create: createMwManager
} = require('../app/managers/middlewares');
const mwsDirectory = config.get('middlewares.path');

describe('middlewares', () => {
  it('Ошибка не указанного параметра пути к маршрутам', () => {
    assert.throws(() => new MwManager(), TypeError);
  });

  describe('Регистрация moddleware\'a: ', () => {
    let manager;

    beforeEach(() => {
      manager = new MwManager(mwsDirectory);
    });

    afterEach(() => {
      manager = null;
    });

    it('с именнованной функцией', () => {
      const middleware = function namedMw(req, res, next) {
        next();
      };

      manager.register(middleware);

      assert.strictEqual(manager.get('namedMw')[0], middleware,
        'middleware "namedMw" не зарегистрирован');
    });

    it('с параметром name', () => {
      const manager = new MwManager(mwsDirectory);
      const name = 'myNameMw';
      const middleware = function (req, res, next) {
        next();
      };

      manager.register(middleware, name);

      assert.strictEqual(manager.get(name)[0], middleware,
        `middleware "${name}" не зарегистрирован`);
    });

    it('без имени', () => {
      const manager = new MwManager(mwsDirectory);

      assert.throws(() => {
        manager.register((req, res, next) => next());
      }, TypeError);
    });
  });

  it('Загрузка middleware\'ов', async () => {
    const manager = await createMwManager(mwsDirectory);
    const middlewares = manager.getAll(['first', 'second']);

    assert.strictEqual(middlewares.length, 2);
    assert.ok(middlewares[0].length,
      'middleware "first" не зарегистрирован');
    assert.ok(middlewares[1].length,
      'middleware "second" не зарегистрирован');

    assert.strictEqual(middlewares[0][0].name, 'first',
      'middleware "first" зарегистрирован неверно');
    assert.strictEqual(middlewares[1][0].name, '',
      'middleware "second" зарегистрирован неверно');
  });
});
