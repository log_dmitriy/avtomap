const assert = require('assert');
const express = require('express');
const request = require('supertest');

const config = require('../configs');
const {ParametersManager, create} = require('../app/managers/parameters');

describe('parameters', () => {
  it('Регистрация с одним параметром', async () => {
    const manager = await create(config.get('parameters'));

    assert.ok(manager.get('global'), 'не зарегестрировано по функции');
    assert.ok(manager.get('custom'), 'не зарегестрировано по объекту');
    assert.ok(manager.get('array'),
      'не зарегестрировано по объекту с массивом');
  });

  it('Регистрация в приложении', async () => {
    const server = express();
    const manager = await create(config.get('parameters'));

    manager.assignIn(server);
    manager.assignIn(server, 'custom');
    manager.assignIn(server, 'array');
    manager.assignIn(server, 'joined');

    server.get('/foo/:foo', (req, res) => { res.send(req.foo); });
    server.get('/bar/:bar', (req, res) => { res.send(req.bar); });
    server.get('/arr1/:arr1', (req, res) => { res.send(req.arr); });
    server.get('/arr2/:arr2', (req, res) => { res.send(req.arr); });
    server.get('/jn/:v1/:v2', (req, res) => { res.send(req.joined); });

    await request(server).get('/foo/123').expect(200).expect('123');
    await request(server).get('/bar/345').expect(200).expect('345');
    await request(server).get('/arr1/678').expect(200).expect('678');
    await request(server).get('/arr2/90').expect(200).expect('90');
    await request(server).get('/jn/5/7').expect(200)
      .expect('{"v1":"5","v2":"7"}');
  });
});