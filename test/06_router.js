const path = require('path');
const assert = require('assert');
const express = require('express');
const request = require('supertest');

const config = require('../configs');
const router = require('../app/managers/router');
const {create: createMwManager} = require('../app/managers/middlewares');
const {create: createParamManager} = require('../app/managers/parameters.js');
const routesDirectory = config.get('router.path');
const mwsDirectory = config.get('middlewares.path');
const paramDirectory = config.get('parameters.path');

describe('router', () => {
  it('Ошибка не указанного параметра пути к маршрутам', () => {
    assert.throws(() => new router.RoutesManager(), TypeError);
  });

  it('Регистрация express маршрутов', () => {
    const manager = new router.RoutesManager(routesDirectory);
    const homeRouter = express.Router();
    const userRouter = express.Router();

    homeRouter.get('/', (req, res) => res.status(200).send('ok home'));
    userRouter.get('/', (req, res) => res.status(200).send('ok user'));

    manager.register({router: homeRouter});
    manager.register({router: userRouter, mount: '/users'});

    assert.strictEqual(manager.get('/')[0], homeRouter,
      'маршрут в пути по-умолчанию не зарегистрирован');
    assert.strictEqual(manager.get('/users')[0], userRouter,
      'маршрут по указанному пути не зарегистрирован');
  });

  it('Регистрация именнованных маршрутов', () => {
    const manager = new router.RoutesManager(routesDirectory);
    const userRouter = {
      'api.home': {
        method: 'get',
        path: '/',
        handlers: [
          (req, res) => res.status(200).send('ok api')
        ]
      }
    };

    manager.register({router: userRouter, mount: '/api'});

    assert.ok(manager.get('/api')[0],
      'маршрут по указанному пути не зарегистрирован');
  });

  it('Загрузка маршрутов из директории', async () => {
    const manager = new router.RoutesManager(routesDirectory);

    await manager.load();

    assert.ok(!!manager.get('/')[0],
      'маршрут home не загружен');
    assert.ok(!!manager.get('/users')[0],
      'маршрут user не загружен');
    assert.ok(!!manager.get('/api')[0],
      'маршрут api не загружен');
  });

  it('Присоединения маршрутов к приложению', async () => {
    const manager = new router.RoutesManager(routesDirectory);
    const server = express();

    await manager.load();
    manager.assignIn(server);

    await request(server).get('/').expect(200).expect('ok home');
    await request(server).get('/users').expect(200).expect('ok user');
    await request(server).get('/api/list').expect(200).expect('ok list');
    await request(server).post('/api/add/12').expect(200)
      .expect('user added');
    await request(server).post('/api/add/789/short').expect(200)
      .expect('user added short');
  });

  describe('Регистрация роутов с middleware\'ами: ', () => {
    const testValue = 'VALUE!';

    let apiRouter;
    let server;
    let manager;
    let mwsManager;

    beforeEach(async () => {
      mwsManager = await createMwManager(mwsDirectory);
      manager = new router.RoutesManager(routesDirectory, mwsManager);

      server = express();

      apiRouter = {
        'api.home': {
          method: 'get',
          path: '/',
          handlers: [
            (req, res) => res.status(200).send(req.customField)
          ]
        }
      };
    });

    afterEach(() => {
      server = null;
      apiRouter = null;
      manager = null;
      mwsManager = null;
    });

    it('middleware без имени', async () => {
      apiRouter['api.home'].middlewares = [
        (req, res, next) => {
          req.customField = testValue;

          next();
        }
      ];

      manager.register({router: apiRouter, mount: '/api'});
      manager.assignIn(server);

      await request(server).get('/api').expect(200).expect(testValue);
    });

    it('именованный middleware', async () => {
      apiRouter['api.home'].middlewares = ['customField'];

      mwsManager.register((req, res, next) => {
        req.customField = testValue;

        next();
      }, 'customField');

      manager.register({router: apiRouter, mount: '/api'});
      manager.assignIn(server);

      await request(server).get('/api').expect(200).expect(testValue);
    });
  });

  it('Регистрация роута с параметрами: ', async () => {
    const paramManager = await createParamManager({path: paramDirectory});
    const manager = new router.RoutesManager(routesDirectory, null, paramManager);

    const server = express();

    const apiRouter = {
      'api.home': {
        method: 'get',
        path: '/:bar',
        handlers: [
          (req, res) => res.status(200).send(req.bar)
        ]
      }
    };

    manager.register({
      router: apiRouter,
      mount: '/api',
      paramsKey: 'custom'
    });
    manager.assignIn(server);

    await request(server).get('/api/ttt').expect(200).expect('ttt');
  });

  it('Получение путей именованных маршрутов', async () => {
    const manager = new router.RoutesManager(routesDirectory);
    const server = express();

    await manager.load();

    assert.strictEqual(manager.getUrl('api.users'), '/api/list',
      'путь для api.users построился неверно');
    assert.strictEqual(
      manager.getUrl('api.user.add', {id: 123}),
      '/api/add/123',
      'путь для api.users.add построился неверно'
    );
    assert.strictEqual(
      manager.getUrl('api.user.add.short', {id: 892}),
      '/api/add/892/short',
      'путь для api.users.add.short построился неверно'
    );
    assert.strictEqual(
      manager.getUrl('api.user.update', {id: 892, name: 'Serega'}),
      '/api/update/892/Serega',
      'путь для api.user.update построился неверно'
    );
  });

  it('Экспорт путей именованных маршрутов', async () => {
    const manager = new router.RoutesManager(routesDirectory);
    const server = express();

    const sampleMap = {
      'api.users': '/api/list',
      'api.user.add': '/api/add/:id',
      'api.user.add.short': '/api/add/:id/short',
      'api.user.update': '/api/update/:id/:name',

      __proto__: null
    };

    await manager.load();

    const exported = manager.exportRoutes();

    assert.deepStrictEqual(sampleMap, exported,
      'пути некорректно экспортируются');
  });
});
