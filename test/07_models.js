const assert = require('assert');
const path = require('path');
const fsp = require('fs-promise');

const config = require('../configs');
const dbConnector = require('../app/database');
const {
  ModelManager: Manager,
  create: createManager
} = require('../app/managers/models');
const modelsDirectory = config.get('models.path');
const IModel = require('../app/managers/models/imodel');

describe('models', () => {
  let db;

  before('Установка соединения с БД', async () => {
    db = await dbConnector(config.get('storage'));
  });

  after(async () => {
    await db.end();
  });

  describe('Регистрация моделей: ', () => {
    let manager;

    beforeEach(() => manager = new Manager(modelsDirectory, db));

    afterEach(() => manager = null);

    it('неизвестный класс', () => {
      class Entity {}

      assert.throws(() => manager.register(Entity), TypeError);
    });

    it('с переданным именем', () => {
      const key = 'key';

      class Entity extends IModel {}

      manager.register(Entity, key);

      assert.ok(manager.get(key), `модель "${key}" не зарегистрирована`);
    });

    it('без имени', () => {
      class Entity extends IModel {}

      manager.register(Entity);

      assert.ok(manager.get('entity'),
        `модель "entity" не зарегистрирована`);
    });
  });

  it('загрузка моделей', async () => {
    const manager = await createManager(modelsDirectory, db);

    assert.ok(manager.get('user'), 'модель "user" не загружена');
  });

  describe('Работа с моделью', async () => {
    let manager;
    let User;
    let user;

    beforeEach(async () => {
      manager = await createManager(modelsDirectory, db);
      User = manager.get('user');
      user = new User({id: 1, name: 'Dima'});
    });

    afterEach(() => {
      manager = null;
      User = null;
      user = null;
    });

    it('создание', () => {
      assert.ok(user instanceof User);
    });

    it('доставание полей', () => {
      assert.strictEqual(user.id, 1);
      assert.strictEqual(user.name, 'Dima');
    });

    it('изменение полей', () => {
      user.id = 2;
      user.name = 'Serega';

      assert.strictEqual(user.id, 2);
      assert.strictEqual(user.name, 'Serega');
    });

    it('удаление полей', () => {
      delete user.id;
      delete user.name;

      assert.ok(!user.id);
      assert.ok(!user.name);
    });
  });

  describe('Работа с БД', async () => {
    let manager;
    let User;

    before(async () => {
      manager = await createManager(modelsDirectory, db);
      User = manager.get('user');

      await db.queryTemplate('create_user_table');
    });

    after(async () => {
      manager = null;
      User = null;

      await db.queryTemplate('delete_user_table');
    });

    it('создание', async () => {
      const user = new User({name: 'Dima'});
      const id = await user.save();

      const select = await db.queryTemplateOne('get_user', {
        column: 'id',
        value: id
      });

      assert.strictEqual(select.name, user.name);
      assert.strictEqual(select.id, user.id);
      assert.strictEqual(id, user.id);
    });

    it('обновление', async () => {
      const user = new User({name: 'Sergey'});
      const id = await user.save();
      const NEW_NAME = 'Petr';

      user.name = NEW_NAME;
      await user.save();

      const select = await db.queryTemplateOne('get_user', {
        column: 'id',
        value: id
      });

      assert.strictEqual(select.name, NEW_NAME);
      assert.strictEqual(select.name, user.name);
      assert.strictEqual(select.id, user.id);
      assert.strictEqual(id, user.id);
    });

    it('удаление', async () => {
      const user = new User({name: 'Sergey'});
      const id = await user.save();

      const select = await db.queryTemplateOne('get_user', {
        column: 'id',
        value: id
      });

      assert.strictEqual(select.name, user.name);

      await user.delete();

      const afterSelect = await db.queryTemplate('get_user', {
        column: 'id',
        value: id
      });

      assert.deepStrictEqual(afterSelect, []);
    });
  });

  it('превращение в json объект', async () => {
    const manager = await createManager(modelsDirectory, db);
    const User = manager.get('user');
    const user = new User({id: 1, name: 'Ivan'});

    const strUser = JSON.stringify(user);
    const copyUser = new User(JSON.parse(strUser));

    assert.strictEqual(strUser, '{"id":1,"name":"Ivan"}');
    assert.strictEqual(JSON.stringify(copyUser), strUser);
  });
});