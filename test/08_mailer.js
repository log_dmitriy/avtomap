const assert = require('assert');

const config = require('../configs');
const {Mailer, create} = require('../app/managers/mailer');
const {create: createI18N} = require('../app/managers/i18n');

describe('mailer (offline!)', () => {
  const options = config.get('mailer');
  const address = 'some@addr.ru';

  it('Отправка текста', async () => {
    const mailer = new Mailer(options);
    const message = 'some <b>text</b>';

    let {message:msg} = await mailer.send(address, message);
    msg = JSON.parse(msg);

    assert.strictEqual(msg.to[0].address, address);
    assert.strictEqual(msg.text, 'some text');
    assert.strictEqual(msg.html, message);
  });

  it('Отправка по шаблону', async () => {
    const i18nManager = createI18N(config.get('i18n'));
    const mailer = await create(options, i18nManager);

    let {message:msg} = await mailer.sendTemplate(address,
      'confirm_register', 'ru', {url: 'http://ya.ru'});
    msg = JSON.parse(msg);

    assert.strictEqual(msg.to[0].address, address);
    assert.strictEqual(msg.text, 'ссылка http://ya.ru');
    assert.strictEqual(msg.html,
      'ссылка <a hre="http://ya.ru">http://ya.ru</a>');
  });

  it('Отправка по шаблону с локалью', async () => {
    const i18nManager = createI18N(config.get('i18n'));
    const mailer = await create(options, i18nManager);

    let {message:msg} = await mailer.sendTemplate(address,
      'confirm_register', 'en', {url: 'http://ya.ru'});
    msg = JSON.parse(msg);

    assert.strictEqual(msg.to[0].address, address);
    assert.strictEqual(msg.text, 'link http://ya.ru');
    assert.strictEqual(msg.html,
      'link <a hre="http://ya.ru">http://ya.ru</a>');
  });
});