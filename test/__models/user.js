const IModel = require('../../app/managers/models/imodel');

class User extends IModel {
  constructor(fields) {
    super(fields);
  }

  someFn() {
    return 'some value';
  }
}

module.exports = User;