module.exports = {
  name: ['arr1', 'arr2'],
  handler: function(req, res, next, value) {
    req.arr = value;

    next();
  },
  key: 'array'
};