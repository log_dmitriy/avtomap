module.exports = {
  name: 'bar',
  handler: function(req, res, next, value) {
    req.bar = value;

    next();
  }
};