module.exports = {
  name: ['v1', 'v2'],
  handler: function(req, res, next, values) {
    req.joined = JSON.stringify(values);

    next();
  },
  key: 'joined',
  join: true
};