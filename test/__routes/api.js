const router = {
  'api.users': {
    method: 'get',
    path: '/list',
    handlers: [
      (req, res) => res.status(200).send('ok list')
    ]
  },
  'api.user.add': {
    method: 'post',
    path: '/add/:id',
    handlers: [
      (req, res) => res.status(200).send('user added')
    ]
  },
  'api.user.add.short': {
    method: 'post',
    path: '/add/:id/short',
    handlers: [
      (req, res) => res.status(200).send('user added short')
    ]
  },
  'api.user.update': {
    method: 'post',
    path: '/update/:id/:name',
    handlers: [
      (req, res) => res.status(200).send('user updated')
    ]
  }
};


module.exports = {router, mount: '/api'};