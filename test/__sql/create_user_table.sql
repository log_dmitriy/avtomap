create table if not exists "user"(
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  "lastActivity" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "registeredAt" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);